<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_ReturnToApp</name>
   <tag></tag>
   <elementGuidId>fe6c5da0-1e4c-490c-8212-3ff9d76caaae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='thank-you-action']/button/span[text()=&quot;Return to app&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='thank-you-action']/button/span[text()=&quot;Return to app&quot;]</value>
   </webElementProperties>
</WebElementEntity>
