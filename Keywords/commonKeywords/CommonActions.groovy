package commonKeywords

import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.util.KeywordUtil as kwUtils
import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent

import internal.GlobalVariable


class CommonActions {

	static FailureHandling failureHandling = FailureHandling.STOP_ON_FAILURE
	static WebUiBuiltInKeywords webUI = new WebUiBuiltInKeywords()
	static String timeOut30s = GlobalVariable.G_maxTimeOut
	static String timeOut10s = GlobalVariable.G_minTimeOut
	static String timeOut1s = GlobalVariable.G_timeOut1s
	static String timeOut5s = GlobalVariable.G_timeOut5s


	private static final Logger logger = LoggerFactory.getLogger("myGroovyLogger")
	Logger LOGGER = Logger.g

	static input(TestObject to, def str_Data) {
		webUI.clearText(to, failureHandling)
		webUI.sendKeys(to, str_Data, failureHandling)
	}

	static click(TestObject to) {
		waitForElementPresent(to, timeOut30s)
		webUI.click(to, failureHandling)
	}

	static openBrowser() {
		webUI.openBrowser("", failureHandling)
	}

	static navigateToUrl(String str_Url) {
		webUI.navigateToUrl(str_Url, failureHandling)
		waitForPageToLoad(timeOut30s)
	}

	static maximizeWindow() {
		webUI.maximizeWindow(failureHandling)
	}

	static closeBrowser() {
		webUI.closeBrowser(failureHandling)
	}

	static waitForPageToLoad(String str_Data) {
		int intTimeOut = Integer.parseInt(str_Data)
		webUI.waitForPageLoad(intTimeOut, failureHandling)
	}

	static verifyTestObjectIsDisplayed(TestObject to){
		webUI.waitForElementVisible(to, Integer.parseInt(timeOut30s))
		webUI.verifyElementVisible(to, failureHandling)
	}

	static verifyTextIsEqual(TestObject to, String str_ExpectedText){
		String str_ActualText = webUI.getText(to, failureHandling)
		waitForElementVisible(to, timeOut30s)
		webUI.verifyEqual(str_ActualText, str_ExpectedText, failureHandling)
	}

	static verifyEquals(TestObject to, Float expectedValue){
		def actualValue = webUI.getText(to, failureHandling)
		webUI.verifyEqual(actualValue.substring(1, actualValue.length()), expectedValue, failureHandling)
	}

	static verifyFloatNumberIsEqual(float actualValue, float expectedValue) {
		webUI.verifyEqual(actualValue, expectedValue, failureHandling)
	}

	static float convertStringToFloat(String value){
		return Float.parseFloat(value)
	}

	static verifyAmountIsEqual(TestObject to, String str_ExpectedText){
		String str_ActualText = webUI.getText(to, failureHandling)
		waitForElementVisible(to, timeOut30s)
		webUI.verifyEqual(str_ActualText.substring(5, str_ActualText.length()), str_ExpectedText, failureHandling)
	}

	static formatDateWithTimeZoneToString(Date date, String str_DateFormat, String str_TimeZone) {
		TimeZone tzAuMelbourne = TimeZone.getTimeZone(str_TimeZone)
		DateFormat df = new SimpleDateFormat(str_DateFormat)

		df.setTimeZone(tzAuMelbourne)
		return df.format(date)
	}

	static switchToIframe(TestObject to, String str_TimeOut){
		int intTimeOut = Integer.parseInt(str_TimeOut)
		webUI.switchToFrame(to, intTimeOut, failureHandling)
	}

	static switchToWindowTitle(String str_WindowTitle){
		webUI.switchToWindowTitle(str_WindowTitle, failureHandling)
	}

	static logInfo(String content){
		kwUtils.logInfo("kw Util: " + content)
	}

	static delay(String str_TimeOut){
		webUI.delay(Integer.parseInt(str_TimeOut), failureHandling)
	}

	static waitForElementPresent(TestObject to, String str_TimeOut) {
		webUI.waitForElementPresent(to, Integer.parseInt(str_TimeOut), failureHandling)
	}

	static waitForElementVisible(TestObject to, String str_TimeOut) {
		webUI.waitForElementVisible(to, Integer.parseInt(str_TimeOut), failureHandling)
	}

	static waitForElementNotVisible(TestObject to, String str_TimeOut) {
		webUI.waitForElementNotVisible(to, Integer.parseInt(str_TimeOut), failureHandling)
	}

	static waitForElementClickable(TestObject to, String str_TimeOut) {
		webUI.waitForElementClickable(to, Integer.parseInt(str_TimeOut), failureHandling)
	}

	static String getText(TestObject to){
		webUI.getText(to, failureHandling)
	}

	static int getNumberOfTestObject (TestObject to, String str_TimeOut) {
		int int_TimeOut = Integer.parseInt(str_TimeOut)
		return webUI.findWebElements(to, int_TimeOut).size()
	}

	static TestObject createTestObject(String locatorType, String locatorValue){
		TestObject to = new TestObject()
		to.addProperty(locatorType, ConditionType.EQUALS, locatorValue)
		return to
	}

	static TestObject modifyTestObject(TestObject to, String locatorType, String modifiedValue){
		return webUI.modifyObjectProperty(to, locatorType, "equals", modifiedValue, true)
	}

	static String findPropertyValue(TestObject to, String propertyName){
		return to.findPropertyValue(propertyName)
	}

	static scrollToObject(TestObject to){
		webUI.scrollToElement(to, Integer.parseInt(timeOut30s))
	}

	static acceptAlert(){
		webUI.acceptAlert()
	}
	static String replaceAllByRegex(String str_OriginalText, String str_Regex, String str_ReplacedText) {
		return str_OriginalText.replaceAll(str_Regex, str_ReplacedText)
	}

	static setText(TestObject to, String strValue){
		webUI.setText(to, strValue);
	}

	static uploadFile(TestObject to, String filePath, String fileName){
		webUI.click(to)
		webUI.delay(timeOut1s)
		String fileDestination = filePath + fileName
		StringSelection ss = new StringSelection(fileDestination);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}
	
	static focusOnObject(TestObject to){
		webUI.focus(to)
	}

	public getRowOnTable
	public getColOnTable
}
