package com.gui.mobile.screens

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import com.core.utilities.common.Enum
import com.core.utilities.common.CommonMobileActions as common

public class ThankYou {

	static FailureHandling stop_on_failure = FailureHandling.STOP_ON_FAILURE

	static TestObject btnDone = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.TextView' and (@text = 'DONE' or . = 'DONE') and @resource-id = 'com.shoutforgood.shout:id/done_btn']")

	//static TestObject btnOtherPayment = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.Button' and (@text = 'OTHER PAYMENT OPTIONS' or . = 'OTHER PAYMENT OPTIONS') and @resource-id = 'com.shoutforgood.shout:id/btnOtherPayment']")

	void verifyNavigateToAdScreen() {

		common.tap(btnDone,stop_on_failure)

	}

	void verifyNavigateToCharityDetailcreen() {

		common.tap(btnDone,stop_on_failure)

	}

	void verifyNavigateToCollectMerchandise(String charityName) {

	}

	void verifyMerchandiseItems(String charityName) {

	}


}
