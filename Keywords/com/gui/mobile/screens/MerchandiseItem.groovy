package com.gui.mobile.screens

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.core.utilities.common.CommonMobileActions as common
import com.core.utilities.common.Enum
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject


public class MerchandiseItem {

	static FailureHandling stop_on_failure = FailureHandling.STOP_ON_FAILURE

	static TestObject lblHelpSupport = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.TextView' and (@text = 'Help support ActionAid Australia' or . = 'Help support ActionAid Australia') and @resource-id = 'com.shoutforgood.shout:id/charity_name_text_view']")
	static TestObject lblMerchandiseName = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.TextView' and (@text = 'Item2 long name long name long name long name long name long name long name long name long name long name' or . = 'Item2 long name long name long name long name long name long name long name long name long name long name') and @resource-id = 'com.shoutforgood.shout:id/merchandise_name']")
	static TestObject lsItemColor = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/androidx.recyclerview.widget.RecyclerView[1]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.TextView[1]")
	static TestObject btnNext = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.Button' and (@text = 'NEXT' or . = 'NEXT') and @resource-id = 'com.shoutforgood.shout:id/next_btn']")
	static TestObject optColor = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.TextView' and (@text = 'Red' or . = 'Red') and @resource-id = 'com.shoutforgood.shout:id/item_content_tv']")
	static TestObject lsItemSize = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.ImageView' and @resource-id = 'com.shoutforgood.shout:id/imvIconDown2']")

	static TestObject optSize = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.TextView' and (@text = 'Large' or . = 'Large') and @resource-id = 'com.shoutforgood.shout:id/item_content_tv']")
	static TestObject txtQuantity = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.ImageView' and @resource-id = 'com.shoutforgood.shout:id/plus_btn']")
	static TestObject btnAddToCart = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.Button' and (@text = 'Add to cart' or . = 'Add to cart') and @resource-id = 'com.shoutforgood.shout:id/add_to_cart_btn']")

	//String merchandiseName = "//*[@class='welcome-message'][contains(text(),'%s')]"
	def mName = "Item2 long name long name long name long name long name long name long name long name long name long name"
	//String xPathMerchandiseName = "//*[@class = 'android.widget.TextView' and @text='%s' and @resource-id = 'com.shoutforgood.shout:id/merchandise_name')]"

	String xPathMerchandiseName = "//*[@class = 'android.widget.TextView' and (@text() = '${mName}' or . = '${mName}') and @resource-id = 'com.shoutforgood.shout:id/merchandise_name')]"

	TestObject createMerchandiseName(String itemName) {
		println("AAAAAAA" + String.format(xPathMerchandiseName, itemName))
		//return common.createTestObject(Enum.LocatorType.XPATH.getType(), String.format(xPathMerchandiseName, itemName))
		return common.createTestObject(Enum.LocatorType.XPATH.getType(), xPathMerchandiseName)
	}

	/*TestObject createMerchandiseName(String itemName) {
	 return common.createTestObject(Enum.LocatorType.XPATH.getType(), String.format(xPathMerchandiseName, itemName))
	 }
	 TestObject createMerchandiseName(String itemName) {
	 return common.createTestObject(Enum.LocatorType.XPATH.getType(), String.format(xPathMerchandiseName, itemName))
	 }
	 TestObject createMerchandiseName(String itemName) {
	 return common.createTestObject(Enum.LocatorType.XPATH.getType(), String.format(xPathMerchandiseName, itemName))
	 }*/


	static goToConfirmPaymentScreen() {
		common.tap(btnNext,stop_on_failure)
	}

	void addMerchandiseToCart(String charityName) {

		common.tap(btnAddToCart,stop_on_failure)

	}

	void selectMerchandiseName(String merchandiseName) {
		//common.tap(createMerchandiseName(merchandiseName),stop_on_failure)
		common.tap(lblMerchandiseName, stop_on_failure)
		common.tap(lsItemColor, stop_on_failure)
		common.scrollToObject('Red')
		common.tap(optColor,stop_on_failure)
		common.tap(lsItemSize,stop_on_failure)
		common.scrollToObject('Large')
		common.tap(optSize,stop_on_failure)
		common.tap(txtQuantity,stop_on_failure)
		common.tap(btnAddToCart,stop_on_failure)
		goToConfirmPaymentScreen()
	}

}
