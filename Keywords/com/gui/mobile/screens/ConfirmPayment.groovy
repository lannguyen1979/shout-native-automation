package com.gui.mobile.screens

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.core.utilities.common.CommonMobileActions as common
import com.core.utilities.common.Enum
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject


public class ConfirmPayment {
	static FailureHandling stop_on_failure = FailureHandling.STOP_ON_FAILURE

	static TestObject btnGPay = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.ImageView[1]")
	static TestObject btnOtherPayment = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.Button' and (@text = 'OTHER PAYMENT OPTIONS' or . = 'OTHER PAYMENT OPTIONS') and @resource-id = 'com.shoutforgood.shout:id/btnOtherPayment']")


	static clickOtherPayment() {
		common.tap(btnOtherPayment,stop_on_failure)
	}


	void donationWithGPay() {

		common.tap(btnGPay,stop_on_failure)
		//verify donation
	}

	void donationWithMerchandise(String charityName) {
		clickOtherPayment()
		//verifyCharityName(charityName)
	}

	void verifyMerchandiseItems(String charityName) {
	}
}
