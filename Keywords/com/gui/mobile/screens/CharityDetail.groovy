package com.gui.mobile.screens

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.core.utilities.common.CommonMobileActions as common
import com.core.utilities.common.Enum
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject

public class CharityDetail {

	static FailureHandling stop_on_failure = FailureHandling.STOP_ON_FAILURE

	static TestObject btnDonate = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.Button' and (@text = 'DONATE' or . = 'DONATE') and @resource-id = 'com.shoutforgood.shout:id/donate_button']")
	static TestObject btnMerchandise = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.Button' and (@text = 'SHOP' or . = 'SHOP') and @resource-id = 'com.shoutforgood.shout:id/buy_merchandise_button']")
	static TestObject lblcharityName = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.TextView' and (@text = 'ActionAid Australia' or . = 'ActionAid Australia') and @resource-id = 'com.shoutforgood.shout:id/charity_name']")


	/*String charityName = "//*[@class='welcome-message'][contains(text(),'%s')]"
	 TestObject createTestObjectById(String id){
	 return common.createTestObject("xpath", "//*[@id='$id']")
	 }*/

	/*TestObject createWelcomeMsgObject(String userFirstName){
	 common.createTestObject("xpath", String.format(lblWelcomeMsgXpath, userFirstName))
	 }*/

	static clickDonateButton() {
		common.tap(btnDonate,stop_on_failure)
	}

	static clickDonateWithMerchandiseButton() {
		common.tap(btnMerchandise,stop_on_failure)
	}

	static void verifyCharityName(String expectedCharityName) {

		common.verifyEqualIgnoreCase(lblcharityName, expectedCharityName, stop_on_failure)
	}

	void charityDonationOnly(String charityName) {

		verifyCharityName(charityName)
		clickDonateButton()
	}

	void charityDonationWithMerchandise(String charityName) {
		verifyCharityName(charityName)
		clickDonateWithMerchandiseButton()
	}
}
