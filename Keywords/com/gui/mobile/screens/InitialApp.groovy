package com.gui.mobile.screens

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.core.utilities.common.CommonMobileActions as common
import com.core.utilities.common.Enum
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject

public class InitialApp {

	static FailureHandling stop_on_failure = FailureHandling.STOP_ON_FAILURE
	static TestObject btnOK = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.Button' and (@text = 'OK' or . = 'OK') and @resource-id = 'android:id/button1']")
	static TestObject btnGotIt = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.Button' and (@text = 'GOT IT!' or . = 'GOT IT!') and @resource-id = 'com.shoutforgood.shout:id/btnGotIt']")
	static TestObject btnSearch = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.Button' and (@text = 'SEARCH' or . = 'SEARCH') and @resource-id = 'com.shoutforgood.shout:id/btnSearch']")
	static TestObject noticeSelectFavouriteCause = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.TextView' and (@text = 'Select your favourite cause' or . = 'Select your favourite cause') and @resource-id = 'com.shoutforgood.shout:id/txtDes']")
	static TestObject txtCharityName = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class = 'android.widget.TextView' and (@text = 'ActionAid Australia' or . = 'ActionAid Australia') and @resource-id = 'com.shoutforgood.shout:id/shout_who_name']")


	void inItialApp() {

		common.startApp('D:\\gpay_native-qa3-release.apk', true)

		common.tap(btnOK, stop_on_failure)

		//Mobile.tap(findTestObject('Notice_Dialog/notice_message_btn_OK'), 0)
		common.tap(btnGotIt, stop_on_failure)

		common.tap(btnSearch, stop_on_failure)

		common.tap(btnSearch, stop_on_failure)

		common.tap(noticeSelectFavouriteCause, stop_on_failure)

		common.tap(txtCharityName, stop_on_failure)
	}
}
