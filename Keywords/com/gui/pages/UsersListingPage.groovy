package com.gui.pages

import com.kms.katalon.core.testobject.TestObject
import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum.LocatorType

class UsersListingPage {

	TestObject txtSearchArea = common.createTestObject(LocatorType.XPATH.getType(), "//input[@id='keyword']")
	TestObject btnSearch = common.createTestObject(LocatorType.XPATH.getType(), "//span[text()='Search']")
	TestObject donorRow = common.createTestObject(LocatorType.XPATH.getType(), "//table[contains(@class,'donors-table')]//tbody/tr[1]")

	void putForVaraiableDonorDetailInfo(TestObject to, String expectedDonorDetailInfo){
		String newXpathValue = String.format(to.findPropertyValue("xpath"),expectedDonorDetailInfo)
		common.modifyTestObject(to,LocatorType.XPATH.getType(), newXpathValue)
	}

	void searchDonorByEmail(String email){
		common.input(txtSearchArea, email)
		common.click(btnSearch)
	}

	/* modifyTestObjectWithDonorAttribute is called after searchDonorByEmail*/
	Map <String, TestObject> modifyTestObjectWithDonorAttribute(){
		String xpathcellDonor =  "//td[contains(@class,'table-content__col__%s')]"
		String  xpathDateCreated=  "//td[contains(@class,'table-content__col__%s')]//span"
		Map <String, TestObject> donorInfo = new HashMap<>()
		TestObject newDonorName = common.createTestObject(LocatorType.XPATH.getType(), String.format(xpathcellDonor, "name"))
		TestObject newDonorEmail = common.createTestObject(LocatorType.XPATH.getType(), String.format(xpathcellDonor, "email"))
		TestObject newDateCreated = common.createTestObject(LocatorType.XPATH.getType(), String.format(xpathDateCreated, "dateCreated"))
		TestObject newDonorRole = common.createTestObject(LocatorType.XPATH.getType(), String.format(xpathcellDonor, "role"))
		TestObject newDonorStatus = common.createTestObject(LocatorType.XPATH.getType(), String.format(xpathcellDonor, "status"))
		donorInfo.put("donorName", newDonorName)
		donorInfo.put("donorEmail", newDonorEmail)
		donorInfo.put("dateCreated", newDateCreated)
		donorInfo.put("donorRole", newDonorRole)
		donorInfo.put("donorStatus", newDonorStatus)
		donorInfo
	}

	//get Expected Donor Information
	Map <String, String> getDonorInfo(String donorName, String donorEmail, Object dateCreated, String donorRole, String donorStatus ){
		Map <String, String> donorInfo = new HashMap<>()
		donorInfo.put("donorName", donorName)
		donorInfo.put("donorEmail", donorEmail)
		donorInfo.put("dateCreated", dateCreated)
		donorInfo.put("donorRole", donorRole)
		donorInfo.put("donorStatus", donorStatus)
		donorInfo
	}

	void verifyDonorInfo(Map <String, TestObject> actualResult, Map <String, String> expectedResult) {
		common.verifyTextIsEqual(actualResult.get("donorName"), expectedResult.get("donorName"))
		common.verifyTextIsEqual(actualResult.get("donorEmail"), expectedResult.get("donorEmail"))
		common.verifyTextIsEqual(actualResult.get("dateCreated"), expectedResult.get("dateCreated"))
		common.verifyTextIsEqual(actualResult.get("donorRole"), expectedResult.get("donorRole"))
		common.verifyTextIsEqual(actualResult.get("donorStatus"), expectedResult.get("donorStatus"))
	}

	void navigateToDonorDetailPage(){
		common.click(donorRow)
	}

}
