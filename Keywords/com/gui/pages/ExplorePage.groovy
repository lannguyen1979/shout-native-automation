package com.gui.pages

import com.kms.katalon.core.testobject.TestObject
import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum

class ExplorePage extends BasePage{
	TestObject btnSearch = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(concat(' ', normalize-space(@class), ' '), ' app__routes ')]//*[text()='Search']")
	TestObject inputSearch = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(@class,'app__routes')]//*[@id='searchbox']")
	TestObject aCategory = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(concat(' ', normalize-space(@class), ' '), ' card__title--main ')][contains(text(),'%s')]")

	void searchbyCharityName(String charityName){
		common.input(inputSearch, charityName)
		common.click(btnSearch)
		waitForIconLoadingNotPresent(common.timeOut30s)
	}
}
