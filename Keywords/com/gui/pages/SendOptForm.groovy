package com.gui.pages

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class SendOptForm {

	TestObject inputMobileNumber = common.createTestObject(Enum.LocatorType.XPATH.getType(),
			"//*[@id='modal-root']//input[@id='mobileNumber']")

	TestObject btnGetCode = common.createTestObject(Enum.LocatorType.XPATH.getType(),
			"//button/span[contains(text(),'Get Code')]")

	TestObject btnSubmit = common.createTestObject(Enum.LocatorType.XPATH.getType(),
			"//button/span[contains(text(),'Submit')]")

	TestObject modalContent = common.createTestObject(Enum.LocatorType.XPATH.getType(),
			"//*[@id='modal-root']")

	TestObject inputVerificationCode = common.createTestObject(Enum.LocatorType.XPATH.getType(),
			"//*[@id='modal-root']//input[@id='verificationCode']")


	String mobileNumber = "0499830224"
	String optCode = "999999"

	void getOptCode(){
		common.waitForElementClickable(inputMobileNumber, common.timeOut30s)
		common.input(inputMobileNumber, mobileNumber)
		common.click(btnGetCode)
		common.waitForPageToLoad(common.timeOut30s)
		common.waitForElementClickable(inputVerificationCode, common.timeOut30s)
		KeywordUtil.logInfo("getOptCode successfully...")
	}

	void submitOptCode(){
		common.waitForElementClickable(inputMobileNumber, common.timeOut30s)
		common.input(inputMobileNumber, mobileNumber)
		common.click(btnGetCode)
		common.waitForPageToLoad(common.timeOut30s)
		common.waitForElementClickable(inputVerificationCode, common.timeOut30s)
		KeywordUtil.logInfo("getOptCode successfully...")
		common.waitForElementClickable(inputVerificationCode, common.timeOut30s)
		common.input(inputVerificationCode, optCode)
		common.click(btnSubmit)
		common.waitForPageToLoad(common.timeOut30s)
	}

	void submitOptCodeOneStep(){
		common.waitForElementClickable(inputVerificationCode, common.timeOut30s)
		common.input(inputVerificationCode, optCode)
		common.click(btnSubmit)
		common.waitForPageToLoad(common.timeOut30s)
		common.delay("1")
	}

	boolean is2FAFormPresent(){
		return WebUI.verifyElementPresent(modalContent, GlobalVariable.G_maxTimeOut)
	}
}
