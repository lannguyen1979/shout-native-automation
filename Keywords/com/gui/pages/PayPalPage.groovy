package com.gui.pages

import com.core.utilities.common.WebObjects
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum
import static com.core.utilities.common.Constants.PAYPAL_LOGIN_TITLE
import static com.core.utilities.common.Constants.PAYPAL_CHECKOUT_CHOOSE_WAY_TO_PAY_TITLE
import static com.core.utilities.common.Constants.PAYPAL_CHECKOUT_TITLE
import static com.core.utilities.common.Constants.PAYPAL_CHECKOUT_REVIEW_PAYMENT

import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

class PayPalPage {

	static String str_PayPalLoginWindowTitle = findTestData('MasterData/PaypalPagesInfo').getObjectValue('WindowTitle', PAYPAL_LOGIN_TITLE)
	static String str_PayPalWayToPayTitle = findTestData('MasterData/PaypalPagesInfo').getObjectValue('WindowTitle', PAYPAL_CHECKOUT_CHOOSE_WAY_TO_PAY_TITLE)
	static String str_PayPalCheckoutTitle = findTestData('MasterData/PaypalPagesInfo').getObjectValue('WindowTitle', PAYPAL_CHECKOUT_TITLE)
	static String str_PayPalCheckoutReviewPaymentTitle = findTestData('MasterData/PaypalPagesInfo').getObjectValue('WindowTitle', PAYPAL_CHECKOUT_REVIEW_PAYMENT)
	static TestObject txtPayPalEmail = common.createTestObject(Enum.LocatorType.CSS.getType(), "#email")
	static TestObject txtPayPalPassword = common.createTestObject(Enum.LocatorType.CSS.getType(), "#password")
	static TestObject btnPayPalLogin = common.createTestObject(Enum.LocatorType.CSS.getType(), "#btnLogin")
	static TestObject btnPayPalRedirectToLogin = common.createTestObject(Enum.LocatorType.CSS.getType(), "div[class*=LoginButtonContainer]")
	static TestObject ifrmPayPal = common.createTestObject(Enum.LocatorType.CSS.getType(), ".zoid-component-frame.zoid-visible")
	static TestObject btnPayPalContinue = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//button[contains(text(),'Continue')]")
	static TestObject btnPayPalPayNow = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//input[@id='confirmButtonTop']")
	static TestObject btnPayment = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//button[@id='payment-submit-btn'][@data-disabled='false']");
	static TestObject btnAgreePayment = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//div[@id='button']");
	static TestObject optPayPalBalanceAUD = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//span[text()='(AUD)']/ancestor::div[@class='row-fluid radioButton']")
	static TestObject btnPaypayWayToPayContinue = common.createTestObject(Enum.LocatorType.CSS.getType(), "div#button > button")
	static TestObject img_PayPalLogo = common.createTestObject(Enum.LocatorType.CSS.getType(), "#PayPalLogo")
	static TestObject btnPayPalNext = common.createTestObject(Enum.LocatorType.CSS.getType(), "#btnNext")
	static TestObject linkPayPalContinue = common.createTestObject(Enum.LocatorType.CSS.getType(), ".paypal-checkout-continue")
	static TestObject userPopUp = common.createTestObject(Enum.LocatorType.CSS.getType(), "#login")
	static TestObject btnLogin = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@id='loginSection']//*[text()='Log In']")
	static TestObject payPayPopUp = common.createTestObject(Enum.LocatorType.CSS.getType(), "#main")
	static TestObject checkOutPopUp = common.createTestObject(Enum.LocatorType.CSS.getType(), "#root")
	

	static loginPayPal(String str_GenericPayPalEmailAddress, String str_GenericPayPalPassword) {
		common.waitForElementClickable(txtPayPalEmail, common.timeOut30s)
		common.input(txtPayPalEmail, str_GenericPayPalEmailAddress)
		common.click(btnPayPalNext)
		common.waitForElementVisible(txtPayPalPassword, common.timeOut30s)
		common.input(txtPayPalPassword, str_GenericPayPalPassword )
		common.click(btnPayPalLogin)
		common.waitForPageToLoad(common.timeOut30s)
	}

	static paymentOneTimeViaPayPal(String str_GenericPayPalEmailAddress, String str_GenericPayPalPassword){
		navigateToPayPalLogin()
		loginPayPal(str_GenericPayPalEmailAddress, str_GenericPayPalPassword)
		navigateToPayPalCheckout()
	}

	static void navigateToPayPalCheckout() {
		common.waitForPageToLoad(common.timeOut30s)
		WebObjects.waitForTitlleReturned("PayPal Checkout - Choose a way to pay")
//		common.waitForElementVisible(btnPayment, common.timeOut30s)
//		common.scrollToObject(btnPayment)
//		common.delay(common.timeOut1s)
		common.scrollToObject(btnPayPalContinue)
		common.click(btnPayPalContinue)
		common.delay(common.timeOut5s)
		common.waitForElementClickable(btnPayPalPayNow, common.timeOut30s)
		common.click(btnPayPalPayNow)
		WebObjects.waitForElementNotPresent(checkOutPopUp, GlobalVariable.G_maxTimeOut)
	}

	static void navigateToPayPalLogin() {
		common.waitForPageToLoad(common.timeOut30s)
		common.switchToWindowTitle(str_PayPalLoginWindowTitle)
	}

	static paymentRecurringViaPayPal(String str_GenericPayPalEmailAddress, String str_GenericPayPalPassword){
		navigateToPayPalLogin()
		loginPayPal(str_GenericPayPalEmailAddress, str_GenericPayPalPassword)
		navigateToPayPalBillingCheckout()
	}

	static void navigateToPayPalBillingCheckout() {
		common.waitForPageToLoad(common.timeOut30s)
		common.waitForElementVisible(btnPayPalContinue, common.timeOut30s)
		common.scrollToObject(btnPayPalContinue)
		common.click(btnPayPalContinue)
		common.delay(common.timeOut5s)
		common.waitForElementVisible(btnAgreePayment, common.timeOut30s)
		common.click(btnAgreePayment)
		WebObjects.waitForElementNotPresent(checkOutPopUp, GlobalVariable.G_maxTimeOut)
	}
}
