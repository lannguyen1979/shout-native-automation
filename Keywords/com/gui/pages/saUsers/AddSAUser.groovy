package com.gui.pages.saUsers

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum
import com.kms.katalon.core.testobject.TestObject

public class AddSAUser {

	TestObject btnAddSAUser = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[text()='Add SA User']")

	TestObject createTestObjectWithId(String id) {
		return common.createTestObject(Enum.LocatorType.XPATH.getType(),
		"//*[@id='$id']")
	}

	TestObject createTestObjectWithText(String text) {
		return common.createTestObject(Enum.LocatorType.XPATH.getType(),
		"//option[text()='$text']")
	}
	
	void addSAAdminSuccessfully(String nonShouterEmail,String firstName, String lastName ){
		inputSAEmailAndInfo(nonShouterEmail,firstName,lastName) 
		selectRoleDropdown()
		common.click(createTestObjectWithText("Admin"))
		common.click(createTestObjectWithId("btn-submit"))
	}
	
	void addSAUserSuccessfully(String nonShouterEmail,String firstName, String lastName ){
		inputSAEmailAndInfo(nonShouterEmail,firstName,lastName)
		selectRoleDropdown()
		common.click(createTestObjectWithText("User"))
		common.click(createTestObjectWithId("btn-submit"))
	}
	void selectRoleDropdown(){
		TestObject drpTo = createTestObjectWithId("role")
		common.waitForElementVisible(drpTo, common.getTimeOut30s())
		common.click(drpTo)
	}
	
	private void inputSAEmailAndInfo(email,firstname,lastName) {
		common.click(btnAddSAUser)
		common.input(createTestObjectWithId("email"), email)
		common.click(createTestObjectWithId("btn-submit"))
		common.input(createTestObjectWithId("firstName"), firstname)
		common.input(createTestObjectWithId("lastName"), lastName)
	}
}
