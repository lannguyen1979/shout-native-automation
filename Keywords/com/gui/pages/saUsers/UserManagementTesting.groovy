package com.gui.pages.saUsers


import com.core.utilities.models.CSRFTokens
import com.core.utilities.models.Donors
import com.core.utilities.models.User
import com.core.utilities.models.charity.UserAssignedRole
import com.core.utilities.models.users.UserDetail
import com.core.utilities.models.users.UserPaymentMethod
import com.gui.pages.Users.ActivateUserPage
import com.core.utilities.webservice.BaseAPITesting
import com.core.utilities.webservice.charities.CharityAPITesting
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.gui.pages.LoginPage
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import org.apache.commons.lang3.StringUtils
import org.testng.Assert

import static com.core.utilities.common.Constants.CHARITIES_CHANGE_OWNER_PATH
import static com.core.utilities.common.Constants.CHARITY_USER_ROLE_ADMIN
import static com.core.utilities.common.Constants.CHARITY_USER_ROLE_REPORTING
import static com.core.utilities.common.Constants.CHARITY_USER_ROLE_USER
import static com.core.utilities.common.Constants.SIGN_UP_INFOMATION
import static com.core.utilities.common.Constants.USER_ROLE_SHOUTER
import static internal.GlobalVariable.*
import static internal.GlobalVariable.G_loginPassword

class UserManagementTesting {

    BaseAPITesting apiTesting = new BaseAPITesting()
    CharityAPITesting charityAPITesting = new CharityAPITesting()
    Gson gson = new Gson()

    /** get existing shouter with only role of giver:charity
     * have no any charity role **/
    User getActiveShouter(){
        List<User> users = getActiveUserList()
        User user = users.find({ it -> StringUtils.equalsIgnoreCase(it.getType(), "Shouter") && StringUtils.equalsIgnoreCase(it.getStatus(), "active") && StringUtils.isEmpty(it.getPendingRole()) && it.getRoles().length == 1 && it.getRoles().find { s -> s.equalsIgnoreCase(USER_ROLE_SHOUTER) }
        })
        user.setTwoFactor(getUserDetail(user.getEmail()).getEnableTwoFactorAuth())
        return user
    }

    private List<User> getActiveUserList(CSRFTokens csrfTokens = apiTesting.getUserAuthorization(G_saUserNameAPI, G_loginPassword)) {
        ResponseObject userRes = apiTesting.sendGetRequestWithCSRF(G_baseAPIUrl + "/admin/donors?page=1&pageSize=100", csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())
        WebUiBuiltInKeywords.verifyEqual(userRes.getStatusCode(), 200)
        List<User> users = apiTesting.mapResponseToModel(userRes, User[].class)
        users
    }

    String activeCharityId = charityAPITesting.getActiveCharity(G_saUserNameAPI).getId()

    String getAccActivationLink(Boolean isChangeOwner = false, boolean isCharityOwner = false, String userLogin = G_saUserNameAPI, UserAssignedRole userAssign) {
        ResponseObject userRes

        if (isChangeOwner && isCharityOwner) {
            userRes = changeOwner(userLogin, CHARITIES_CHANGE_OWNER_PATH, userAssign)
        } else if (isChangeOwner && !(isCharityOwner)) {
            userRes = changeOwner(userLogin, userAssign)
        } else {
            userRes = assignRole(userLogin, userAssign)
        }
        def respondBody = userRes.getResponseBodyContent()
        Assert.assertEquals(userRes.getStatusCode(), 200, "change owner or assign role failed ${userRes.getStatusCode()}")
        JsonObject responseJs = gson.fromJson(respondBody, JsonObject.class)
        KeywordUtil.logInfo("assigned user response: " + gson.toJson(respondBody))
        String activationLink = responseJs.get("activation_link").asString
        KeywordUtil.logInfo("activationLink = $activationLink")
        return activationLink
    }

    ResponseObject assignRole(String userLogin = G_saUserNameAPI, UserAssignedRole userAssign) {
        def charityAssignRolePath = "/admin/charities/${userAssign.getCharityId()}/assign-role"
        'Step: assign role'
        CSRFTokens csrfTokens = apiTesting.getUserAuthorization(userLogin, G_loginPassword)
        ResponseObject userRes = apiTesting.sendPOSTRequestWithCSRF(G_baseAPIUrl + charityAssignRolePath, gson.toJson(userAssign), csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())
        def bodyResContent = userRes.getResponseBodyContent()
        KeywordUtil.logInfo("$charityAssignRolePath failed, detail is $bodyResContent")
        Assert.assertEquals(userRes.getStatusCode(), 200, "$charityAssignRolePath failed because ${userRes.getStatusCode()}")
        userRes
    }

    /*change owner when userAssign having other charity role*/
    ResponseObject changeOwner(String userLogin = G_saUserNameAPI, String changeOwnerPath = "/admin/charities/${existingUser.getCharityId()}/change-owner", UserAssignedRole existingUser) {
        'Step: change owner'
        CSRFTokens csrfTokens = apiTesting.getUserAuthorization(userLogin, G_loginPassword)
        ResponseObject userRes = apiTesting.sendPOSTRequestWithCSRF(G_baseAPIUrl + changeOwnerPath, gson.toJson(existingUser), csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())
        def bodyResContent = userRes.getResponseBodyContent()
        KeywordUtil.logInfo("existing UserAssignedRole : ${gson.toJson(existingUser)}")
        KeywordUtil.logInfo("$changeOwnerPath bodyResContent : $bodyResContent")
        Assert.assertEquals(userRes.getStatusCode(), 200, "$changeOwnerPath failed because ${userRes.getStatusCode()}")
        userRes
    }

    List<User> getAssignedUsers(String charityId = activeCharityId) {
        CSRFTokens csrfTokens = apiTesting.getUserAuthorization(G_saUserNameAPI, G_loginPassword)
        ResponseObject userRes = apiTesting.sendGetRequestWithCSRF(G_baseAPIUrl + "/admin/charities/$charityId/assigned-users", csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())
        WebUiBuiltInKeywords.verifyEqual(userRes.getStatusCode(), 200)
        List<Donors> users = apiTesting.mapResponseToModel(userRes, User[].class)
        return users
    }

    /**
     * get current assigned users as charity roles of activeCharityId**/
    User getCurrentUserAssigned(String charityId = activeCharityId, String assignedRole) {
        getAssignedUsers(charityId).find({
            for (String role : it.getRoles()) {
                if (StringUtils.equalsIgnoreCase(role, "$assignedRole")) {
                    KeywordUtil.logInfo("User owner role: " + new Gson().toJson(it))
                    return it
                }
            }
        })
    }

    /** get the random current assigned user as charity role
     * but no charity owner*/
    User getAssignedCharityRoleUser(String charityId = activeCharityId) {
        User currentCharityRoleUser = getDefaultAssignedCharityRoleUsers(charityId)
        /*if no existing charity user except owner, new user will be assigned to as Admin role*/
        if (Objects.isNull(currentCharityRoleUser)) {
            def newUser = randomNewUserAssignRole(charityAPITesting.createRandomCharityEmail(), charityId)
            newUser.setRole(CHARITY_USER_ROLE_ADMIN)
            activateCharityOwner(false, true, getAccActivationLink(newUser), newUser)
            currentCharityRoleUser = getDefaultAssignedCharityRoleUsers(charityId)
        }
        return currentCharityRoleUser
    }

    private User getDefaultAssignedCharityRoleUsers(String charityId) {
        List<User> existingCharityUsers = getAssignedUsers(charityId)
        User currentCharityRoleUser = existingCharityUsers.find({
            it.getRoles().find { s -> s.equalsIgnoreCase(CHARITY_USER_ROLE_ADMIN) | s.equalsIgnoreCase(CHARITY_USER_ROLE_REPORTING) | s.equalsIgnoreCase(CHARITY_USER_ROLE_USER)
            }
        })
        currentCharityRoleUser
    }

    UserAssignedRole randomNewUserAssignRole(String userEmail, String charityId = activeCharityId) {
        UserAssignedRole userAssign = new UserAssignedRole()
        userAssign.setEmail(userEmail)
        userAssign.setFirstName(StringUtils.substringBetween(userEmail, "_", "@"))
        userAssign.setLastName(TestDataFactory.findTestData('SignUp/SignUp').getObjectValue('LastName', SIGN_UP_INFOMATION))
        userAssign.setCharityId(charityId)
        return userAssign
    }

    UserAssignedRole newUserAssignFromExistingUser(User existingShouter) {
        UserAssignedRole userAssign = new UserAssignedRole()
        def existingShouterEmail = existingShouter.getEmail()
        userAssign.setEmail(existingShouterEmail)
        userAssign.setCharityId(activeCharityId)
        userAssign.setFirstName(existingShouter.getFirstName())
        userAssign.setLastName(existingShouter.getLastName())
        KeywordUtil.logInfo("UserAssign getting from an existing user: " + gson.toJson(userAssign))
        userAssign
    }

    void activateCharityOwner(boolean isTwoFA = true, boolean isNewUser = false, String activationLink, UserAssignedRole userAssign) {
        'Step: activate user by change pass'
        ActivateUserPage activateUserPage = new ActivateUserPage()
        activateUserPage.activateUser(activationLink, isNewUser)

        'Step: accept changing owner account'
        LoginPage loginPage = new LoginPage()
        def email = userAssign.getEmail()
        if (isTwoFA) {
            loginPage.twoFAStepsLogin(email, G_loginPassword)
        } else loginPage.loginFromHomePage(email, G_loginPassword)
        activateUserPage.acceptChangeOwner(userAssign.getFirstName())
    }

    UserDetail getUserDetail(String userEmail) {
        CSRFTokens csrfTokens = apiTesting.getUserAuthorization(G_saUserNameAPI, G_loginPassword)
        List<User> users = getActiveUserList(csrfTokens)
        User user = users.find({ it -> StringUtils.equalsIgnoreCase(it.getEmail(), userEmail) })
        def userDetailPath = "/admin/donors/${user.getId()}"
        ResponseObject usersRes = apiTesting.sendGetRequestWithCSRF(internal.GlobalVariable.G_baseAPIUrl + userDetailPath, csrfTokens)
        Assert.assertEquals(usersRes.getStatusCode(), 200, "userRes $userDetailPath failed")
        return apiTesting.mapJSONResponseToModel(usersRes, UserDetail)
    }

    List<UserPaymentMethod> getUserPaymentMethod(String userName) {
        CSRFTokens csrfTokens = apiTesting.getUserAuthorization(userName, G_loginPassword)
        def getPaymentMethodPath = "/payment_methods"
        ResponseObject paymentMethodRes = apiTesting.sendGetRequestWithCSRF(internal.GlobalVariable.G_baseAPIUrl + getPaymentMethodPath, csrfTokens)
        Assert.assertEquals(paymentMethodRes.getStatusCode(), 200, "$getPaymentMethodPath failed. Status code: ${paymentMethodRes.getStatusCode()}")
        return apiTesting.mapResponseToModel(paymentMethodRes, UserPaymentMethod[].class)
    }

    boolean isCreditCardSaveDefault(String userName) {
        UserPaymentMethod creditCard = getUserPaymentMethod(userName).find({ it -> it.getType() == "CreditCardPayment" && StringUtils.isNotBlank(it.getCardNumber()) && StringUtils.isNotBlank(it.getCardType()) && StringUtils.isNotBlank(it.getCardExpiry())
        })
        return Objects.nonNull(creditCard)
    }
}
