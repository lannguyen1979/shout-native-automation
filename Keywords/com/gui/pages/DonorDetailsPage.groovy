package com.gui.pages

import com.kms.katalon.core.testobject.TestObject
import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum.LocatorType

public class DonorDetailsPage {

	TestObject donorStatus = common.createTestObject(LocatorType.XPATH.getType(),
	"//strong[span='Status']//following-sibling::div//span")
	TestObject donorName = common.createTestObject(LocatorType.XPATH.getType(),
	"//strong[span='Name']//following-sibling::div")
	TestObject donorEmail = common.createTestObject(LocatorType.XPATH.getType(),
	"//strong[span='Email']//following-sibling::div")
	TestObject donorRole = common.createTestObject(LocatorType.XPATH.getType(),
	"//strong[span='Role']//following-sibling::div")


	void verifyDonorInfo(Map <String, TestObject> actualResult, Map <String, String> expectedResult) {
		common.verifyTextIsEqual(actualResult.get("donorName"), expectedResult.get("donorName"))
		common.verifyTextIsEqual(actualResult.get("donorEmail"), expectedResult.get("donorEmail"))
		common.verifyTextIsEqual(actualResult.get("donorRole"), expectedResult.get("donorRole"))
		common.verifyTextIsEqual(actualResult.get("donorStatus"), expectedResult.get("donorStatus"))
	}

	/* Get Status & Role on Details Page */
	public Map <String, TestObject> getDonorInfo(){
		Map <String, TestObject> donorDetailsInfo = new HashMap<>()
		donorDetailsInfo.put("donorName",donorName)
		donorDetailsInfo.put("donorEmail", donorEmail)
		donorDetailsInfo.put("donorRole",donorRole)
		donorDetailsInfo.put("donorStatus",donorStatus)
		donorDetailsInfo
	}
}