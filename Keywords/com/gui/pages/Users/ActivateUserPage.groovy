package com.gui.pages.Users

import com.core.utilities.common.CommonActions
import com.core.utilities.common.Constants
import com.core.utilities.common.WebObjects
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import static com.core.utilities.common.Constants.LOGIN_PATH
import static internal.GlobalVariable.*

class ActivateUserPage {
	CommonActions common = new CommonActions()
	TestObject btnAcceptOwnerAcc = common.createTestObject("xpath", "//button/*[text()='Accept']")
	String lblWelcomeMsgXpath = "//*[@class='welcome-message'][contains(text(),'%s')]"

	TestObject createTestObjectById(String id){
		return common.createTestObject("xpath", "//*[@id='$id']")
	}

	TestObject createWelcomeMsgObject(String userFirstName){
		common.createTestObject("xpath", String.format(lblWelcomeMsgXpath, userFirstName))
	}

	void activateUser(String activationLink, boolean isNewUser = true, String newPassword = G_loginPassword){
		common.navigateToUrl(activationLink)
		if (isNewUser){
			changePassword(newPassword)
		}
	}

	private void changePassword(String newPassword) {
		common.input(createTestObjectById("password"), newPassword)
		common.click(createTestObjectById("btn-save-new-password"))
		WebObjects.waitForURLReturned(G_baseUrl + LOGIN_PATH, G_maxTimeOut)
	}

	void acceptChangeOwner(String userFirstName){
		common.waitForElementClickable(btnAcceptOwnerAcc, common.timeOut30s)
		common.click(btnAcceptOwnerAcc)
		common.waitForPageToLoad(common.timeOut30s)
		common.waitForElementVisible(createWelcomeMsgObject(userFirstName), common.timeOut30s)
	}
}
