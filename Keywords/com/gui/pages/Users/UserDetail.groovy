package com.gui.pages.Users

import com.core.utilities.common.CommonActions
import com.kms.katalon.core.testobject.TestObject

class UserDetailPage {

    CommonActions common = new CommonActions()
    TestObject btnEdit = common.createTestObject("xpath", "//*[text()='Edit']")
    TestObject drpRole = common.createTestObject("xpath", "//*[@id='role']")
    TestObject optionRole = common.createTestObject("xpath", "//option[text()='%s']") //role name: Admin, Owner, Reporting, User
    String optionRoleXpath = "//option[text()='%s']"
    String btnXpath = "//*[text()='Edit']"

    TestObject createButton(String buttonCaption){
        common.createTestObject("xpath", String.format(btnXpath, buttonCaption))
    }

    TestObject selectUserRole(String userRole){
        common.click(drpRole)
        common.click(common.createTestObject("xpath", String.format(optionRoleXpath, userRole)))
        common.click(createButton("Save"))
    }

    void editUserRole(String role){
        common.click(createButton("Edit"))
        common.waitForElementVisible(drpRole, common.timeOut30s)
    }

}
