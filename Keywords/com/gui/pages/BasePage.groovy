package com.gui.pages

import com.core.utilities.common.CommonActions
import com.kms.katalon.core.testobject.TestObject

class BasePage {

	CommonActions common = new CommonActions()
	TestObject iconLoading = common.createTestObject("xpath", "//*[@class='loading--partial is-show']")

	void waitForIconLoadingNotPresent(String timOutInSecond = common.timeOut30s){
		common.waitForElementNotPresent(iconLoading, timOutInSecond)
	}
}
