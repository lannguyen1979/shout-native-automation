package com.gui.pages.transactions

import com.core.utilities.common.CommonActions
import com.core.utilities.common.Enum
import com.gui.pages.BasePage
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import org.apache.commons.lang3.StringUtils

class TransactionRecurring extends BasePage{
    TestObject dropKeywordValue = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//select[@id='keywordType']/option[text()='%s']")

    TestObject txtSearchArea = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//input[@id='keyword']")

    TestObject btnSearch = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//button[*[text()='Search']]")

    TestObject lblNoResultFound = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//span[contains(.,'There is no transactions available yet')]")

    TestObject cellCharityName = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]/td[contains(@class,'charity')]")

    TestObject cellDonationAmount = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]/td/div[contains(@class,'total-column')]/span")

    TestObject cellTransactionStatus = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(),"//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]/td[contains(@class,'status')]/span/span");

    TestObject cellNextDue = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]/td[contains(@class,'nextDue')]//span[text()]")

    TestObject txnRow = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]")

    TestObject tnsRow = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(),
            "//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]")

    TestObject loadingIndicator = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//div[@class='loading--partial is-show']")

    void selectKeywordType(String keywordType) {
        def newXpath = String.format(dropKeywordValue.findPropertyValue("xpath"), keywordType)
        CommonActions.modifyTestObject(dropKeywordValue, Enum.LocatorType.XPATH.getType(), newXpath)
        CommonActions.click(dropKeywordValue)
    }

    private searchRecurringByPlanId(String keywordType, String keywordValue) {
        selectKeywordType(keywordType)
        CommonActions.input(txtSearchArea, keywordValue)
        CommonActions.click(btnSearch)
        CommonActions.waitForPageToLoad(CommonActions.timeOut30s)
        common.waitForElementNotVisible(iconLoading, common.timeOut30s)
    }

    /*Search transactions, modify objects and then verify transaction info*/
    TestObject getSearchResultByPlanId(String planId, String charityName, String dateSubmitted, String transAmount){
        searchRecurringByPlanId("Plan ID", planId)
        verifyRecurringInfo(charityName, dateSubmitted, transAmount)
        WebUiBuiltInKeywords.verifyElementVisible(txnRow, FailureHandling.STOP_ON_FAILURE)
        return txnRow
    }

    void navigateToRecurringDetail(TestObject txnRow){
        CommonActions.click(txnRow)
        CommonActions.waitForPageToLoad(CommonActions.timeOut30s)
        KeywordUtil.logInfo("Navigating to Transaction Detail page: ${WebUiBuiltInKeywords.getUrl()}")
    }

    private verifyRecurringInfo(String charityName, String dateSubmitted, String transAmount){
        KeywordUtil.logInfo("Expected Amount before submit:" + transAmount)
        def charityNameXPath = "//tr[1]//*[contains(@class,'recurring-table-column__charity')]//div[contains(text(),'$charityName')]"
        def transAmountXPath = "//tr[1]//*[contains(@class,'able-content__col__amount')]//*[contains(text(),'$transAmount')]"
        TestObject cellCharityValue = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), charityNameXPath)
        CommonActions.verifyTestObjectIsDisplayed(cellCharityValue)
        CommonActions.verifyEqualIgnoreCase(cellTransactionStatus, "Active")
//        WebUiBuiltInKeywords.verifyEqual(CommonActions.getText(cellNextDue), dateSubmitted)//Todo: to calculate recurring date from submit date
        TestObject cellDonationAmount = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), transAmountXPath)
        CommonActions.waitForElementVisible(cellDonationAmount, CommonActions.timeOut30s)
        WebUiBuiltInKeywords.verifyEqual(getAmount(), transAmount)
    }

    private String getAmount() {
        return StringUtils.substringAfter(CommonActions.getText(cellDonationAmount), "\$")
    }
}
