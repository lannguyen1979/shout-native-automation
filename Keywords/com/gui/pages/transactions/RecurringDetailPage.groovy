package com.gui.pages.transactions

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum.LocatorType
import com.core.utilities.common.StringHelper
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import org.apache.commons.lang3.StringUtils

class RecurringDetailPage {

    TestObject lblRecipientEmail = common.createTestObject(LocatorType.XPATH.getType(), "(//div[preceding-sibling::strong/span[text()='Email']])[2]")
    TestObject lblEmail = common.createTestObject(LocatorType.XPATH.getType(), "(//div[preceding-sibling::strong/span[text()='Email']])[1]")
    TestObject lblProduct = common.createTestObject(LocatorType.XPATH.getType(), "//div[preceding-sibling::strong/span[text()='Product']]//span[2]")
    TestObject lblEnding = common.createTestObject(LocatorType.XPATH.getType(), "//span[preceding-sibling::span[contains(text(),'Ending')]]")
	TestObject btnCancel = common.createTestObject(LocatorType.XPATH.getType(),"//span[(text()='Cancel Plan')]")
	TestObject btnCancelconfirm = common.createTestObject(LocatorType.XPATH.getType(),"//div[contains(@class,'is-active')]//button[span='Confirm']")
	TestObject lblStatus = common.createTestObject(LocatorType.XPATH.getType(),"//strong[span='Status']/following-sibling::div/span/span")
	
    TestObject createTestObject(String xpath){
        return common.createTestObject(LocatorType.XPATH.getType(), xpath)
    }

    /*
	 objectLable: Charity, Donor Name
	 */
    TestObject createHrefObject(String objectLable){
        return common.createTestObject(LocatorType.XPATH.getType(), "//div[preceding-sibling::strong/span[text()='$objectLable']]/a")
    }


    /*
     objectLable: Plan Id, Recurring Frequency, Date Created, Name
     */
    TestObject createDivObject(String objectLable){
        return common.createTestObject(LocatorType.XPATH.getType(), "//div[preceding-sibling::strong/span[text()='$objectLable']]")
    }

    /*
     objectLable: Donation Amount, Anonymously, Status
     */
    TestObject createSpanObject(String objectLable){
        return common.createTestObject(LocatorType.XPATH.getType(), "//div[preceding-sibling::strong/span[text()='$objectLable']]/span")
    }


    void verifyRecurringInfo(String charityName, String dateSubmitted, Float transAmount, String email, String donorName, String product, String ending, FailureHandling failureHandling = FailureHandling.CONTINUE_ON_FAILURE){
        KeywordUtil.logInfo("Verifying info in Recurring Detail...")
        WebUiBuiltInKeywords.verifyEqual(getTotalAmount(), transAmount) //substring AUD
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(createHrefObject("Charity")), charityName, failureHandling)
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(lblEmail), email, failureHandling)
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(createHrefObject("Donor Name")), donorName, failureHandling)//todo: donor name must get from api
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(lblProduct), product, failureHandling)
        def dateCreatedTxt = getTextFromObject(createDivObject("Date Created"))
        if (StringUtils.isNotEmpty(ending)) {
            WebUiBuiltInKeywords.verifyEqual(StringHelper.getEndingDigits(getTextFromObject(lblEnding)), ending, failureHandling)
            KeywordUtil.logInfo("Payment method is credit card")
        }
        WebUiBuiltInKeywords.verifyEqual(StringUtils.substringBefore(dateCreatedTxt, " "), dateSubmitted, failureHandling)
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(createDivObject("Recurring Frequency")), "Monthly", failureHandling)
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(createDivObject("Name")), donorName, failureHandling)
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(lblRecipientEmail), email, failureHandling)
        common.verifyEqualIgnoreCase(createSpanObject("Status"), "Active", failureHandling)
    }

    String getTotalAmount() {
        def fullAmount = getTextFromObject(createSpanObject("Donation Amount"))
        return fullAmount.substring(fullAmount.indexOf("\$") + 1)
    }

    String getTextFromObject(TestObject to){
        return common.getText(to)
    }
	
	void cancelRecurringPlan(){
		common.click(btnCancel)
		common.waitForElementClickable(btnCancelconfirm, common.getTimeOut30s())
		common.click(btnCancelconfirm)
		common.verifyTextIsEqual(lblStatus, "Cancelled")
	}

}