package com.gui.pages


import com.kms.katalon.core.testobject.TestObject
import com.core.utilities.common.CommonActions
import com.core.utilities.common.Enum
import internal.GlobalVariable

class EmailLoginPage {

	static TestObject txt_EmailAddress = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//input[@id='loginID']")
	static TestObject txt_EmailPassword = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(),"//input[@id='login_password']")
	static TestObject btnLogin = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(),"//input[@id='login_btn']")
	static TestObject img_Gmail = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(),"//img[@alt='Gmail']")
	static TestObject btn_Continue = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(),"//span[text()='Continue']")

	static String timeOut = GlobalVariable.G_maxTimeOut

	static clickGmailIconIfDisplayed(String str_TimeOut) {
		boolean bResult = CommonActions.verifyTestObjectIsDisplayed(img_Gmail)
		if (bResult) {
			CommonActions.click(img_Gmail)
		}
	}

	static clickContinueButtonToVerifyIfDisplayed(String str_TimeOut) {
		boolean bResult = CommonActions.verifyTestObjectIsDisplayed(btn_Continue)
		if (bResult) {
			CommonActions.click(btn_Continue)
		}
	}

	static logInEmail(String email, String password) {
		CommonActions.input(txt_EmailAddress, email)
		CommonActions.input(txt_EmailPassword, password)
		CommonActions.click(btnLogin)
		CommonActions.waitForPageToLoad(timeOut)
		clickGmailIconIfDisplayed(timeOut)
		CommonActions.waitForPageToLoad(timeOut)
		clickContinueButtonToVerifyIfDisplayed(timeOut)
		CommonActions.waitForPageToLoad(timeOut)
	}
}
