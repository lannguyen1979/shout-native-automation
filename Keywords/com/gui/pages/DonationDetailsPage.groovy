package com.gui.pages


import com.core.utilities.models.CreditCard
import com.core.utilities.models.FeesSetting
import com.core.utilities.models.donation.DonationPaymentMethod
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import org.apache.commons.lang3.StringUtils
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum
import com.core.utilities.common.WebObjects

import static com.core.utilities.common.Constants.AMEX_CARD
import static com.core.utilities.common.Constants.MASTER_CARD
import static com.core.utilities.common.Constants.PAYMENT_TYPE_AMEX
import static com.core.utilities.common.Constants.PAYMENT_TYPE_CREDITCARD
import static com.core.utilities.common.Constants.PAYMENT_TYPE_GOOGLEPAY
import static com.core.utilities.common.Constants.PAYMENT_TYPE_MASTER
import static com.core.utilities.common.Constants.PAYMENT_TYPE_PAYPAL
import static com.core.utilities.common.Constants.PAYMENT_TYPE_VISA
import static com.core.utilities.common.Constants.VISA_CARD
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static org.apache.commons.lang3.StringUtils.*


class DonationDetailsPage {
	TestObject inputCreditCardNo = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//input[@name='ccNumber']")
	TestObject inputExpiry = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//input[@name='ccExpiry']")
	TestObject inputCcv = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//input[@name='ccCcv']")

	TestObject inputSavedCreditCardNo = common.createTestObject(Enum.LocatorType.CSS.getType(), "#hasSaved-ccNumber")
	TestObject inputSavedExpiry = common.createTestObject(Enum.LocatorType.CSS.getType(), "#hasSaved-ccExpiry")
	TestObject inputSavedCcv = common.createTestObject(Enum.LocatorType.CSS.getType(), "#hasSaved-ccCcv")
	TestObject inputNoSavedCreditCardNo = common.createTestObject(Enum.LocatorType.CSS.getType(), "#noSaved-ccNumber")
	TestObject inputNoSavedExpiry = common.createTestObject(Enum.LocatorType.CSS.getType(), "#noSaved-ccExpiry")
	TestObject inputNoSavedCcv = common.createTestObject(Enum.LocatorType.CSS.getType(), "#noSaved-ccCcv")

	TestObject inputEmail = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//div[contains(@class,'your-details')]//input[@id='email']")
	TestObject inputFirstName = common.createTestObject(Enum.LocatorType.CSS.getType(), "#firstName")
	TestObject inputLastName = common.createTestObject(Enum.LocatorType.CSS.getType(), "#lastName")


	TestObject inputDedicationFirstName = common.createTestObject(Enum.LocatorType.CSS.getType(), "#dedicationFirstName")
	TestObject inputDedicationLastName = common.createTestObject(Enum.LocatorType.CSS.getType(), "#dedicationLastName")
	TestObject inputOnBehalfEmail = common.createTestObject(Enum.LocatorType.CSS.getType(), "#onBehalfEmail")

	TestObject inputIndividualFirstName = common.createTestObject(Enum.LocatorType.CSS.getType(), "#individualFirstName")
	TestObject inputIndividualLastName = common.createTestObject(Enum.LocatorType.CSS.getType(), "#individualLastName")
	TestObject inputIndividualEmail = common.createTestObject(Enum.LocatorType.CSS.getType(), "#individualEmail")

	TestObject inputNotifierFirstName = common.createTestObject(Enum.LocatorType.CSS.getType(),"#notifiedDedicationFirstName")
	TestObject inputNotifierLastName = common.createTestObject(Enum.LocatorType.CSS.getType(), "#notifiedDedicationLastName")
	TestObject inputNotifierEmail = common.createTestObject(Enum.LocatorType.CSS.getType(), "#notifiedDedicationEmail")
	TestObject inputNotifierMessage = common.createTestObject(Enum.LocatorType.CSS.getType(), "#notifiedDedicationMessage")

	TestObject tabbusinessReceipt = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//div[@class='tab-bar clickable is-outlined']//span[contains(text(),'Business / Organisation')]");
	TestObject inputBusinessName = common.createTestObject(Enum.LocatorType.CSS.getType(), "#businessName")
	TestObject inputBusinessFirstName = common.createTestObject(Enum.LocatorType.CSS.getType(), "#businessFirstName")
	TestObject inputBusinessLastName = common.createTestObject(Enum.LocatorType.CSS.getType(), "#businessLastName")
	TestObject inputBusinessEmail = common.createTestObject(Enum.LocatorType.CSS.getType(), "#businessEmail")

	TestObject btnDonateNow = common.createTestObject(Enum.LocatorType.CSS.getType(), ".donation-actions .btn-text")
	TestObject btnChangePaymentMethod = common.createTestObject(Enum.LocatorType.CSS.getType(), ".saved-payment-methods .payment-method-item__button")
	TestObject btnUseAnotherPaymentMethod = common.createTestObject(Enum.LocatorType.CSS.getType(), ".saved-payment-methods .payment-method-item__use-another")
	TestObject tabPayPal = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(concat(' ', normalize-space(@class), ' '), ' tab-bar ')][child::div[text()='PayPal']]")

	TestObject inputIncludeFees = common.createTestObject(Enum.LocatorType.CSS.getType(), "#includedFees")
	TestObject divPayPalFrame = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[contains(@id,'zoid-paypal-button')]")
	TestObject tabCreditCard = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(concat(' ', normalize-space(@class), ' '), ' tab-bar ')]//*[text()='Credit Card']")

	TestObject btnPayPal = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//div[@class='donation-actions']//div[contains(@id,'paypal-button')]")
	TestObject btnGooglePay = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//button[@aria-label='Google Pay']")

	TestObject lblfrequency = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//div[contains(concat(' ', normalize-space(@class), ' '), ' donation-summary ')]//span[text()='Frequency']/following-sibling::span")

	TestObject chkonBehalf = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[contains(text(),'on behalf of someone')]")
	TestObject inputDedication = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//span[contains(concat(' ', normalize-space(@class), ' '), ' checkbox-sign ')][preceding-sibling::input[@id='dedicationId']]")

	TestObject chkNotify = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//span[contains(concat(' ', normalize-space(@class), ' '), ' checkbox-sign ')][preceding-sibling::input[@id='isNotifiedDedication']]")
	TestObject chkSaveCard = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//span[contains(concat(' ', normalize-space(@class), ' '), ' check-sign ')][preceding-sibling::input[@name='selectedCardId']]")

	TestObject chkissueReceipt = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[contains(text(),'receipt')]/preceding-sibling::input");

	TestObject txtTotal = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[parent::*[preceding-sibling::*/span[text()='Total']]][2]")
	TestObject txtTransactionFees = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(text(), 'Transaction fees')]")
	TestObject btnAddNewCard = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(text(),'Add a new card')]")

	TestObject lblSummarySections = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//div[@class='donation-summary']")

	String txtThankyouEmailXpath = "//*[contains(@class, 'thank-you-donation-page')]//*[contains(text(),'%s')]"
	String inputPaymentTypeXpath = "//*[@class='check-sign'][preceding-sibling::input[@name='selectedPaymentType'][@value='%s']]"
	String tabXpath = "//*[contains(concat(' ', normalize-space(@class), ' '), ' tab-bar ')]//*[text()='%s']"
	String btnLabelXpath = "//button[*[contains(text(),'%s')]]"

	void useAnotherPaymentMethod(String creditCardNumber, String expiry, String ccV){
		common.scrollToObject(btnChangePaymentMethod)
		WebObjects.waitForElementToBeClickableByCss(btnChangePaymentMethod, Integer.parseInt(common.timeOut30s))
		common.delay(common.timeOut1s)
		common.click(btnChangePaymentMethod)
		common.scrollToObject(btnUseAnotherPaymentMethod)
		WebObjects.waitForElementToBeClickableByCss(btnUseAnotherPaymentMethod, Integer.parseInt(common.timeOut30s))
		common.delay(common.timeOut1s)
		common.click(btnUseAnotherPaymentMethod)
		inputCreditCardInfo(creditCardNumber, expiry, ccV)
	}

	private void inputCreditCardInfo(String creditCardNumber, String expiry, String ccV) {
		inputCreditCardInforWithSelectiveObjects(creditCardNumber, expiry, ccV)
	}

	private void inputCreditCardInforWithSelectiveObjects(String creditCardNumber, String expiry, String ccV,
			TestObject txtCreditCardNo = inputCreditCardNo, TestObject txtExpiry = inputExpiry, TestObject txtCcv = inputCcv) {
		common.waitForElementClickable(inputCreditCardNo, common.getTimeOut30s())
		common.input(txtCreditCardNo, creditCardNumber)
		common.input(txtExpiry, expiry)
		common.input(txtCcv, ccV)
	}

	void clickBtnDonateNow () {
		WebObjects.waitForElementToBeClickableByCss(btnDonateNow, Integer.parseInt(common.timeOut30s))
		WebUiBuiltInKeywords.scrollToElement(btnDonateNow, GlobalVariable.G_maxTimeOut)
		common.click(btnDonateNow)
	}

	void selectCreditCard(String creditCardNumber, String expiry, String ccV){
		inputCreditCardInforWithSelectiveObjects(creditCardNumber, expiry, ccV,
				inputCreditCardNo, inputExpiry, inputCcv)
	}
	void selectCreditCardForDonor(String creditCardNumber, String expiry, String ccV){
		inputCreditCardInforWithSelectiveObjects(creditCardNumber, expiry, ccV,
				inputCreditCardNo, inputExpiry, inputCcv)
	}

	void selectPayPal(){
		selectPayPalTab()
		submitPayPal()
	}


	private void submitPayPal() {
		common.scrollToObject(divPayPalFrame)
		common.delay(common.timeOut3s)
		common.waitForElementVisible(divPayPalFrame, '60')
		common.waitForElementPresent(iFramePayPalFromNativeApp, common.timeOut30s)
		WebObjects.waitForFrameToBeAvailableAndSwitchToIt(iFramePayPalFromNativeApp.findPropertyValue("xpath"), GlobalVariable.G_maxTimeOut * 6)
		KeywordUtil.logInfo("after waitForFrameToBeAvailableAndSwitchToIt...")
		TestObject btnPayPal = common.createTestObject("xpath", "//*[@id='paypal-animation-content']")
		common.scrollToObject(btnPayPal)
		common.click(btnPayPal)
	}

	void selectPayPalTab() {
		common.waitForElementVisible(tabPayPal, common.timeOut30s)
		common.scrollToObject(tabPayPal)
		common.delay(common.timeOut1s)
		common.click(tabPayPal)
		common.waitForElementVisible(inputIncludeFees, common.timeOut30s)
	}

	void selectPayPal(float totalWithFees) {
		selectPayPalTab()
		compareTotalAmount(totalWithFees)
		submitPayPal()
	}

	TestObject iFramePayPalFromNativeApp = common.createTestObject("xpath", "//iframe[@title='PayPal']")
	TestObject iDefaultFramePayPal = common.createTestObject("xpath", "//*[@title='ppbutton']")

	void selectPayPalFromNativeApp(float totalWithFees){
		selectPayPal(totalWithFees)
	}

	/*Myself contribution*/

	void inputYourDetailsInfo(String email, String firstName, String lastName, String phoneNumber = GlobalVariable.G_shouterPhone) {
		common.input(inputFirstName, firstName)
		common.input(inputLastName, lastName)
		common.input(inputEmail, email)
		common.input(createToWithId("phone"), phoneNumber)
	}

	void inputDedicationInfo(String email, String firstName, String lastName) {
		inputDedicationName(firstName, lastName)
		inputNotificationInfo(substringBefore(email, "@"), lastName, email, "notify $email for my dedication")
	}


	void inputNotificationInfo(String firstName, String lastName, String email, String message){
		common.click(chkNotify)
		common.input(inputNotifierFirstName, firstName)
		common.input(inputNotifierLastName, lastName)
		common.input(inputNotifierEmail, email)
		common.input(inputNotifierMessage, message)
	}

	void inputIndividualInfo(String email, String firstName, String lastName){
		common.click(chkissueReceipt)
		common.input(inputIndividualEmail, email)
		common.input(inputIndividualFirstName, firstName)
		common.input(inputIndividualLastName, lastName)
	}
	void inputBusinessInfo(String name,String email, String firstName, String lastName){
		common.click(chkissueReceipt)
		common.click(tabbusinessReceipt)
		common.input(inputBusinessName, name)
		common.input(inputBusinessEmail, email)
		common.input(inputBusinessFirstName, firstName)
		common.input(inputBusinessLastName, lastName)
	}

	void verifyThankYouContent(String thankYouEmail){
		common.verifyTestObjectIsDisplayed(common.createTestObject(Enum.LocatorType.XPATH.getType(), String.format(txtThankyouEmailXpath, thankYouEmail)),FailureHandling.STOP_ON_FAILURE)
	}


	void verifyTransactionFees(String expectedTransactionFees){
		common.verifyEquals(txtTransactionFees, expectedTransactionFees)
	}

	void verifyTotal(float expectedTotal){
		common.verifyEquals(txtTotal, expectedTotal)
	}

	void compareTotalAmount(float expectedAmount){
		common.waitForPageToLoad(common.timeOut30s)
		common.scrollToObject(txtTotal)
		def actualValue = common.getText(txtTotal)
		common.verifyFloatNumberIsEqual(Float.parseFloat(actualValue.substring(1, actualValue.length())), expectedAmount)
	}

	String getFrequencyText() {
		return common.getText(lblfrequency)
	}

	private void inputDedicationName(String firstName, String lastName) {
		common.input(inputDedicationFirstName, firstName)
		common.input(inputDedicationLastName, lastName)
	}

	void inputDonationForm(boolean isDedicated, boolean isFromMyself, boolean isGuest, boolean isCreditCardSaved = false, String notifyEmail = donorEmail, String donorEmail, String donorFirstName, String donorLastName, float totalWithFees, DonationPaymentMethod payment) {
		if (isDedicated) {
			'Step: input dedication name'
			inputDedicationInfo(notifyEmail, donorFirstName, donorLastName)
			common.delay(common.getTimeOut2s())
			clickNextStep()
		} else {
			common.click(createBtnLabel("Skip This Step"))
		}
		'Step: input contribution info'
		if (isFromMyself) {
			clickToContributionTab("Myself")
			common.delay(common.getTimeOut2s())
			if (isGuest) {
				inputMyselfContribution(donorEmail, donorFirstName, donorLastName)
			}
		} else {
			inputCompanyContribution("Company Name")
			common.delay(common.getTimeOut2s())
			if (isGuest) {
				inputYourDetailsInfo(donorEmail, donorFirstName, donorLastName)
			}
		}
		'Step: select Payment method'
		if (equalsIgnoreCase(payment.getPaymentMethod(), PAYMENT_TYPE_CREDITCARD)) {
			selectPaymentMethod("creditcard")
			def creditCard = payment.getCreditCard()
			if (java.util.Objects.nonNull(creditCard)) {
				'Step: input credit card info'
				if (isCreditCardSaved) {
					clickAddNewCard()
				}
				inputCreditCardInforWithSelectiveObjects(creditCard.getCardNumber(), creditCard.getExpiryDate(), creditCard.getCcv())
				common.focusOnObject(lblSummarySections)
				compareTotalAmount(totalWithFees)
				clickBtnDonateNow()
			} else {
				KeywordUtil.markFailed("Not set credit card info yet.")
			}
		} else if (equalsIgnoreCase(payment.getPaymentMethod(), PAYMENT_TYPE_PAYPAL)) {
			selectPaymentMethod("paypal")
			compareTotalAmount(totalWithFees)
			submitPayPal()
			KeywordUtil.logInfo("getPayment method: " + payment.getPaymentMethod())
		} else if (equalsIgnoreCase(payment.getPaymentMethod(), PAYMENT_TYPE_GOOGLEPAY)) {
			selectPaymentMethod("googlePay")
			common.click(btnGooglePay)
		}
	}

	private void clickAddNewCard() {
		common.scrollToObject(btnAddNewCard)
		common.delay(common.timeOut1s)
		common.waitForElementClickable(btnAddNewCard, common.timeOut30s)
		common.click(btnAddNewCard)
	}

	private void clickSaveCardBtn() {
		common.scrollToObject(chkSaveCard)
		common.delay(common.getTimeOut1s())
		common.click(chkSaveCard)
	}

	void donateByMySelfWithCreditCard(boolean isGuest = true, boolean isCreditCardSaved = false, String donorEmail, String donorFistName, String donorLastName, float totalFees, String cardType = PAYMENT_TYPE_VISA) {
		DonationPaymentMethod payment = newPaymentMethod(PAYMENT_TYPE_CREDITCARD, cardType)
		inputDonationForm(false, true, isGuest, isCreditCardSaved, donorEmail, donorFistName, donorLastName, totalFees, payment)
	}

	/*Guest donate by credit card with parameters: isDedicated and isFromMyself and other donor info*/

	void donateByGuestWithCreditCard(boolean isDedicated = false, boolean isFromMyself, String donorEmail, String donorFistName, String donorLastName, float totalFees, String cardType = PAYMENT_TYPE_VISA) {
		DonationPaymentMethod payment = newPaymentMethod(PAYMENT_TYPE_CREDITCARD, cardType)
		inputDonationForm(isDedicated, isFromMyself, true, false, donorEmail, donorFistName, donorLastName, totalFees, payment)
	}

	/*Shouter donate by credit card with parameters: isDedicated and isFromMyself and other donor info*/

	void donateByShouterWithCreditCard(boolean isDedicated = false, boolean isCreditCardSaved, boolean isFromMyself, String donorEmail, String donorFistName, String donorLastName, float totalFees, String cardType = PAYMENT_TYPE_VISA) {
		DonationPaymentMethod payment = newPaymentMethod(PAYMENT_TYPE_CREDITCARD, cardType)
		inputDonationForm(isDedicated, isFromMyself, false, isCreditCardSaved, donorEmail, donorFistName, donorLastName, totalFees, payment)
	}

	/*Guest donate by Paypal with parameters: isDedicated and isFromMyself and other donor info*/

	void donateByGuestWithPaypal(boolean isDedicated = false, boolean isFromMyself, String donorEmail, String donorFistName, String donorLastName, float totalFees) {
		DonationPaymentMethod payment = newPaymentMethod(PAYMENT_TYPE_PAYPAL)
		inputDonationForm(isDedicated, isFromMyself, true, false, donorEmail, donorFistName, donorLastName, totalFees, payment)
	}

	/*Shouter donate by Paypal with parameters: isDedicated and isFromMyself and other donor info*/

	void donateByShouterWithPaypal(boolean isDedicated = false, boolean isCreditCardSaved, boolean isFromMyself, String donorEmail, String donorFistName, String donorLastName, float totalFees) {
		DonationPaymentMethod payment = newPaymentMethod(PAYMENT_TYPE_PAYPAL)
		inputDonationForm(isDedicated, isFromMyself, false, isCreditCardSaved, donorEmail, donorFistName, donorLastName, totalFees, payment)
	}

	private void selectPaymentMethod(String paymentMethod) {
		def chbCreditCard = createPaymentTypeTo(paymentMethod)//creditcard
		common.waitForElementVisible(chbCreditCard, common.timeOut30s)
		common.scrollToObject(chbCreditCard)
		common.delay(common.timeOut1s)
		common.click(chbCreditCard)
	}

	private DonationPaymentMethod newPaymentMethod(String paymentMethod, String cardType = null) {
		DonationPaymentMethod payment = new DonationPaymentMethod()
		if (StringUtils.equalsIgnoreCase(paymentMethod, PAYMENT_TYPE_CREDITCARD)) {
			payment.setPaymentMethod(PAYMENT_TYPE_CREDITCARD)
			CreditCard creditCard = newCreditCardFromDataFile(cardType)
			payment.setCreditCard(creditCard)
		} else if (StringUtils.equalsIgnoreCase(paymentMethod, PAYMENT_TYPE_PAYPAL)) {
			payment.setPaymentMethod(PAYMENT_TYPE_PAYPAL)
		}
		payment
	}

	private CreditCard newCreditCardFromDataFile(String paymentMethod) {
		CreditCard creditCard = new CreditCard()
		switch (paymentMethod) {
			case PAYMENT_TYPE_VISA:
				creditCard.setCardNumber(findTestData('MasterData/CreditCards').getObjectValue('CreditCardNo', VISA_CARD))
				creditCard.setExpiryDate(findTestData('MasterData/CreditCards').getObjectValue('Expiry', VISA_CARD))
				creditCard.setCcv(findTestData('MasterData/CreditCards').getObjectValue('CCV', VISA_CARD))
				creditCard.setCardType(PAYMENT_TYPE_VISA)
				return creditCard
				break
			case PAYMENT_TYPE_MASTER:
				creditCard.setCardNumber(findTestData('MasterData/CreditCards').getObjectValue('CreditCardNo', MASTER_CARD))
				creditCard.setExpiryDate(findTestData('MasterData/CreditCards').getObjectValue('Expiry', MASTER_CARD))
				creditCard.setCcv(findTestData('MasterData/CreditCards').getObjectValue('CCV', MASTER_CARD))
				creditCard.setCardType(PAYMENT_TYPE_MASTER)
				return creditCard
				break
			case PAYMENT_TYPE_AMEX:
				creditCard.setCardNumber(findTestData('MasterData/CreditCards').getObjectValue('CreditCardNo', AMEX_CARD))
				creditCard.setExpiryDate(findTestData('MasterData/CreditCards').getObjectValue('Expiry', AMEX_CARD))
				creditCard.setCcv(findTestData('MasterData/CreditCards').getObjectValue('CCV', AMEX_CARD))
				creditCard.setCardType(PAYMENT_TYPE_AMEX)
				return creditCard
				break
		}
	}

	private void inputMyselfContribution(String email, String firstName, String lastName) {
		clickToContributionTab("Myself")
		inputYourDetailsInfo(email, firstName, lastName)
	}

	private void clickToContributionTab(String tabName) {
		def tabMyself = createTabTo(tabName)
		common.scrollToObject(tabMyself)
		common.delay(common.getTimeOut1s())
		common.click(tabMyself)
	}

	private void inputCompanyContribution(String companyName) {
		clickToContributionTab("Company")
		inputCompanyInfo(companyName)
	}

	void inputCompanyInfo(String companyName) {
		common.input(createToWithId("companyName"), companyName)
	}

	/*paymentType: creditcard, paypal, googlePay*/

	TestObject createPaymentTypeTo(String paymentType) {
		return common.createTestObject("xpath", String.format(inputPaymentTypeXpath, paymentType))
	}

	/*paymentType: Myself, Company, PayPal*/

	TestObject createTabTo(String tabName) {
		return common.createTestObject("xpath", String.format(tabXpath, tabName))
	}


	TestObject createToWithId(String id) {
		return common.createTestObject("xpath", String.format("//*[@id='%s']", id))
	}

	/*Next Step, Skip This Step*/

	TestObject createBtnLabel(String btnLabel) {
		return common.createTestObject("xpath", String.format(btnLabelXpath, btnLabel))
	}


	private void clickNextStep() {
		def btnNextStep = createBtnLabel("Next Step")
		common.waitForElementVisible(btnNextStep, common.getTimeOut30s())
		common.scrollToObject(btnNextStep)
		common.waitForElementClickable(btnNextStep, common.getTimeOut30s())
		common.click(btnNextStep)
	}

	float countTotalFees(String donationAmount, String paymentType, String product = "fund") {
		FeesSetting feesSetting = new FeesSetting()
		def totalWithFees = feesSetting.calculateTotalWithFees(donationAmount, feesSetting.getFeesSettings(paymentType, product))
		totalWithFees
	}

	void inputSignup(String firstName, String lastName, String email, String password) {
		common.input(createToWithId("signUpEmail"), email)
		common.input(createToWithId("signUpFirstName"), firstName)
		common.input(createToWithId("signUpLastName"), lastName)
		common.input(createToWithId("signUpPassword"), password)
		clickNextStep()
	}
}
