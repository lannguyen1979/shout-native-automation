package com.gui.pages

import com.core.utilities.common.Enum.LocatorType

import com.kms.katalon.core.testobject.TestObject
import com.core.utilities.common.CommonActions as common

public class SearchResultPage {
	
	TestObject lblCharityName = common.createTestObject(LocatorType.XPATH.getType(),
	"//div[contains(@class,'charity card__title')]//span[text()='%s']")
	
	TestObject getlblCharityNameResult(String charityName){
		return common.createTestObject(LocatorType.XPATH.getType(),String.format(lblCharityName.findPropertyValue("xpath"), charityName))
	}
	

}
