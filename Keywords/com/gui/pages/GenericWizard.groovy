package com.gui.pages

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum

public class GenericWizard {

	TestObject lblWizardItems = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[@class='partial-loading-wrapper']//*[@class='menu-bar']//span[contains(text(),'%s')]")
	TestObject divLoading = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[@class='loading--partial is-show']/*[@class='spinner']")
	TestObject btnContinue = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//h3/span[text()='%s']//ancestor::form//button[@id='btnNextSubmit']")
	TestObject fieldSetActive = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//fieldset[contains(@class,'active')]")
	def navObjectXpath = lblWizardItems.findPropertyValue("xpath")

	TestObject createNavObject(String navTxt) {
		return common.createTestObject(Enum.LocatorType.XPATH.getType(), String.format(navObjectXpath, navTxt))

	}

	void selectStepInWizard(String pageName){
		TestObject newLblWizardItems = createNavObject(pageName)
		common.delay("1")
		WebUiBuiltInKeywords.waitForElementNotVisible(divLoading, Integer.parseInt(common.timeOut2s), FailureHandling.OPTIONAL)
		WebUiBuiltInKeywords.waitForElementClickable(newLblWizardItems, Integer.parseInt(common.timeOut3s), FailureHandling.OPTIONAL)
		common.click(newLblWizardItems)
		common.waitForElementPresent(fieldSetActive, common.timeOut30s)
	}

	void navigateToNextStep(String pageName){
		String str_newxpath = String.format(btnContinue.findPropertyValue("xpath"), pageName)
		common.modifyTestObject(btnContinue,Enum.LocatorType.XPATH.getType(), str_newxpath)
		common.delay(common.getTimeOut1s())
		common.waitForElementClickable(btnContinue,common.getTimeOut30s())
		common.click(btnContinue)
	}
}
