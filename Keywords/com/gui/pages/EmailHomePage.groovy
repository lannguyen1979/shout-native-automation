package com.gui.pages


import com.kms.katalon.core.testobject.TestObject
import com.core.utilities.common.CommonActions as imp
import com.core.utilities.common.Enum
import internal.GlobalVariable

class EmailHomePage {
	static String timeOut = GlobalVariable.G_maxTimeOut
	static final FIRST_WORD_EMAIL_TITLE = "Receipt"
	static final SECOND_WORD_EMAIL_TITLE = "for"
	static TestObject chk_SelectAllEmailOnPage = imp.createTestObject(Enum.LocatorType.XPATH.getType(),"//span[@role='checkbox']")
	static TestObject lnk_SelectAllEmailInbox = imp.createTestObject(Enum.LocatorType.XPATH.getType(),
			"//span[[contains(concat(' ', normalize-space(@class), ' '), ' x8 ')] and @tabindex='0' and @role='link']")

	static TestObject btn_DeleteEmail = imp.createTestObject(Enum.LocatorType.XPATH.getType(),"//div[@aria-label='Delete']")
	static TestObject img_ProfileIcon = imp.createTestObject(Enum.LocatorType.XPATH.getType(),"//a[contains(@aria-label,'Google Account:')]")
	static TestObject btn_LogOut = imp.createTestObject(Enum.LocatorType.XPATH.getType(),"//a[text()='Sign out']")
	static TestObject lbl_NoNewMail = imp.createTestObject(Enum.LocatorType.XPATH.getType(),"//td[text()='No new mail!']")
	static TestObject btn_OKConfirmBulkAction = imp.createTestObject(Enum.LocatorType.XPATH.getType(),"//span[text()='Confirm bulk action']/ancestor::div//div/button[text()='OK']")
	static TestObject chk_EmailSelection = imp.createTestObject(Enum.LocatorType.XPATH.getType(), "//table/tbody/tr/td/div[@role='checkbox']")
	static TestObject lnk_EmailTitle = imp.createTestObject(Enum.LocatorType.XPATH.getType(), "//table//tbody//tr[1]/td[6]//span")
	static TestObject iconRefresh = imp.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@aria-label='Refresh']")


	static clickSelectAllEmailInBoxLinkIfDisplayed(String str_TimeOut) {
		boolean bResult = imp.verifyTestObjectIsDisplayed(lnk_SelectAllEmailInbox)
		if (bResult) {
			imp.click(lnk_SelectAllEmailInbox)
		}
	}

	static clickOKConfirmBulkActionIfDisplayed(String str_TimeOut) {
		boolean bResult = imp.verifyTestObjectIsDisplayed(btn_OKConfirmBulkAction)
		if (bResult) {
			imp.click(btn_OKConfirmBulkAction)
		}
	}

	static int countNumberOfExistingEmails(String str_TimeOut) {
		return imp.getNumberOfTestObject(chk_EmailSelection, str_TimeOut)
	}

	static String getTransactionEmailTitle(){
		return imp.getText(lnk_EmailTitle)
	}

	private static void refreshMailBox() {
		imp.waitForElementVisible(iconRefresh, timeOut)
		imp.click(iconRefresh)
		imp.waitForPageToLoad(timeOut)
	}

	static String getTransactionIdFromEmailTitle (String str_EmailTitle) {
		if (org.apache.commons.lang3.StringUtils.isEmpty(str_EmailTitle)) return null
		String str_TransactionID
		String str_Regex = "[^\\.0123456789]" // Remove all string before number
		String str_Regex1 = "^0+" // Remove all 0 before number
		String str_ReplacedText = ""

		int startIndex = str_EmailTitle.indexOf(FIRST_WORD_EMAIL_TITLE)
		int endIndex
		String str_SubString = str_EmailTitle.substring(startIndex)

		//Double check subString has "for"
		if (str_SubString.contains(SECOND_WORD_EMAIL_TITLE)) {
			endIndex = str_SubString.indexOf(SECOND_WORD_EMAIL_TITLE)
			str_TransactionID = str_SubString.substring(0, endIndex).trim()
		} else {
			str_TransactionID = str_SubString.trim()
		}
		//Re-Format TransactionID
		str_TransactionID = imp.replaceAllByRegex(str_TransactionID, str_Regex, str_ReplacedText)
		str_TransactionID = imp.replaceAllByRegex(str_TransactionID, str_Regex1, str_ReplacedText)
		return str_TransactionID
	}

	static deleteAllEmailInBox() {
		if (countNumberOfExistingEmails(timeOut) > 0) {
			imp.click(chk_SelectAllEmailOnPage)
			imp.click(btn_DeleteEmail)
			imp.waitForElementPresent(lbl_NoNewMail, timeOut)
		}
	}

	static signOutEmail() {
		imp.click(img_ProfileIcon)
		imp.click(btn_LogOut)
		imp.waitForPageToLoad(timeOut)
	}

}
