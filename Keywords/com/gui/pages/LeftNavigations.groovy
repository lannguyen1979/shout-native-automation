package com.gui.pages

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum
import com.kms.katalon.core.testobject.TestObject

class LeftNavigations {

	TestObject menuGiving = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(concat(' ', normalize-space(@class), ' '), ' sidebar__menu-wrapper ')]//span[text()='Giving']")
	TestObject menuTransactions = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(concat(' ', normalize-space(@class), ' '), ' sidebar__menu-wrapper ')]//span[text()='Transactions']")
	TestObject menuCharities = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(concat(' ', normalize-space(@class), ' '), ' sidebar__menu-wrapper ')]//span[text()='Charities']")
	TestObject menuCharityListing = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(concat(' ', normalize-space(@class), ' '), ' sidebar__menu-wrapper ')]//span[text()='Listing']")

	TestObject navObject = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[contains(concat(' ', normalize-space(@class), ' '), ' sidebar__menu-wrapper ')]//span[text()='%s']")

	TestObject subNavObject = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[text()='%s']")

	TestObject subDonationHistory = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[text()='Donation History']")
	
	TestObject subTransactionsHistory = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[text()='Donations']")
	
	TestObject subTransactionsRecurring = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[text()='Recurring']")

	def navObjectXpath = navObject.findPropertyValue("xpath")
	def subNavObjectXpath = subNavObject.findPropertyValue("xpath")

	void navigateToMenuDoationHistory(){
		common.click(menuGiving)
		common.click(subDonationHistory)
	}

	void navigateToMenuTransactionsHistory(){
        navigateToTransactions(subTransactionsHistory)
	}
	void navigateToMenuTransactionsRecurring(){
        navigateToTransactions(subTransactionsRecurring)
	}

    private void navigateToTransactions(TestObject subTransactionMenu) {
        common.waitForElementVisible(menuTransactions, common.getTimeOut30s())
        common.click(menuTransactions)
        common.waitForElementVisible(subTransactionMenu, common.getTimeOut30s())
        common.click(subTransactionMenu)
    }

    void navigateToCharityListing(){
		common.click(menuCharities)
	}

	TestObject createNavObject(String navName) {
		return common.modifyTestObject(navObject, Enum.LocatorType.XPATH.getType(), String.format(navObjectXpath, navName))
	}

	TestObject createSubNavObject(String navName) {
		return common.modifyTestObject(subNavObject, Enum.LocatorType.XPATH.getType(), String.format(subNavObjectXpath, navName))
	}

	void navigateToShoutButton() {
		common.click(createNavObject("Fundraising"))
		common.click(createSubNavObject("ShoutButton"))
	}

	void navigateToFundraising() {
		common.click(createNavObject("Fundraising"))
		common.click(createSubNavObject("ShoutFund"))
	}

	void navigateToSAUsers() {
		common.click(createNavObject("SA Users"))
	}

	void navigateToUsers() {
		common.click(createNavObject("Users"))
	}
	
	void navigateToFundraiser() {
		common.click(createNavObject("Fundraisers"))
		//common.click(createSubNavObject("ShoutFund"))
	}
}
