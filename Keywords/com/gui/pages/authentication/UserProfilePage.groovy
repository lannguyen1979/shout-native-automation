package com.gui.pages.authentication

import com.core.utilities.common.CommonActions
import static com.core.utilities.common.Constants.*
import com.core.utilities.common.Enum
import com.core.utilities.models.UserProfile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import org.apache.commons.lang3.StringUtils


class UserProfilePage {

	CommonActions common = new CommonActions()

	/*
	 * id: firstName, lastName, email, phoneNumber*/
	TestObject createTestObject(String id){
		return common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@id='$id'][@value]")
	}

	String getAttributeValue(TestObject to) {
		common.waitForElementVisible(to, common.timeOut30s)
		def value = WebUiBuiltInKeywords.getAttribute(to, "value", FailureHandling.STOP_ON_FAILURE)
		int retry= 5
		while(StringUtils.isEmpty(value) && retry>0) {
			KeywordUtil.logInfo("retry getting attribute value $to: $value $retry time(s)")
			common.delay(common.timeOut1s)
			value = WebUiBuiltInKeywords.getAttribute(to, "value", FailureHandling.STOP_ON_FAILURE)
			retry--
		}
		return value
	}

	UserProfile getUserProfile() {
		UserProfile profile = new UserProfile()
		common.navigateToUrl(GlobalVariable.G_baseUrl + USER_PROFILE_PATH)
		common.waitForPageToLoad(common.timeOut30s)
		profile.setFirstName(getAttributeValue(createTestObject("firstName")))
		profile.setLastName(getAttributeValue(createTestObject("lastName")))
		profile.setEmail(getAttributeValue(createTestObject("email")))
		profile.setPhoneNumber(getAttributeValue(createTestObject("phoneNumber")))
		return profile
	}
}
