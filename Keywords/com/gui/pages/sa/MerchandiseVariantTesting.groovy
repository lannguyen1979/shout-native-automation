package com.gui.pages.sa

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum
import com.kms.katalon.core.testobject.TestObject
import com.core.utilities.model.merchandise.Merchandise
import com.core.utilities.model.merchandise.StockValue

public class MerchandiseVariantTesting {
	
	TestObject btnAddMer = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//div[contains(@class,'section-title')]//button[span='+ Add New Merchandise']")
	
	//merchandise general
	TestObject txtMerchandiseName = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//div[@class='merchandise-form__details']//input[@id='name']")
	TestObject txtMerchandiseAmount = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//div[@class='merchandise-form__details']//input[@id='amount']")
	String xpathshoutProduct = "//h6[contains(text(),'Shout products')]//following-sibling::div//input[@id='%s']"
	String xpathtaxSetting = "//h6[contains(text(),'Tax settings')]//following-sibling::div//input[@id='%s']"
	TestObject btnNext = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//button[span='Next']")
	TestObject btnSave = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//button[span='Save']")
	
	//merchandise variant settings
	TestObject enableMerchandiseVariant = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='hasVariants']")
	
	
	//merchandise stock limit
	TestObject enableStockSettings = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='enableStockLimit']")
	TestObject txtStockAmount = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='stockAmount']")
	TestObject txtStockAlert = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='lowStockAlertAmount']")
	
	
	//merchandise listing page
	//merchandise detail page
	
	Merchandise mer = new Merchandise()
	StockValue stockvalue = new StockValue()
	
	private TestObject selectShoutProduct(String shoutProduct) {
		return common.createTestObject("xpath", String.format(xpathshoutProduct, shoutProduct))
	}

	private TestObject selectTaxSettings(String taxSettings) {
		return common.createTestObject("xpath", String.format(xpathtaxSetting, taxSettings))
	}

	void inputMerchandiseGeneral(String shoutProduct, String taxSettings, String merchandiseName, String merchandiseAmount) {
		common.input(txtMerchandiseName, merchandiseName)
		common.input(txtMerchandiseAmount, merchandiseAmount)
		selectShoutProduct(shoutProduct)
		selectTaxSettings(taxSettings)
	}

	void addNewMerchandiseVariant(String shoutProduct, String taxSettings, boolean isEnableMerchandiseVariant,
     boolean isEnableStockLimit, String merchandiseName, String merchandiseAmount, String stockAmount, String stockAlert) {
		common.click(btnAddMer)
		common.waitForElementNotPresent(btnAddMer, common.getTimeOut30s())
		inputMerchandiseGeneral(shoutProduct, taxSettings, merchandiseName, merchandiseAmount)
		common.scrollToClick(btnNext)
		//common.click(btnNext)
		common.waitForElementPresent(txtStockAmount, common.getTimeOut30s())
		if (isEnableMerchandiseVariant) {
			inputMerchandiseVariant()
		}
		else {
			if (isEnableStockLimit){
			inputStocksettings(stockAmount, stockAlert)
		}
		}
		common.scrollToClick(btnSave)
	}

	void inputMerchandiseVariant() {
		
	}

	void inputStocksettings(String stockAmount, String stockAlert) {
		common.click(enableStockSettings)
		common.waitForElementPresent(txtStockAmount,common.getTimeOut30s())
		common.input(txtStockAmount, stockAmount)
		common.input(txtStockAlert, stockAlert)		
	}
	
	void verifyMerchandiseListing() {
		
	}
	
	void verifyMerchandiseDetail() {
		
	}
}
