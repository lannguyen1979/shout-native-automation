package com.gui.pages.sa

import com.core.utilities.common.CommonActions
import com.core.utilities.common.Enum
import com.core.utilities.webservice.charities.CharityAPITesting
import com.gui.pages.LoginPage
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

class AddAUser {
	CommonActions common = new CommonActions()
	CharityAPITesting charity = new CharityAPITesting()
	/*
	 * ids:
	 * email
	 * role
	 * btn-submit (text: Continue, Done)
	 * */
	TestObject tdUserFullName = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//span[text()='%s']")
	TestObject btnChangeOwnserShip = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[@class='modal__content']//*[contains(text(),'Change Owner')]")
	String tdUserXpath = "//td[*[text()='%s']][preceding-sibling::td//*[text()='%s']]" //1st: td column name: User Name, Role, 2FA, Status
	String tdSubMenu = "//td[@class='table-content__col__actions'][preceding-sibling::td[div/*[text()='%s']]]" //parameter is Charity name

	TestObject createTestObjectWithId(String id) {
		return CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(),
				"//*[contains(@class,'edit-charity-detail-modal is-active')]//*[@id='$id']")
	}

	TestObject createtdUserFullName(String fullName) {
		return CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//span[text()='$fullName']")
	}

	TestObject createTdUserObject(String userName, String tdValue) {
		return CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), String.format(tdUserXpath, tdValue, userName))
	}

	TestObject createTdSubMenuObject(String userName) {
		return CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), String.format(tdSubMenu, userName))
	}

	TestObject optRoleName = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//select[@id='role'][following-sibling::label[text()='Select role']]/option[contains(text(),'Owner')]")

	String optRoleNameXpath = "//select[@id='role'][following-sibling::label[text()='Select role']]/option[contains(text(),'%s')]"

	void selectRoleDropdown(String role = "Owner"){
		TestObject drpTo = createTestObjectWithId("role")
		CommonActions.waitForElementVisible(drpTo, CommonActions.getTimeOut30s())
		CommonActions.click(drpTo)
		TestObject optRoleName = common.createTestObject("xpath", String.format(optRoleNameXpath, role))
		CommonActions.scrollToObject(optRoleName)
		CommonActions.click(optRoleName)
	}

	void addUserSuccessfully(String shouterEmail, String firstName, String lastName, String userRole = "Owner") {
		inputUserEmail(shouterEmail)
		common.input(createTestObjectWithId("firstName"), firstName)
		common.input(createTestObjectWithId("lastName"), lastName)
		selectRoleDropdown(userRole)
		common.click(createTestObjectWithId("btn-submit"))
		common.waitForPageToLoad("${GlobalVariable.G_maxTimeOut}")
	}

	void addExistingUserSuccessfully(String shouterEmail, String userRole = "Owner") {
		inputUserEmail(shouterEmail)
		selectRoleDropdown(userRole)
		common.click(createTestObjectWithId("btn-submit"))
		common.waitForPageToLoad("${GlobalVariable.G_maxTimeOut}")
	}

		private void inputUserEmail(email) {
			common.input(createTestObjectWithId("email"), email)
			common.click(createTestObjectWithId("btn-submit"))
		}

	void verifyUserAddedToCharityDetail(String fullUserName, String expectedStatus = "Pending", String expectedRole = "Owner") {
		TestObject tdUserObject = createtdUserFullName(fullUserName)
		common.waitForElementVisible(tdUserObject, "${GlobalVariable.G_maxTimeOut}")
		WebUiBuiltInKeywords.verifyElementPresent(tdUserObject, GlobalVariable.G_maxTimeOut)
		createTdUserObject(fullUserName, expectedStatus)
		createTdUserObject(fullUserName, expectedRole)

	}

	void navigateToSAActiveCharityDetail() {
		String charityId = charity.getActiveCharity().getId()
		new LoginPage().twoFALogin(GlobalVariable.G_saUserName, GlobalVariable.G_loginPassword)
		CommonActions.navigateToUrl(GlobalVariable.G_baseUrl + "/admin/charities/$charityId?tab=detail")
	}

	void closeChangeOwnerShip(){
		common.click(btnChangeOwnserShip)
	}

	void navigateToEditUser(String userFullName){
		common.click(createTdSubMenuObject(userFullName))
	}
}
