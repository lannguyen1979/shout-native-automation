package com.gui.pages.sa

import com.core.utilities.common.CommonActions
import com.core.utilities.common.DateTimeUtils
import com.core.utilities.common.Enum.LocatorType
import com.core.utilities.models.sa.CharityRefine
import com.core.utilities.models.sa.FundraiserRefine
import com.core.utilities.models.sa.UserRefine
import com.core.utilities.models.transaction.TransactionRefine
import com.core.utilities.webservice.CharityAPIRefineTesting
import com.core.utilities.webservice.FundraiserAPITesting
import com.core.utilities.webservice.TransactionAPITesting
import com.core.utilities.webservice.UserAPITesting
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

public class Dashboard {
	CommonActions common= new CommonActions()
	TransactionAPITesting txnTest = new TransactionAPITesting()
	TransactionRefine txnconditions = new TransactionRefine()
	CharityRefine chconditions = new CharityRefine()
	FundraiserRefine fundConditions = new FundraiserRefine()
	CharityAPIRefineTesting chApi = new CharityAPIRefineTesting()
	FundraiserAPITesting fundApi = new FundraiserAPITesting()
	UserAPITesting userApi = new UserAPITesting()
	UserRefine userconditions = new UserRefine()

	TestObject createTestObjectWithText(String titleHeader) {
		return common.createTestObject(LocatorType.XPATH.getType(),
				"//div[span='$titleHeader']/following-sibling::div/span")
	}

	TestObject createTestObjectNumberWithText(String titleHeader) {
		return common.createTestObject(LocatorType.XPATH.getType(),
				"//div[span='$titleHeader']/following-sibling::div")
	}

	TestObject createTestObjectProductWithText(String productName){
		return common.createTestObject(LocatorType.XPATH.getType(),
				"//div[text()='$productName']/following-sibling::div/span")
	}

	TestObject txtFromDate = common.createTestObject(LocatorType.XPATH.getType(), "//input[@id='dateRangePicker_startDate']")

	TestObject txtToDate = common.createTestObject(LocatorType.XPATH.getType(), "//input[@id='dateRangePicker_endDate']")

	String getTotalAmount(String title) {
		def fullAmount = common.getText(createTestObjectWithText(title))
		return fullAmount.substring(fullAmount.indexOf("\$") + 1)
	}

	TestObject createTabObject(String tabHeader){
		return common.createTestObject(LocatorType.XPATH.getType(),
				"//div[contains(@class,'tab-bar')]//span[text()='$tabHeader']")
	}

	void verifytheNumberOfTxns(String expectedTxnsCount, String title){
		String actualTxnsCount = getTotalAmount(title)
		WebUiBuiltInKeywords.verifyEqual(actualTxnsCount.replaceAll(",", ""), expectedTxnsCount, FailureHandling.CONTINUE_ON_FAILURE)
	}

	void selectTab(String tabheader){
		common.click(createTabObject(tabheader))
	}

	void verifySAdashboardByDateTime(String fromDate, String toDate){

		txnconditions.setfromDate(fromDate)
		txnconditions.settoDate(toDate)
		chconditions.setfromDate(fromDate)
		chconditions.settoDate(toDate)
		fundConditions.setfromDate(fromDate)
		fundConditions.settoDate(toDate)

		String totalSuccessfulTransaction = txnTest.getTotalSuccessfullTxns(txnconditions)
		CommonActions.verifyValueIsEqual(createTestObjectWithText('No. of transactions'), Float.parseFloat(totalSuccessfulTransaction))

		String totalLiveFundraiser = fundApi.countLiveFundraiserbyFundraiserStatus(fundConditions)
		CommonActions.verifyValueIsEqual(createTestObjectWithText('Active fundraisers'), Float.parseFloat(totalLiveFundraiser))

		String totalActiveCharities = chApi.countActiveCharitybyCHStatus(chconditions)
		CommonActions.scrollToObject(createTestObjectWithText('Total no. of charities'))
		CommonActions.verifyValueIsEqual(createTestObjectWithText('Total no. of charities'), Float.parseFloat(totalActiveCharities))

		String totalFutureFundraiser = fundApi.countFutureFundraiserbyFundraiserStatus(fundConditions)
		KeywordUtil.logInfo("total là " + totalFutureFundraiser)
		//CommonActions.delay(CommonActions.getTimeOut2s())
		CommonActions.scrollToObject(createTestObjectWithText('Future fundraisers'))
		CommonActions.verifyValueIsEqual(createTestObjectWithText('Future fundraisers'), Float.parseFloat(totalFutureFundraiser))

		Float totalShouter = (userApi.countUserbyShouterRole(fromDate, toDate) - userApi.countUserbyCharityRole(fromDate, toDate))
		CommonActions.verifyValueIsEqual(createTestObjectWithText('Total no. of shouters'), totalShouter)
	}
	void verifySAdashboardAllTime(){
		String allTimeFromDate = "2010-08-01"
		String allTimeToDate = "2035-08-01"
		verifySAdashboardByDateTime(allTimeFromDate, allTimeToDate)
		CommonActions.verifyEqualIgnoreCase(createTestObjectNumberWithText('No. of new charities'),'N/A')
		CommonActions.verifyEqualIgnoreCase(createTestObjectNumberWithText('No. of new shouters'),'N/A')
	}

	void verifySAdashboardCurrentFY(){
		String currentyear = DateTimeUtils.getCurrentDate("yyyy")
		int lastyear = DateTimeUtils.getTimeMinusYears("yyyy", currentyear, 1)
		String currentFYToDate = currentyear + "-10-01"
		String currentFYFromDate = lastyear + "-09-30"
		verifySAdashboardByDateTime(currentFYFromDate, currentFYToDate)
	}

	void verifySAdashboardLastFY(){
		int lastyear = DateTimeUtils.getTimeMinusYears("yyyy", DateTimeUtils.getCurrentDate("yyyy"), 1)
		int last2year = DateTimeUtils.getTimeMinusYears("yyyy", DateTimeUtils.getCurrentDate("yyyy"), 2)
		String lastFYToDate = lastyear + "-10-01"
		String lastFYFromDate = last2year + "-09-30"
		verifySAdashboardByDateTime(lastFYToDate, lastFYFromDate)
	}

	void verifySAdashboardbyFilterDate(){
		String fromDate = common.getText(getTxtFromDate())
		String toDate = common.getText(getTxtToDate())
		verifySAdashboardByDateTime(fromDate, toDate)
	}
}