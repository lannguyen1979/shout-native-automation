package com.gui.pages.sa

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum
import org.openqa.selenium.WebElement

public class CommonForm {

	TestObject btnSave = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//button[@type='submit']")

	void saveEditform(){
		common.scrollToObject(btnSave)
		common.click(btnSave)
	}
}
