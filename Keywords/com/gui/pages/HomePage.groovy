package com.gui.pages

import com.core.utilities.common.CommonActions
import com.core.utilities.common.Enum
import com.core.utilities.common.WebObjects
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

class HomePage {

	TestObject btnLogin = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//button/*[text()='Log In']")
	TestObject btnSignUp = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//button/*[text()='Sign Up']")
	TestObject menuIcon = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(),
			"//div[contains(@class,'profile__nav-name')]")
	static TestObject toScripts = CommonActions.createTestObject(Enum.LocatorType.XPATH.getType(), "//script")

	CommonActions common = new CommonActions()

	void navigateToLoginPage(){
		common.navigateToUrl(GlobalVariable.G_baseUrl)
		common.waitForElementVisible(btnLogin, common.timeOut30s)
		common.click(btnLogin)
		common.waitForPageToLoad(common.timeOut30s)
	}

	void navigateToMenuIcon() {
		navigateToHomePage()
		CommonActions.waitForElementVisible(menuIcon, CommonActions.timeOut30s)
		CommonActions.click(menuIcon)
	}

	private void navigateToHomePage() {
		CommonActions.navigateToUrl(GlobalVariable.G_baseUrl)
		WebUiBuiltInKeywords.waitForElementPresent(toScripts, GlobalVariable.G_maxTimeOut)
	}
}
