package com.gui.pages

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum
import com.core.utilities.common.WebObjects as webobj
import com.kms.katalon.core.testobject.TestObject

import internal.GlobalVariable

class SignUpPage {

	TestObject tabDonorSignUp = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[.='Donor']/../..")

	TestObject tabCharitySignUp = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[.='Charity']/../..")

	TestObject txtFirstNameDonorSignUp = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='firstName']")

	TestObject txtLastNameDonorSignUp = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='lastName']")

	TestObject txtEmailDonorSignUp = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='email']")

	TestObject txtPasswordSignUp= common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='password']")

	TestObject btnSignUp = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//button[@id='btn-signUp']")

	TestObject errorMessageEmailexists = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//div[@class='notification__content']//span[contains(text(),'is already in use')]")

	TestObject genericLogOutLink = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[text()='Log Out']")

	TestObject menuIcon = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[@class ='profile__nav']/*[contains(@class,'header__icon')]")

    GenericMenu menu = new GenericMenu()

	void txtFirstName(String firstname) {
		common.input(txtFirstNameDonorSignUp, firstname);
	}

	void  txtLastName(String lastname) {
		common.input(txtLastNameDonorSignUp, lastname);
	}

	void  txtEmail(String email) {
		common.input(txtEmailDonorSignUp, email);
	}

	void txtpassword(String password) {
		common.scrollToObject(txtPasswordSignUp)
		common.input(txtPasswordSignUp, password)
	}

	void clickSignupButton() {
		common.scrollToObject(btnSignUp)
//		common.delay(common.timeOut2s)
		common.waitForElementVisible(btnSignUp, common.timeOut30s)
		webobj.waitForElementToBeClickable(btnSignUp, Integer.parseInt(common.timeOut30s))
		common.click(btnSignUp)
	}

	void verifyErrorMessageWhenEmailExists () {
		common.verifyTestObjectIsDisplayed(errorMessageEmailexists)
	}

	void signUpDonorSuccessfully(String first, String last, String Email, String password) {
		common.click(tabDonorSignUp)
		submitSignUpInfo(first, last, Email, password)
        menu.verifyAvatarIsDisplayed()
    }

	void signUpCharitySuccessfully(String first, String last, String Email, String password) {
		common.click(tabCharitySignUp)
		submitSignUpInfo(first, last, Email, password)
	}

	void submitSignUpInfo(String first, String last, String Email, String password) {
		txtFirstName(first)
		txtLastName(last)
		txtEmail(Email)
		txtpassword(password)
		clickSignupButton()
		common.waitForPageToLoad(common.timeOut30s)
	}

	void signupUnSuccessfully(String first, String last, String Email, String password) {
		common.click(tabDonorSignUp)
		submitSignUpInfo(first, last, Email, password)
		common.waitForPageToLoad(common.timeOut30s)
		verifyErrorMessageWhenEmailExists(common.getText(errorMessageEmailexists))
	}
}
