package com.gui.pages

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum
//import com.core.utilities.models.shoutButton.donation.ReceiptRecipient
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import org.apache.commons.lang3.StringUtils

class TransactionDetailPage {
    // Receipt details sections
    TestObject lblfundraiserName = common.createTestObject(Enum.LocatorType.XPATH.getType(),
            "//a[@href[contains(.,'/shout-funds/')]]")

    TestObject lblRecipientEmail = common.createTestObject(Enum.LocatorType.XPATH.getType(), "(//div[preceding-sibling::strong/span[text()='Email']])[2]")
    TestObject lblRecipientPhoneNo = common.createTestObject(Enum.LocatorType.XPATH.getType(), "(//div[preceding-sibling::strong/span[contains(text(),'Phone no')]])[2]")
    TestObject lblProduct = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//div[preceding-sibling::strong/span[text()='Product']]//span[2]")
    TestObject lblEnding = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//*[contains(text(),'Ending')]")
    TestObject lblTotalCharged = common.createTestObject(Enum.LocatorType.XPATH.getType(), "//td[preceding-sibling::td[descendant::span[text()='Total']]]/p[contains(@class,'total')]")
    TestObject lblPlanID = common.createTestObject(Enum.LocatorType.XPATH.getType(),
            "//div[preceding-sibling::strong/span[text()='Recurring Plan ID']]")

    TestObject honoreeName = common.createTestObject(Enum.LocatorType.XPATH.getType(),
            "//span[preceding-sibling::span[text()='Honoree']]")

    TestObject notifierName = common.createTestObject(Enum.LocatorType.XPATH.getType(),
            "//strong[span[text()='Dedication']]/following-sibling::div/div/div/span")
	
	//RECEIPT DETAIL sections
	

    void verifyCharityDetailsectionsHasFundraiser(String expectedFundraiserName){
        common.waitForElementVisible(lblfundraiserName, common.timeOut30s)
        common.scrollToObject(lblfundraiserName)
        common.verifyTextIsEqual(lblfundraiserName,expectedFundraiserName)
    }

    String getPlanId(){
        return common.getText(lblPlanID)
    }


    TestObject createTestObject(String xpath){
        return common.createTestObject(Enum.LocatorType.XPATH.getType(), xpath)
    }

    /*
	 objectLable: Charity, Donor Name, Email (Donor)
	 */
    TestObject createHrefObject(String objectLabel){
        return common.createTestObject(Enum.LocatorType.XPATH.getType(), "//div[preceding-sibling::strong/span[text()='$objectLabel']]/a")
    }


    /*
     objectLable: Transaction ID, Date Submitted, Name, Dedicated To
     */
    TestObject createDivObject(String objectLabel){
        return common.createTestObject(Enum.LocatorType.XPATH.getType(), "//div[preceding-sibling::strong/span[text()='$objectLabel']]")
    }

    /*
     objectLable: Frequency, Total Amount, Anonymously, Status, DGR Status
     */
    TestObject createSpanObject(String objectLabel){
        return common.createTestObject(Enum.LocatorType.XPATH.getType(), "//div[preceding-sibling::strong/span[text()='$objectLabel']]/span")
    }


    void verifyTxnDetails(String txnId, String frequency, String charityName, String dateSubmitted, Float transAmount, String email, String phoneNo, String donorName, String product, String ending, String dgrStatus, boolean isDedicated = false, FailureHandling failureHandling = FailureHandling.CONTINUE_ON_FAILURE){
        KeywordUtil.logInfo("Verifying info in Recurring Detail...")
        common.verifyEqualIgnoreCase(createSpanObject("Status"), "Success", failureHandling)
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(createDivObject("Transaction ID")), txnId, failureHandling)
        if (StringUtils.isNotEmpty(ending)) {
            WebUiBuiltInKeywords.verifyEqual(getEnding(), ending, failureHandling)
            KeywordUtil.logInfo("Payment method is credit card")
        }
        def dateCreatedTxt = getTextFromObject(createDivObject("Date Submitted"))
        WebUiBuiltInKeywords.verifyEqual(StringUtils.substringBefore(dateCreatedTxt, " "), dateSubmitted, failureHandling)
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(lblProduct), product, failureHandling)
        if (StringUtils.isNotEmpty(donorName)){
            WebUiBuiltInKeywords.verifyEqual(getTextFromObject(createHrefObject("Donor Name")), donorName, failureHandling)
            WebUiBuiltInKeywords.verifyEqual(getTextFromObject(createDivObject("Name")), donorName, failureHandling)
        }
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(createHrefObject("Email")), email, failureHandling)
        WebUiBuiltInKeywords.verifyEqual(getTotalAmount(), transAmount) //substring AUD
        if (StringUtils.isNotEmpty(frequency)) {
            common.verifyEqualIgnoreCase(createSpanObject("Frequency"), frequency, failureHandling)
        }
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(createHrefObject("Charity")), charityName, failureHandling)
        if (StringUtils.isNotEmpty(dgrStatus)) {
            WebUiBuiltInKeywords.verifyEqual(getTextFromObject(createSpanObject("DGR Status")), dgrStatus, failureHandling)
        }
//        if (StringUtils.isNotEmpty(phoneNo)) {
//            common.scrollToObject(lblRecipientPhoneNo)
//            WebUiBuiltInKeywords.verifyEqual(getPhoneNo(lblRecipientPhoneNo), phoneNo, failureHandling)
//        }
        WebUiBuiltInKeywords.verifyEqual(getTextFromObject(lblRecipientEmail), email, failureHandling)
        WebUiBuiltInKeywords.verifyEqual(StringUtils.substringAfter(getTextFromObject(lblTotalCharged), "\$"), transAmount, failureHandling)
        if (isDedicated){
            common.verifyTextIsEqual(createDivObject("Dedicated To"), donorName)
        }
    }

    private String getPhoneNo(TestObject to) {
        StringUtils.removeAll(StringUtils.substringAfter(getTextFromObject(to), " "), "\\s+")
    }

    String getTotalAmount() {
        def fullAmount = getTextFromObject(createSpanObject("Total Amount"))
        return fullAmount.substring(fullAmount.indexOf("\$") + 1)
    }

    String getEnding() {
        def source = getTextFromObject(lblEnding)
        StringUtils.substringAfter(source, ": ")
    }

    String getTextFromObject(TestObject to){
        return common.getText(to)
    }

}

