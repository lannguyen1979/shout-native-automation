package com.gui.pages

import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum.LocatorType
import com.core.utilities.common.WebObjects as webUI
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import static internal.GlobalVariable.*

class TransactionHistory extends BasePage{

	TestObject dropKeywordValue = common.createTestObject(LocatorType.XPATH.getType(), "//select[@id='keywordType']/option[text()='%s']")

	TestObject txtSearchArea = common.createTestObject(LocatorType.XPATH.getType(), "//input[@id='keyword']")

	TestObject btnSearch = common.createTestObject(LocatorType.XPATH.getType(),
	"//div[contains(concat(' ', normalize-space(@class), ' '), ' searchbox-container ')]//button[contains(concat(' ', normalize-space(@class), ' '), ' btn btn-base btn--pink btn--sm ')]")

	TestObject lblNoResultFound = common.createTestObject(LocatorType.XPATH.getType(), "//span[contains(.,'There is no transactions available yet')]")

	TestObject cellCharityName = common.createTestObject(LocatorType.XPATH.getType(), "//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]/td[contains(@class,'charity-name')]")

	TestObject cellDonationAmount = common.createTestObject(LocatorType.XPATH.getType(), "//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]/td/div[contains(@class,'total-column')]/span")

	TestObject cellTransactionStatus = common.createTestObject(LocatorType.XPATH.getType(),"//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]/td[contains(@class,'status')]/span/span");

	TestObject cellDateSubmitted = common.createTestObject(LocatorType.XPATH.getType(), "//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]/td[contains(@class,'dateSubmitted')]//span[text()]")

	TestObject transactionRow = common.createTestObject(LocatorType.XPATH.getType(), "//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]")

	TestObject tnsRow = common.createTestObject(LocatorType.XPATH.getType(),
	"//table[contains(concat(' ', normalize-space(@class), ' '), ' transactions-table ')]//tbody/tr[1]")

	TestObject loadingIndicator = common.createTestObject(LocatorType.XPATH.getType(), "//div[@class='loading--partial is-show']")

	void selectKeywordType(String keywordType) {
		webUI.waitForElementNotVisible(lblNoResultFound, 0)
		webUI.waitForElementNotVisible(loadingIndicator, 0)
		def newXpath = String.format(dropKeywordValue.findPropertyValue("xpath"), keywordType)
		common.modifyTestObject(dropKeywordValue,LocatorType.XPATH.getType(), newXpath)
		common.click(dropKeywordValue)
	}

	void verifyErrorMessageWhenNoSearchResultFound(String strData) {
		common.verifyTextIsEqual(lblNoResultFound, strData)
	}

	private searchTransactionsByTransId(String keywordType, String keywordValue) {
		selectKeywordType(keywordType)
		common.input(txtSearchArea, keywordValue)
		common.click(btnSearch)
        common.waitForElementNotVisible(iconLoading, common.timeOut30s)
	}

	/*Search transactions, modify objects and then verify transaction info*/
	TestObject getSearchResultByTransId(String transId, String charityName, String dateSubmitted, String transAmount){
		searchTransactionsByTransId("Transaction ID", transId)
		verifyTransactionInfo(charityName, dateSubmitted, transAmount)
		common.waitForElementNotVisible(transactionRow, common.timeOut10s)
		return transactionRow
	}

	void navigatetoTransactionDetail(TestObject txnRow){
		common.click(txnRow)
		common.waitForPageToLoad(common.timeOut30s)
		KeywordUtil.logInfo("Navigating to Transaction Detail page: ${WebUiBuiltInKeywords.getUrl()}")
	}

	private verifyTransactionInfo(String charityName, String dateSubmitted, String transAmount){
		KeywordUtil.logInfo("Expected Amount before submit:" + transAmount)
		common.verifyAmountIsEqual(cellDonationAmount, transAmount)
		common.verifyTextIsEqual(cellCharityName, charityName)
		common.verifyTextIsEqual(cellDateSubmitted, dateSubmitted)
	}

	String getTransactionStatus(){
		return common.getText(cellTransactionStatus)
	}
}