package com.gui.pages

import com.core.utilities.common.CommonActions as imp
import com.core.utilities.common.Enum.LocatorType
import com.kms.katalon.core.testobject.TestObject

import internal.GlobalVariable


class GenericMenu {

	TestObject genericAvatarImage = imp.createTestObject(LocatorType.XPATH.getType(),
	"//div[@class='profile__nav']//div[contains(@class,'avatar')]")

	TestObject genericLogOutLink = imp.createTestObject(LocatorType.XPATH.getType(),
	"//*[contains(@class,'profile is-open')]//span[text()='Log Out']")
	TestObject logOutLinkCH = imp.createTestObject(LocatorType.XPATH.getType(),
	"//*[contains(@class,'profile__menu')]//span[text()='Log Out']")
	TestObject aGiving = imp.createTestObject(LocatorType.XPATH.getType(),
	"//span[text()='Giving']")
	TestObject aShoutForGood = imp.createTestObject(LocatorType.XPATH.getType(),
	"//*[contains(concat(' ', normalize-space(@class), ' '), ' select-role__name ')]//span[text()='Shout For Good']")
	TestObject aSuperAdmin = imp.createTestObject(LocatorType.XPATH.getType(),
	"//*[contains(@class,'profile is-open')]//span[text()='Super Admin']")
	TestObject menuIcon = imp.createTestObject(LocatorType.XPATH.getType(),
	"//div[contains(@class,'profile__nav-name')]")
	//Menu profile if login by Shouter
	TestObject menuProfile = imp.createTestObject(LocatorType.XPATH.getType(), "//div[contains(@class,'profile__menu-item')]//span[contains(., 'My Profile')]")
	
	TestObject headerMenu = imp.createTestObject(LocatorType.XPATH.getType(),
	"//ul[contains(@class,'menu-nav')]//a[span='%s']")
	TestObject btnLogin = imp.createTestObject(LocatorType.XPATH.getType(),
	"//button[span='Log In']")
	TestObject txtLoginEmail = imp.createTestObject(LocatorType.XPATH.getType(),
	"//form[contains(@class,'account__form')]//input[@id='userEmail']")
	TestObject txtLoginPassword = imp.createTestObject(LocatorType.XPATH.getType(),
	"//form[contains(@class,'account__form')]//input[@id='userPassword']")
	TestObject btnSubmitLogin = imp.createTestObject(LocatorType.XPATH.getType(),
	"//form[contains(@class,'account__form')]//button[@id='btn-logIn']")
	String menuItemXpath = "//div[contains(@class,'profile__menu')]//span[contains(., '%s')]"
	HomePage homePage = new HomePage()

	void verifyAvatarIsDisplayed() {
		imp.verifyTestObjectIsDisplayed(genericAvatarImage)
	}

	void logOut() {
		homePage.navigateToMenuIcon()
        imp.waitForElementClickable(genericLogOutLink, imp.timeOut30s)
		imp.click(genericLogOutLink)
        imp.waitForPageToLoad(imp.timeOut30s)
		imp.verifyTestObjectIsDisplayed(btnLogin)
	}

	void navigateToSuperAdmin() {
		navigateToMenuItem("Super Admin")
	}

	void navigateToMyProfile() {
		navigateToMenuItem("My Profile")
	}

	void navigateToMenuItem(String menuName) {
		homePage.navigateToMenuIcon()
		TestObject menuItemObj = createMenuItem(menuName)
		imp.waitForElementVisible(menuItemObj, imp.timeOut30s)
		imp.click(menuItemObj)
		imp.waitForPageToLoad(imp.timeOut30s)
	}

	void navigateviaHeaderMenu(String pagename){
		TestObject headermenu = imp.createTestObject(LocatorType.XPATH.getType(),String.format(headerMenu.findPropertyValue("xpath"), pagename))
		imp.click(headermenu)
	}

	TestObject createMenuItem(String menuName){
		return imp.createTestObject("xpath", String.format(menuItemXpath, menuName))
	}

}
