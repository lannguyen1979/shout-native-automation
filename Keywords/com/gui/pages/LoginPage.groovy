package com.gui.pages


import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.core.utilities.common.CommonActions as imp
import com.core.utilities.common.Enum.LocatorType
import com.kms.katalon.core.testobject.TestObject

import internal.GlobalVariable

class LoginPage extends BasePage{

	static TestObject to_EmailAddress = imp.createTestObject(LocatorType.XPATH.getType(), "//input[@id='userEmail']")
	static TestObject to_Password = imp.createTestObject(LocatorType.XPATH.getType(), "//input[@id='userPassword']")
	static TestObject to_SubmitPage = imp.createTestObject(LocatorType.XPATH.getType(), "//button[@type='submit']")
	static TestObject toScripts = imp.createTestObject(LocatorType.XPATH.getType(), "//script[contains(@src,'recaptcha')]")
	static TestObject toLinks = imp.createTestObject(LocatorType.XPATH.getType(), "//link")
	static TestObject to_ErrorMessageNotification = imp.createTestObject(LocatorType.XPATH.getType(),
			"//div[contains(concat(' ', normalize-space(@class), ' '), ' notification__content ')]/span")
	static TestObject divAvatar = imp.createTestObject(LocatorType.XPATH.getType(),
			"//*[contains(@class,'avatar-small')]")
	static TestObject divReCaptchError = imp.createTestObject(LocatorType.XPATH.getType(),
			"//div[contains(text(),'Could not connect to the reCAPTCHA service')]")

	static GenericMenu menu = new GenericMenu()
	HomePage homePage = new HomePage()
	def reCaptChaErrorMsg = "ReCaptcha service may not work..."

	static inputEmailAddress(String email) {
		waitForLoginFormReady()
		imp.input(to_EmailAddress, email)
	}

	private static void waitForLoginFormReady() {
//		if (WebUiBuiltInKeywords.waitForAlert(GlobalVariable.G_minTimeOut)) {
//			WebUiBuiltInKeywords.dismissAlert()
//		}//todo: move this out when navigate to Home page for re-login
		WebUiBuiltInKeywords.waitForElementPresent(toScripts, GlobalVariable.G_maxTimeOut)
		WebUiBuiltInKeywords.waitForElementVisible(to_EmailAddress, GlobalVariable.G_maxTimeOut)
	}

	static inputPassword(String password) {
		imp.input(to_Password, password)
	}

	static clickSubmitPageButton() {
		imp.click(to_SubmitPage)
		imp.waitForPageToLoad(imp.timeOut30s)
	}

	static verifyErrorMessageWhenEmailAndPasswordCombinationIsNotValid (String strData) {
		imp.verifyTextIsEqual(to_ErrorMessageNotification, strData)
	}

	static void inputLoginInfo(String email, String password) {
		inputEmailAddress(email)
		inputPassword(password)
		clickSubmitPageButton()
	}

	static void inputLoginInfo() {
		inputEmailAddress(GlobalVariable.G_shouterUsername)
		inputPassword(GlobalVariable.G_loginPassword)
		clickSubmitPageButton()
	}

	static loginSuccessfully() {
		inputLoginInfo()
		menu.verifyAvatarIsDisplayed()
		imp.waitForPageToLoad(imp.timeOut30s)
	}

	static loginSuccessfully(String email, String password) {
		inputLoginInfo(email, password)
		imp.waitForPageToLoad(imp.timeOut30s)
	}

	void shouterLogin(String email, String password){
		loginFromHomePage(email, password)
		imp.verifyTestObjectIsDisplayed(menu.menuIcon)
	}

	void loginFromHomePage(String email, String password) {
		if (WebUiBuiltInKeywords.waitForAlert(GlobalVariable.G_minTimeOut)) {
			WebUiBuiltInKeywords.dismissAlert()
		}
		homePage.navigateToLoginPage()
		loginSuccessfully(email, password)
	}

	void twoFALogin(String email, String password)  {
		loginFromHomePage(email, password)
		SendOptForm optForm = new SendOptForm()
		/*re-login if having captcha service error*/
		retryLoginIfReCaptchaError(email, password)
		optForm.submitOptCodeOneStep()
		/*re-login and submitOptCode if having captcha service error*/
		retryLoginIfVerifyCodeError(email, password)
	}

	void twoFAStepsLogin(String email, String password)  {
		loginFromHomePage(email, password)
		SendOptForm optForm = new SendOptForm()
		/*re-login if having captcha service error*/
		retryLoginIfReCaptchaError(email, password)
		optForm.submitOptCode()
		/*re-login and submitOptCode if having captcha service error*/
//		retryLoginIfVerifyCodeError(email, password)
	}

	private void retryLoginIfVerifyCodeError(Boolean successCondition = imp.verifyTestObjectIsDisplayed(menu.menuIcon), String email, String password) {
		int retry = 3
		while (!successCondition && retry > 0) {
			loginFromHomePage(email, password)
			new SendOptForm().submitOptCodeOneStep()
			KeywordUtil.markPassed(reCaptChaErrorMsg)
			successCondition = imp.verifyTestObjectIsDisplayed(menu.menuIcon)
			retry--
		}
	}

	private void retryLoginIfReCaptchaError(String email, String password) {
		int retry = 3
		def optForm = new SendOptForm()
		boolean is2FAFormPresent = optForm.is2FAFormPresent()
		while (!is2FAFormPresent && retry > 0) {
			loginFromHomePage(email, password)
			KeywordUtil.markPassed(reCaptChaErrorMsg)
			is2FAFormPresent = optForm.is2FAFormPresent()
			retry--
		}
	}

	void charityLogin(String g_charityRandomEmail, String password) {
		loginFromHomePage(g_charityRandomEmail, password)
//		retryLoginIfReCaptchaError(g_charityRandomEmail, password)
		SendOptForm sendOptForm = new SendOptForm()
		sendOptForm.submitOptCodeOneStep()
//		retryLoginIfVerifyCodeError(WebUiBuiltInKeywords.getUrl().contains("/charity/"), g_charityRandomEmail, password)
		imp.delay('1')
	}
}
