package com.gui.pages

import static com.core.utilities.common.Constants.PAYMENT_TYPE_AMEX
import static com.core.utilities.common.Constants.PAYMENT_TYPE_MASTER
import static com.core.utilities.common.Constants.PAYMENT_TYPE_VISA

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.core.utilities.common.WebObjects as webobj

public class MyProfilePages extends BasePage{

	//Payment method tab
	TestObject inputCreditCardNo = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//input[@name='ccNumber']")

	String tabNameXpath = "//div[@class='tab-bars']//div[span='%s']"
	// tab name : My Details, Password, Payment Methods
	TestObject selectTabByName(String tabname){
		return common.createTestObject(Enum.LocatorType.XPATH.getType(), String.format(tabNameXpath, tabname))
	}

	//payment method sections

	TestObject createTestObjectRemoveCC(String endingDigit, String expiryDate){
		return common.createTestObject(Enum.LocatorType.XPATH.getType(),
				"//div[contains(text(),'$expiryDate')]/preceding-sibling::div[contains(.,'$endingDigit')]/ancestor::div[@role='menuitem']//span[text()='Remove']")
	}

	TestObject removePaymentPopup = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//span[contains(text(),'Remove payment method')]/ancestor::div[@class='modal is-active']")

	TestObject btnConfirmRemove = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//div[@class='modal is-active']//button[span='Yes']")

	//Change password sections
	TestObject txtCurrentPassword = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='oldPassword']")

	TestObject txtNewPassword= common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='newPassword']")

	TestObject txtConfirmPassword= common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//input[@id='confirmNewPassword']")

	TestObject btnSave = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//button[@id='btnPasswordSaveChanges']")

	TestObject lblChangePasswordSuccessfull = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//div[contains(concat(' ', normalize-space(@class), ' '), 'notification--inline notification--success')]//span/div[text() = 'Your new password has been updated']")

	void removeSelectedCC(String paymentMethod){
		switch (paymentMethod) {
			case PAYMENT_TYPE_VISA:
				String endingDigit = '1111'
				String expiryDate = '12/30'
				common.delay(common.getTimeOut2s())
				common.click(createTestObjectRemoveCC(endingDigit, expiryDate))
				break
			case PAYMENT_TYPE_AMEX:
				String endingDigit = '0005'
				String expiryDate = '12/30'
				common.delay(common.getTimeOut2s())
				common.click(createTestObjectRemoveCC(endingDigit, expiryDate))
				break
			case PAYMENT_TYPE_MASTER:
				String endingDigit = '5454'
				String expiryDate = '12/30'
				common.delay(common.getTimeOut2s())
				common.click(createTestObjectRemoveCC(endingDigit, expiryDate))
				break
		}
		common.waitForElementPresent(btnConfirmRemove, common.getTimeOut30s())
		common.click(btnConfirmRemove)
	}

	void setCurrentPasword(String currentPassword) {
		common.input(txtCurrentPassword, currentPassword);
	}

	void  setNewPassword(String newPassword) {
		common.input(txtNewPassword, newPassword);
	}

	void  setConfirmPassword(String confirmPassword) {
		common.input(txtConfirmPassword, confirmPassword);
	}

	void clickSaveButton() {
		common.scrollToObject(btnSave)
		common.waitForElementVisible(btnSave, common.timeOut30s)
		webobj.waitForElementToBeClickable(btnSave, Integer.parseInt(common.timeOut30s))
		common.click(btnSave)
	}

	void verifySuccessMessageWhenChangePasswordComplete () {
		webobj.waitForElementNotVisible(lblChangePasswordSuccessfull,Integer.parseInt(common.timeOut2s))
	}

	void submitChangePassword(String currentPassword, String newPassword, String ConfirmPassword) {
		setCurrentPasword(currentPassword)
		setNewPassword(newPassword)
		setConfirmPassword(ConfirmPassword)
		clickSaveButton()
		common.waitForPageToLoad(common.timeOut30s)
	}


	void verifyChangePasswordSuccessful(String currentPassword, String newPassword, String ConfirmPassword) {
		submitChangePassword(currentPassword,newPassword,ConfirmPassword )
		verifySuccessMessageWhenChangePasswordComplete()
	}

	void selecTab(String tabname){
		common.click(selectTabByName(tabname))
	}
}
