package com.gui.pages

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Constants
import com.core.utilities.common.Enum
import com.kms.katalon.core.testobject.TestObject
import org.apache.commons.lang3.StringUtils

public class CreateYourAccountPage {
	TestObject txtEmail = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//input[@id='signUpDonateForm_email']")
	TestObject txtFirstName = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//input[@id='signUpDonateForm_firstName']")
	TestObject txtLastName = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//input[@id='signUpDonateForm_lastName']")
	TestObject txtPassword = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//input[@id='signUpDonateForm_password']")
	TestObject btnContinue = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//div/button[@id='btn-signUp']/span")
	TestObject lblAmount = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//div[@class='item-info__price']/span")
	TestObject lblFrequency = common.createTestObject(Enum.LocatorType.XPATH.getType(),"//span[text()='Frequency']/following-sibling::span")

	void inputSignup(String str_firstName, String str_lastName, String str_Email, String str_Password){
		common.input(txtEmail,str_Email);
		common.input(txtFirstName, str_firstName);
		common.input(txtLastName,str_lastName);
		common.input(txtPassword,str_Password);
		common.click(btnContinue);
		
	}
	void verifyDonateInformation (String str_DonationAmount, String str_Frequency){
		String donationAmount = common.getText(lblAmount)
		common.verifyFloatNumberIsEqual(common.convertStringToFloat(donationAmount.substring(1, donationAmount.length())), common.convertStringToFloat(str_DonationAmount))
		if (StringUtils.equalsAnyIgnoreCase(str_Frequency, Constants.FREQUENCE_TYPE_MONTHLY)){
            common.verifyTextIsEqual(lblFrequency, "monthly donation")
        }
	}
}
