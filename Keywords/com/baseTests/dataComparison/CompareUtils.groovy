package com.baseTests.dataComparison

import com.core.utilities.common.DateTimeUtils
import com.core.utilities.common.FileUtils
import com.core.utilities.models.Donors
import com.kms.katalon.core.util.KeywordUtil
import org.apache.commons.lang3.StringUtils

class CompareUtils {
	static <T> boolean dataContains(List<T> containerList, List<T> subList, OrderBy orderBy = new OrderBy([{it ->it.id}])) {
		CompareUtils.sortList(subList, orderBy)
		CompareUtils.sortList(containerList, orderBy)

		Map<String, T> map = new HashMap<>()
		boolean isDataCorrect = true
		for (T actualModel: containerList){
			map.put(actualModel.id, actualModel)
		}

		List<String> missingList = new ArrayList<>()
		List<String> mismatchList = new ArrayList<>()
		for (int i = 0; i< subList.size(); i++) {
			T other = map.get(subList.get(i).id)
			if (other == null) {
				missingList.add("${subList.get(i).id}")
				isDataCorrect = false
				continue
			}
			if (subList.get(i).toString() == other.toString()) {
				continue
			}
			mismatchList.add("\nrow = $i \ncontainer:${other.toString()} \nsub      :${subList.get(i).toString()}")
			isDataCorrect = false
		}
		String timestamp = DateTimeUtils.getCurrentDate("yyyy_mm_dd_hhmmss")
		KeywordUtil.logInfo("missingList.size()= " + missingList.size())
		KeywordUtil.logInfo("mismatchList.size()= " + mismatchList.size())
		FileUtils.writeListToFile(missingList, "missingList_${timestamp}.txt")
		FileUtils.writeListToFile(mismatchList, "mismatchList_${timestamp}.txt")
		return isDataCorrect
	}

	static <T> boolean dataEquals(List<T> containerList, List<T> subList, OrderBy orderBy = new OrderBy([{it ->it.id}])) {
		CompareUtils.sortList(subList, orderBy)
		CompareUtils.sortList(containerList, orderBy)
		List<String> lstStringObjectDb = new ArrayList<>()
		boolean isDataCorrect = true
		for (T actualModel: containerList){
			lstStringObjectDb.add(actualModel.toString())
		}
		for (int i = 0; i< subList.size(); i++) {
			if (!lstStringObjectDb.contains(subList.get(i).toString())) {
				KeywordUtil.logInfo "row = $i"
				KeywordUtil.logInfo("db:" + containerList.get(i).toString())
				KeywordUtil.logInfo("json:  " + subList.get(i).toString())
				isDataCorrect = false
				break
			}
		}
		return isDataCorrect
	}

	static boolean userDataContains(List<Donors> containerList, List<Donors> subList) {
		List<String> lstStringObjectDb = new ArrayList<>()
		boolean isDataCorrect = true
		for (Donors actualModel: containerList){
			lstStringObjectDb.add(actualModel.toString())
		}

		for (int i = 0; i< subList.size(); i++) {
			if (Objects.isNull(subList.get(i).getEmail())){
				if (!lstStringObjectDb.contains(subList.get(i).getFirstName()) |
				!lstStringObjectDb.contains(subList.get(i).getLastName())) {
					continue
				}
			}else {
				if (!lstStringObjectDb.contains(subList.get(i).toString())) {
					KeywordUtil.logInfo("row = $i")
					KeywordUtil.logInfo("Mismatch item in subList (DbS2): " + subList.get(i).toString() + '\n')
					KeywordUtil.logInfo("Corresponding item in From containerList (json): " + containerList.get(i).toString() + '\n')
					isDataCorrect = false
					break
				}
			}
		}
		return isDataCorrect
	}

	static <T> boolean modelEquals(T actualModel, T expectedModel) {
		boolean isDataCorrect = true
		if (!StringUtils.equals(actualModel.toString(), expectedModel.toString())){
			return false
		}
		return isDataCorrect
	}

	static <T> List<T> sortList(List<T> list, OrderBy orderBy) {
		list.sort{orderBy}
		return list
	}
}
