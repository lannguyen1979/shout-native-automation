package com.core.utilities.browser


import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

class GenericBrowser {

	static WebUiBuiltInKeywords webUI = new WebUiBuiltInKeywords()
	static FailureHandling stop_on_failure = FailureHandling.STOP_ON_FAILURE

	static openBrowser(FailureHandling failureHandling = stop_on_failure) {
		webUI.openBrowser("", failureHandling)
	}

	static maximizeWindow(FailureHandling failureHandling = stop_on_failure) {
		webUI.maximizeWindow(failureHandling)
	}

	static closeBrowser(FailureHandling failureHandling = stop_on_failure) {
		webUI.closeBrowser(failureHandling)
	}
}
