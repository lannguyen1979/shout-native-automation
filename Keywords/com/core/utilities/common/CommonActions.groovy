package com.core.utilities.common

import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

class CommonActions {

	static FailureHandling stop_on_failure = FailureHandling.STOP_ON_FAILURE
	static WebUiBuiltInKeywords webUI = new WebUiBuiltInKeywords()
	static String timeOut30s = GlobalVariable.G_maxTimeOut
	static String timeOut10s = GlobalVariable.G_minTimeOut
	static String timeOut1s = GlobalVariable.G_timeOut1s
	static String timeOut2s = GlobalVariable.G_timeOut2s
	static String timeOut3s = GlobalVariable.G_timeOut3s
	static String timeOut5s = GlobalVariable.G_timeOut5s


	static input(TestObject to, String inputValue, FailureHandling failureHandling = stop_on_failure) {
		waitForElementPresent(to, timeOut30s)
		webUI.clearText(to, failureHandling)
		webUI.sendKeys(to, inputValue, failureHandling)
	}

	static click(TestObject to, FailureHandling failureHandling = stop_on_failure) {
		waitForElementPresent(to, timeOut30s)
		webUI.click(to,failureHandling)
	}

	static navigateToUrl(String url, FailureHandling failureHandling = stop_on_failure) {
		webUI.navigateToUrl(url, failureHandling)
		waitForPageToLoad(timeOut30s)
	}

	static waitForPageToLoad(String timeOut, FailureHandling failureHandling = stop_on_failure) {
		int intTimeOut = Integer.parseInt(timeOut)
		webUI.waitForPageLoad(intTimeOut, failureHandling)
	}

	static verifyTestObjectIsDisplayed(TestObject to, FailureHandling failureHandling = stop_on_failure){
		webUI.waitForElementVisible(to, Integer.parseInt(timeOut30s))
		webUI.verifyElementVisible(to, failureHandling)
	}

	static verifyTestObjectPresent(TestObject to, int timeOut = GlobalVariable.G_maxTimeOut, FailureHandling failureHandling = stop_on_failure){
		webUI.verifyElementPresent(to, timeOut, failureHandling)
	}

	static verifyTextIsEqual(TestObject to, String expectedText, FailureHandling failureHandling = stop_on_failure){
		waitForElementVisible(to, timeOut30s)
		String str_ActualText = webUI.getText(to, failureHandling).toLowerCase()
		KeywordUtil.logInfo("Actual text: " + str_ActualText + " vs. expected text: " + expectedText)
		webUI.verifyEqual(str_ActualText, expectedText.toLowerCase(), failureHandling)
	}

	static verifyEqualIgnoreCase(TestObject to, String expectedText, FailureHandling failureHandling = stop_on_failure){
		waitForElementVisible(to, timeOut30s)
		String actualText = webUI.getText(to, failureHandling).toLowerCase()
		KeywordUtil.logInfo("actual text: " + actualText + "vs. expected text:" + expectedText)
		webUI.verifyEqual(actualText.toLowerCase(), expectedText.toLowerCase(), failureHandling)
	}

	static verifyEquals(TestObject to, Float expectedValue, FailureHandling failureHandling = stop_on_failure){
		def actualValue = webUI.getText(to, failureHandling)
		webUI.verifyEqual(actualValue.substring(1, actualValue.length()), expectedValue, failureHandling)
	}

	static verifyValueIsEqual(TestObject to, Float expectedValue, FailureHandling failureHandling = stop_on_failure){
		def actualValue = webUI.getText(to, failureHandling)
		webUI.verifyEqual(actualValue, expectedValue, failureHandling)
	}

	static verifyFloatNumberIsEqual(float actualValue, float expectedValue, FailureHandling failureHandling = stop_on_failure) {
		webUI.verifyEqual(actualValue, expectedValue, failureHandling)
	}

	static float convertStringToFloat(String value){
		return Float.parseFloat(value)
	}

	static verifyAmountIsEqual(TestObject to, String expectedText, FailureHandling failureHandling = stop_on_failure){
		String actualText = webUI.getText(to, failureHandling)
		waitForElementVisible(to, timeOut30s)
		webUI.verifyEqual(actualText.substring(1, actualText.length()), expectedText, failureHandling)
	}

	static switchToIframe(TestObject to, String timeOut, FailureHandling failureHandling = stop_on_failure){
		int intTimeOut = Integer.parseInt(timeOut)
		webUI.waitForElementPresent(to, intTimeOut)
		webUI.switchToFrame(to, intTimeOut, failureHandling)
	}

	static switchToWindowTitle(String windowTitle, FailureHandling failureHandling = stop_on_failure){
		webUI.switchToWindowTitle(windowTitle, failureHandling)
	}

	static delay(String timeOut, FailureHandling failureHandling = stop_on_failure){
		webUI.delay(Integer.parseInt(timeOut), failureHandling)
	}

	static waitForElementPresent(TestObject to, String timeOut, FailureHandling failureHandling = stop_on_failure) {
		webUI.waitForElementPresent(to, Integer.parseInt(timeOut), failureHandling)
	}

	static waitForElementVisible(TestObject to, String str_TimeOut, FailureHandling failureHandling = stop_on_failure) {
		webUI.waitForElementVisible(to, Integer.parseInt(str_TimeOut), failureHandling)
	}

	static waitForElementNotPresent(TestObject to, String timeOut, FailureHandling failureHandling = stop_on_failure){
		webUI.waitForElementNotPresent(to, Integer.parseInt(timeOut), failureHandling)
	}

	static waitForElementNotVisible(TestObject to, String timeOut, FailureHandling failureHandling = stop_on_failure){
		webUI.waitForElementNotVisible(to, Integer.parseInt(timeOut), failureHandling)
	}

	static waitForElementClickable(TestObject to, String timeOut, FailureHandling failureHandling = stop_on_failure) {
		webUI.waitForElementClickable(to, Integer.parseInt(timeOut), failureHandling)
	}

	static String getText(TestObject to, FailureHandling failureHandling = stop_on_failure){
		webUI.getText(to, failureHandling)
	}

	static int getNumberOfTestObject (TestObject to, String timeOut) {
		return webUI.findWebElements(to, Integer.parseInt(timeOut)).size()
	}

	static TestObject createTestObject(String locatorType, String locatorValue){
		TestObject to = new TestObject()
		to.addProperty(locatorType, ConditionType.EQUALS, locatorValue)
		return to
	}

	static TestObject createTestObjectWithId(String id) {
		return CommonActions.createTestObject("xpath", "//*[@id='$id']")
	}

	static TestObject modifyTestObject(TestObject to, String locatorType, String modifiedValue){
		return webUI.modifyObjectProperty(to, locatorType, "equals", modifiedValue, true)
	}

	static String findPropertyValue(TestObject to, String propertyName){
		return to.findPropertyValue(propertyName)
	}

	static scrollToObject(TestObject to){
		webUI.scrollToElement(to, Integer.parseInt(timeOut30s))
	}

	static String replaceAllByRegex(String srcText, String regex, String replacedText) {
		return srcText.replaceAll(regex, replacedText)
	}

	static uploadFile(TestObject to, String fileName){
		webUI.click(to)
		String filePath = System.getProperty("user.dir") + File.separator + "Data Files" + File.separator + fileName
		StringSelection ss = new StringSelection(filePath)
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null)
		Robot robot = new Robot()
		robot.keyPress(KeyEvent.VK_ENTER)
		robot.keyRelease(KeyEvent.VK_ENTER)
		robot.delay(1000)
		robot.keyPress(KeyEvent.VK_CONTROL)
		robot.keyPress(KeyEvent.VK_V)
		robot.keyRelease(KeyEvent.VK_V)
		robot.delay(1000)
		robot.keyRelease(KeyEvent.VK_CONTROL)
		robot.keyPress(KeyEvent.VK_ENTER)
		robot.keyRelease(KeyEvent.VK_ENTER)
	}

	void addGlobalVariable(CharSequence name, def value) {
		GroovyShell shell1 = new GroovyShell()
		MetaClass mc = shell1.evaluate("internal.GlobalVariable").metaClass
		String getterName = "get" + name.capitalize()
		mc.'static'."$getterName" = { -> return value }
		mc.'static'."$name" = value
	}

	private String getGlobalVariable(String name){
		String var = ""
		try {
			var = GlobalVariable."$name"
		}catch (MissingPropertyException e){
			KeywordUtil.logInfo("Caught " + e.getMessage())
		}
		return var
	}

	/*
	 * pre-condition: g_chairtyRandomEmail must be added by @addGlobalVariable('g_chairtyRandomEmail', <value>) first
	 * g_chairtyRandomEmail: must follow variable convention*/
	String getG_charityRandomEmail(){
		getGlobalVariable("g_charityRandomEmail")
	}

	String getG_charityRandomName(){
		getGlobalVariable("g_charityRandomName")
	}

	static switchtoWindowIndex(def index){
		webUI.switchToWindowIndex(index, stop_on_failure)
	}

	static focusOnObject(TestObject to){
		webUI.focus(to)
	}

	static void scrollToClick(String timOut= "0", TestObject to) {
		scrollToObject(to)
		delay(timOut)
		click(to)
	}

	static void scrollToInput(String timOut= "0", TestObject to, String value) {
		scrollToObject(to)
		delay(timOut)
		input(to, value)
	}

	/*tobeModifiedOptionTo: locator must have %s to be modified by text option*/
	static void selectDropDownList(TestObject dropDownTo, TestObject tobeModifiedOptionTo, String txtOption) {
		click(dropDownTo)
		TestObject newOptionTo = createTestObject(Enum.LocatorType.XPATH.getType(), String.format(tobeModifiedOptionTo.findPropertyValue("xpath"), txtOption))
		waitForElementClickable(newOptionTo, timeOut30s)
		click(newOptionTo)
	}
}
