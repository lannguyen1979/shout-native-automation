package com.core.utilities.common


import java.text.SimpleDateFormat
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor

class DateTimeUtils {
	static Object getCurrentDate(String dateFormatter) {
		ZonedDateTime ausTime = ZonedDateTime.now(ZoneId.of("Australia/Sydney"))
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormatter)
		return ausTime.format(formatter)
	}

	static Date getDate(String dateFormat, String specificDate) {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat)
		return format.parse(specificDate)
	}

	static String convertDateFormat(String parsingFormat, String expectedFormat, String specificDate) {
		SimpleDateFormat expect = new SimpleDateFormat(expectedFormat)
		return expect.format(getDate(parsingFormat, specificDate))
	}

	static String convertDateFormat(Date date, String expectedFormat){
		SimpleDateFormat format = new SimpleDateFormat(expectedFormat)
		return format.format(date)
	}

	static String convertUTCToAEST(String parsingFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", String expectedFormat, String specificDate) {
		DateTimeFormatter utcFormatter = DateTimeFormatter
				.ofPattern(parsingFormat)
				.withZone(ZoneId.of("UTC"))
		TemporalAccessor inputParsed = utcFormatter.parse(specificDate)
		DateTimeFormatter formatter = DateTimeFormatter
				.ofPattern(expectedFormat)
				.withZone(ZoneId.of("Australia/Sydney"))
		System.out.println(formatter.format(inputParsed))
		return formatter.format(inputParsed)
	}

	static Date getTimeMinusMinutes(int min) {
		return getDateMinusTime(min)
	}

	static Date getTimeMinusYears(int years) {
		return getDateMinusTime(years, "YEAR")
	}

	private static Date getDateMinusTime(int time, String TIME_TYPE = "MINUTE") {
		Calendar calendar = Calendar.getInstance()
		calendar.add(Calendar."$TIME_TYPE", -time)
		return calendar.getTime()
	}

	static String getUTCNow(String dateFormatter){
		ZonedDateTime ausTime = ZonedDateTime.now(ZoneOffset.UTC)
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormatter)
		return ausTime.format(formatter)
	}

	static Date getDateMinusSecond(Date date, int sec){
		Calendar day = Calendar.getInstance()
		day.setTime(date)
		day.add(Calendar.SECOND, -sec)
		return day.getTime()
	}

	static String getTxnSubmittedDate() {
		return DateTimeUtils.getDateMinusSecond(DateTimeUtils.getDate("yyyy-MM-dd'T'hh:mm:ss.000'Z'", DateTimeUtils.getUTCNow("yyyy-MM-dd'T'hh:mm:ss.000'Z'")), 1)
	}

	static Object getYear(int intervalYears) {
		Calendar calendar = Calendar.getInstance()
		calendar.setTime(getTimeMinusYears(intervalYears))
		def year = calendar.get(Calendar.YEAR)
		year
	}
}
