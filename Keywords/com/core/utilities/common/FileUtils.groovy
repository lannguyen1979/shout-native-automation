package com.core.utilities.common

import com.google.gson.Gson

class FileUtils {

    static <T> FileWriter writeJsListToFile(List<T> txnList, String fileName = "transactionList.txt") {
        def filePath = "Data Files" + File.separator + fileName
        FileWriter writer = new FileWriter(filePath)
        writer.write(new Gson().toJson(txnList))
        writer.close()
        return writer
    }

    static <T> FileWriter writeListToFile(List<T> txnList, String fileName = "transactionList.txt") {
        def filePath = "Data Files" + File.separator + fileName
        FileWriter writer = new FileWriter(filePath)
        writer.write(txnList.toString())
        writer.close()
        return writer
    }

}
