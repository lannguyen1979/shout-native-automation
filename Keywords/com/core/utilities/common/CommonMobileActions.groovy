package com.core.utilities.common

import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords

import internal.GlobalVariable

class CommonMobileActions {

	static FailureHandling stop_on_failure = FailureHandling.STOP_ON_FAILURE
	
	static MobileBuiltInKeywords mobile = new MobileBuiltInKeywords()
	
	static String timeOut30s = GlobalVariable.G_maxTimeOut
	static String timeOut10s = GlobalVariable.G_minTimeOut
	static String timeOut1s = GlobalVariable.G_timeOut1s
	static String timeOut2s = GlobalVariable.G_timeOut2s
	static String timeOut3s = GlobalVariable.G_timeOut3s
	static String timeOut5s = GlobalVariable.G_timeOut5s
	
	
	static startApp( String appFile, boolean unInstallApp = true) {
		mobile.startApplication(appFile, unInstallApp)
	}
	
	
	static closeApp(FailureHandling failureHandling = stop_on_failure ) {
		mobile.closeApplication(failureHandling)
	}
	static input(TestObject to, String inputValue, FailureHandling failureHandling = stop_on_failure) {
		waitForElementPresent(to, timeOut30s)
		mobile.clearText(to, timeOut30s, failureHandling)
		mobile.sendKeys(to, inputValue, failureHandling)
	}
	static tap(TestObject to, FailureHandling failureHandling = stop_on_failure) {
		waitForElementPresent(to, timeOut30s)
		mobile.tap(to,  Integer.parseInt(timeOut30s), failureHandling)
		
	}


	static verifyTestObjectIsDisplayed(TestObject to, FailureHandling failureHandling = stop_on_failure){
		mobile.waitForElementPresent(to, Integer.parseInt(timeOut30s))
		mobile.verifyElementVisible(to, Integer.parseInt(timeOut30s), failureHandling)
	}

	static verifyTestObjectPresent(TestObject to, int timeOut = GlobalVariable.G_maxTimeOut, FailureHandling failureHandling = stop_on_failure){
		mobile.verifyElementVisible(to, timeOut, failureHandling)
	}

	static verifyTextIsEqual(TestObject to, String expectedText, FailureHandling failureHandling = stop_on_failure){
		waitForElementVisible(to, timeOut30s)
		String str_ActualText = mobile.getText(to, Integer.parseInt(timeOut30s), failureHandling).toLowerCase()
		KeywordUtil.logInfo("Actual text: " + str_ActualText + " vs. expected text: " + expectedText)
		mobile.verifyEqual(str_ActualText, expectedText.toLowerCase(), failureHandling)
	}

	static verifyEqualIgnoreCase(TestObject to, String expectedText, FailureHandling failureHandling = stop_on_failure){
		waitForElementVisible(to, timeOut30s, failureHandling)
		String str_ActualText = mobile.getText(to, Integer.parseInt(timeOut30s), failureHandling).toLowerCase()
		KeywordUtil.logInfo("actual text: " + str_ActualText + "vs. expected text:" + expectedText)
		mobile.verifyEqual(str_ActualText.toLowerCase(), expectedText.toLowerCase(), failureHandling)
	}

	static verifyEquals(TestObject to, Float expectedValue, FailureHandling failureHandling = stop_on_failure){
		def actualValue = mobile.getText(to, Integer.parseInt(timeOut30s), failureHandling)
		mobile.verifyEqual(actualValue.substring(1, actualValue.length()), expectedValue, failureHandling)
	}

	static verifyValueIsEqual(TestObject to, Float expectedValue, FailureHandling failureHandling = stop_on_failure){
		def actualValue = mobile.getText(to, Integer.parseInt(timeOut30s), failureHandling)
		mobile.verifyEqual(actualValue, expectedValue, failureHandling)
	}

	static verifyFloatNumberIsEqual(float actualValue, float expectedValue, FailureHandling failureHandling = stop_on_failure) {
		mobile.verifyEqual(actualValue, expectedValue, failureHandling)
	}

	static float convertStringToFloat(String value){
		return Float.parseFloat(value)
	}

	static verifyAmountIsEqual(TestObject to, String expectedText, FailureHandling failureHandling = stop_on_failure){
		String actualText = mobile.getText(to, Integer.parseInt(timeOut30s), failureHandling).toLowerCase()
		waitForElementVisible(to, timeOut30s)
		mobile.verifyEqual(actualText.substring(1, actualText.length()), expectedText, failureHandling)
	}


	static delay(String timeOut, FailureHandling failureHandling = stop_on_failure){
		mobile.delay(Integer.parseInt(timeOut), failureHandling)
	}

	static waitForElementPresent(TestObject to, String timeOut, FailureHandling failureHandling = stop_on_failure) {
		mobile.waitForElementPresent(to, Integer.parseInt(timeOut), failureHandling)
	}

	static waitForElementVisible(TestObject to, String str_TimeOut, FailureHandling failureHandling = stop_on_failure) {
		mobile.verifyElementVisible(to, Integer.parseInt(str_TimeOut), failureHandling)
	}


	static String getText(TestObject to, FailureHandling failureHandling = stop_on_failure){
		mobile.getText(to,  Integer.parseInt(timeOut30s), failureHandling)
	}


	static TestObject createTestObject(String locatorType, String locatorValue){
		TestObject to = new TestObject()
		to.addProperty(locatorType, ConditionType.EQUALS, locatorValue)
		return to
	}

	static TestObject createTestObjectWithId(String id) {
		return CommonActions.createTestObject("xpath", "//*[@id='$id']")
	}


	static String findPropertyValue(TestObject to, String propertyName){
		return to.findPropertyValue(propertyName)
	}

	static scrollToObject(String value, FailureHandling failureHandling = stop_on_failure){
		
		mobile.scrollToText(value, failureHandling)
		
	}

	static String replaceAllByRegex(String srcText, String regex, String replacedText) {
		return srcText.replaceAll(regex, replacedText)
	}

	static uploadFile(TestObject to, String fileName){
		mobile.tap(to, Integer.parseInt(timeOut30s))
		String filePath = System.getProperty("user.dir") + File.separator + "Data Files" + File.separator + fileName
		StringSelection ss = new StringSelection(filePath)
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null)
		Robot robot = new Robot()
		robot.keyPress(KeyEvent.VK_ENTER)
		robot.keyRelease(KeyEvent.VK_ENTER)
		robot.delay(1000)
		robot.keyPress(KeyEvent.VK_CONTROL)
		robot.keyPress(KeyEvent.VK_V)
		robot.keyRelease(KeyEvent.VK_V)
		robot.delay(1000)
		robot.keyRelease(KeyEvent.VK_CONTROL)
		robot.keyPress(KeyEvent.VK_ENTER)
		robot.keyRelease(KeyEvent.VK_ENTER)
	}

	void addGlobalVariable(CharSequence name, def value) {
		GroovyShell shell1 = new GroovyShell()
		MetaClass mc = shell1.evaluate("internal.GlobalVariable").metaClass
		String getterName = "get" + name.capitalize()
		mc.'static'."$getterName" = { -> return value }
		mc.'static'."$name" = value
	}

	private String getGlobalVariable(String name){
		String var = ""
		try {
			var = GlobalVariable."$name"
		}catch (MissingPropertyException e){
			KeywordUtil.logInfo("Caught " + e.getMessage())
		}
		return var
	}

	/*
	 * pre-condition: g_chairtyRandomEmail must be added by @addGlobalVariable('g_chairtyRandomEmail', <value>) first
	 * g_chairtyRandomEmail: must follow variable convention*/
	String getG_charityRandomEmail(){
		getGlobalVariable("g_charityRandomEmail")
	}

	String getG_charityRandomName(){
		getGlobalVariable("g_charityRandomName")
	}


	static void scrollToClick(String timOut= "0", TestObject to) {
		scrollToObject(to)
		delay(timOut)
		tap(to)
	}

	static void scrollToInput(String timOut= "0", TestObject to, String value) {
		scrollToObject(to)
		delay(timOut)
		input(to, value)
	}

	/*tobeModifiedOptionTo: locator must have %s to be modified by text option*/
	static void selectDropDownList(TestObject dropDownTo, TestObject tobeModifiedOptionTo, String txtOption) {
		tap(dropDownTo)
		TestObject newOptionTo = createTestObject(Enum.LocatorType.XPATH.getType(), String.format(tobeModifiedOptionTo.findPropertyValue("xpath"), txtOption))
		waitForElementPresent(newOptionTo, timeOut30s)
		tap(newOptionTo)
	}
	
	static void closeApplication() {
		mobile.closeApplication()
		
	}
}
