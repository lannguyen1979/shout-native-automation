package com.core.utilities.common

import com.core.utilities.models.CSRFTokens
import com.core.utilities.models.ServiceResponse
import com.core.utilities.models.charity.CharityUser
import com.core.utilities.models.uploadFile.DataFile
import com.google.gson.Gson
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.core.utilities.webservice.BaseAPITesting

import groovy.json.JsonSlurper
import internal.GlobalVariable

import static com.core.utilities.common.Constants.UPLOAD_FILE_PATH

public class UploadFiles {
	
	BaseAPITesting api = new BaseAPITesting()
	Gson gson = new Gson()	
	
	CSRFTokens getCharityAuthorization(String charityUsername, String charityPassword) {
		'Login user'
		CharityUser charityUser = new CharityUser()
		charityUser.setUsername(charityUsername)
		charityUser.setPassword(charityPassword)
		ResponseObject login = api.sendPOSTRequestWithCSRF(GlobalVariable.G_baseAPIUrl + AUTHEN_TOKEN_PATH, new Gson().toJson(charityUser))
		WebUiBuiltInKeywords.verifyEqual(new JsonSlurper().parseText(login.getResponseBodyContent()).status, "success")
		'Get cookies and headers from login'
		return api.getSetCookies(login.getHeaderFields())
	}
	
	DataFile getUploadFileOnBoading(String charityUsername, String charityPassword, String fileNameOnDataFiles, String type, String step) {
		CSRFTokens csrfTokens = getCharityAuthorization(charityUsername, charityPassword)
		ResponseObject uploadFile = api.sendFormData(
				GlobalVariable.G_baseAPIUrl + UPLOAD_FILE_PATH,
				csrfTokens.getCsrfToken(),
				csrfTokens.getShoutWebSession(),
				fileNameOnDataFiles,
				type, step)
		WebUiBuiltInKeywords.verifyEqual(uploadFile.getStatusCode(), "200")
		ServiceResponse sr = gson.fromJson(uploadFile.getResponseBodyContent(), ServiceResponse.class)
		return gson.fromJson(sr.getData(), DataFile.class)
	}

}
