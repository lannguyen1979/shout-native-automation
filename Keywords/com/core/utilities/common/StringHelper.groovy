package com.core.utilities.common


import org.apache.commons.lang3.StringUtils

import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat

class StringHelper {

	static String randomString(String prefix){
		String output = new SimpleDateFormat("yyMMddHHmmss").format(new Date())
		return prefix + output
	}

	static String containsClass(String className) {
		return String.format("contains(concat(' ', normalize-space(@class), ' '), ' %s ')", className)
	}

	static String getEndingDigits(String cardNo) {
		String ending = StringUtils.substring(cardNo, cardNo.length() - 4, cardNo.length())
		ending
	}

	static String toFloat(String number, NumberFormat formatter = new DecimalFormat("0.00")) {
		String formmatedFloatValue = formatter.format(Float.parseFloat(number))
		formmatedFloatValue
	}

	static String randomNum(int min, int max){
		Random rand = new Random()
		return rand.nextInt((max-min)+1)+min
	}

}
