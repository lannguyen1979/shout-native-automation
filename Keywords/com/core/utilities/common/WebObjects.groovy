package com.core.utilities.common

import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.By
import org.openqa.selenium.TimeoutException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.interactions.Actions

import static internal.GlobalVariable.*

class WebObjects {

	private static WebDriver getDriver(){
		return DriverFactory.getWebDriver()
	}

	static waitForElementToBeClickable(TestObject to, int timeOut){
		WebDriver webDriver = getDriver()
		String xpath = to.findPropertyValue("xpath")
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut, 100)
		wait.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.xpath(xpath))))
	}

	static waitForElementToBeClickableByCss(TestObject to, int timeOut){
		WebDriver webDriver = getDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut, 100)
		wait.until(ExpectedConditions.elementToBeClickable(webDriver.findElement(By.cssSelector(to.findPropertyValue("css")))))
	}

	static waitForElementNotVisible(TestObject to, int timeOut){
		WebDriver webDriver = getDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut, 100)
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(to.findPropertyValue("css"))))
	}

	static waitForElementNotPresentByCss(TestObject to, int timeOut){
		WebDriver webDriver = getDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut, 100)
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(to.findPropertyValue("css"))))
	}

	static waitForElementNotPresent(TestObject to, int timeOut){
		WebDriver webDriver = getDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut, 100)
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(to.findPropertyValue("xpath"))))
	}

	static waitForElementsVisible(TestObject to, int timeOut){
		WebDriver webDriver = getDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut, 100)
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(to.findPropertyValue("xpath"))))
	}

	static waitForElementNotVisibleXpath(TestObject to, int timeOut){
		WebDriver webDriver = getDriver()
		String xpath = to.findPropertyValue("xpath")
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut, 100)
		wait.until(ExpectedConditions.invisibilityOfElementLocated(webDriver.findElement(By.xpath(xpath), FailureHandling.OPTIONAL)))
	}
	static waitForElementDismissAlert(TestObject to, int timeOut){
		WebDriver webDriver = getDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut, 100)
		wait.until(ExpectedConditions.alertIsPresent())
		driver.switchTo().alert().dismiss()
		def element = webDriver.findElement(By.xpath(to.findPropertyValue("xpath")))
		wait.until(ExpectedConditions.elementToBeClickable(element))

		new Actions(driver)
				.moveToElement(element)
				.pause(1000)
				.build()
				.perform()
	}

	static waitForURLReturned(String expectedUrl, int timeOut = G_maxTimeOut){
		WebDriver webDriver = getDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut, 100)
		try{
			wait.until(ExpectedConditions.urlToBe(expectedUrl))
		}catch (TimeoutException e){
			KeywordUtil.markFailed(e.getMessage())
		}

	}
	static waitForTitlleReturned(String title, int timeOut = G_maxTimeOut){
		WebDriver webDriver = getDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut, 100)
		try{
			wait.until(ExpectedConditions.titleIs(title))
		}catch (TimeoutException e){
			KeywordUtil.markFailed(e.getMessage())
		}

	}

	static waitForFrameToBeAvailableAndSwitchToIt(String frameXpath, int timeOut = G_maxTimeOut){
		WebDriver webDriver = getDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, G_maxTimeOut)
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(frameXpath)))
	}

	static waitForAttributeContains(String xPath, String attribute, String attributeValue, int timeOut = G_maxTimeOut){
		WebDriver webDriver = getDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, timeOut)
		wait.until(ExpectedConditions.attributeContains(By.xpath(xPath), attribute, attributeValue))
	}

	static int countNoOfObjects(String xpath){
		WebDriver webDriver = getDriver()
		return webDriver.findElements(By.xpath(xpath)).size()
	}
}
