package com.core.utilities.common

public class Enum {
	public enum LocatorType{
		XPATH('xpath'),
		ID('id'),
		CSS('css'),
		NAME('name')
		private String type

		LocatorType(String type){
			this.type = type
		}

		public String getType(){
			return type
		}
	}
}
