package com.core.utilities.models.charity

class Location {
    private String administrativeAreaLevel1
    private String postalCode
    private String country
    private String locality
    private String address

    String getAdministrativeAreaLevel1() {
        return administrativeAreaLevel1
    }

    void setAdministrativeAreaLevel1(String administrativeAreaLevel1) {
        this.administrativeAreaLevel1 = administrativeAreaLevel1
    }

    String getPostalCode() {
        return postalCode
    }

    void setPostalCode(String postalCode) {
        this.postalCode = postalCode
    }

    String getCountry() {
        return country
    }

    void setCountry(String country) {
        this.country = country
    }

    String getLocality() {
        return locality
    }

    void setLocality(String locality) {
        this.locality = locality
    }

    String getAddress() {
        return address
    }

    void setAddress(String address) {
        this.address = address
    }


}
