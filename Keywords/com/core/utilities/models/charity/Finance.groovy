package com.core.utilities.models.charity

class Finance {
    private String bankName
    private String accountName
    private String bsbNumber
    private String accountNumber
    private List<ContactPerson> contacts
    private List<Document> documents

    String getBankName() {
        return bankName
    }

    void setBankName(String bankName) {
        this.bankName = bankName
    }

    String getAccountName() {
        return accountName
    }

    void setAccountName(String accountName) {
        this.accountName = accountName
    }

    String getBsbNumber() {
        return bsbNumber
    }

    void setBsbNumber(String bsbNumber) {
        this.bsbNumber = bsbNumber
    }

    String getAccountNumber() {
        return accountNumber
    }

    void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber
    }

    List<ContactPerson> getContacts() {
        return contacts
    }

    void setContacts(List<ContactPerson> contacts) {
        this.contacts = contacts
    }

    List<Document> getDocuments() {
        return documents
    }

    void setDocuments(List<Document> documents) {
        this.documents = documents
    }
}
