package com.core.utilities.models.charity

class StateAuthority {

    private String state
    private String authority
    private String abbrState

    String getAbbreState() {
        return abbrState
    }

    void setAbbreState(String abbreState) {
        this.abbrState = abbreState
    }

    String getState() {
        return state
    }

    void setState(String state) {
        this.state = state
    }

    String getAuthority() {
        return authority
    }

    void setAuthority(String authority) {
        this.authority = authority
    }
}