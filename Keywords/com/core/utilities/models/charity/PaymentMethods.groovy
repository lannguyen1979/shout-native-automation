package com.core.utilities.models.charity

class PaymentMethods {
    private String charityId = null
    private boolean creditCard
    private boolean paypal
    private boolean applePay
    private boolean googlePay
    private boolean active = false
    private String source = null

    String getCharityId() {
        return charityId
    }

    void setCharityId(String charityId) {
        this.charityId = charityId
    }

    boolean getCreditCard() {
        return creditCard
    }

    void setCreditCard(boolean creditCard) {
        this.creditCard = creditCard
    }

    boolean getPaypal() {
        return paypal
    }

    void setPaypal(boolean paypal) {
        this.paypal = paypal
    }

    boolean getApplePay() {
        return applePay
    }

    void setApplePay(boolean applePay) {
        this.applePay = applePay
    }

    boolean getGooglePay() {
        return googlePay
    }

    void setGooglePay(boolean googlePay) {
        this.googlePay = googlePay
    }

    boolean getActive() {
        return active
    }

    void setActive(boolean active) {
        this.active = active
    }

    String getSource() {
        return source
    }

    void setSource(String source) {
        this.source = source
    }

/*this is required for toString model comparing*/

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PaymentMethods{");
        sb.append("charityId='").append(charityId).append('\'');
        sb.append(", creditCard=").append(creditCard);
        sb.append(", paypal=").append(paypal);
        sb.append(", applePay=").append(applePay);
        sb.append(", googlePay=").append(googlePay);
        sb.append(", active=").append(active);
        sb.append(", source='").append(source).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
