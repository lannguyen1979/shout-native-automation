package com.core.utilities.models.charity

/*this model is for product(s) of charity/fundraiser api/products/list/{charityId}*/
class CustomDonationProduct {
    private int id
    private String impact
    private String price

    int getId() {
        return id
    }

    void setId(int id) {
        this.id = id
    }

    String getImpact() {
        return impact
    }

    void setImpact(String impact) {
        this.impact = impact
    }

    String getPrice() {
        return price
    }

    void setPrice(String price) {
        this.price = price
    }
}
