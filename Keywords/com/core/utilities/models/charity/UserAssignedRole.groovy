package com.core.utilities.models.charity

class UserAssignedRole {

    private String email
    private String firstName
    private String lastName
    private String role
    private String charityId

    String getEmail() {
        return email
    }

    void setEmail(String email) {
        this.email = email
    }

    String getFirstName() {
        return firstName
    }

    void setFirstName(String firstName) {
        this.firstName = firstName
    }

    String getLastName() {
        return lastName
    }

    void setLastName(String lastName) {
        this.lastName = lastName
    }

    String getRole() {
        return role
    }

    void setRole(String role) {
        this.role = role
    }

    String getCharityId() {
        return charityId
    }

    void setCharityId(String charityId) {
        this.charityId = charityId
    }
}
