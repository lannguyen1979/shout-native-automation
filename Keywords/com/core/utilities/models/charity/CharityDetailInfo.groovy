package com.core.utilities.models.charity

import com.core.utilities.models.uploadFile.FinancialDetail

class CharityDetailInfo {

	private String id
	private String slug
	private String name
	private String legalName
	private String abn
	private String purpose
	private String logo
	private Location location //it's principal location
	private Social social
	private FinancialDetail finance
	private Boolean hasDgr
	private String status
	private String reviewStatus
	private String createdDate
	private String entityType
	private List<ContactPerson> contactPersons
	private List<Fundraisings> fundraising
	private List<StateAuthority> fundraisings
	private List<ResponsiblePerson> responsiblePersons
	private List<Document> dgrDocuments
	private CharityOwner charityOwner
	private Location registerdLocation

	Location getRegisterdLocation() {
		return registerdLocation
	}

	void setRegisterdLocation(Location registerdLocation) {
		this.registerdLocation = registerdLocation
	}

	String getCreatedDate() {
		return createdDate
	}

	void setCreatedDate(String createdDate) {
		this.createdDate = createdDate
	}

	CharityOwner getCharityOwner() {
		return charityOwner
	}

	void setCharityOwner(CharityOwner charityOwner) {
		this.charityOwner = charityOwner
	}

	List<Document> getDgrDocuments() {
		return dgrDocuments
	}

	void setDgrDocuments(List<Document> dgrDocuments) {
		this.dgrDocuments = dgrDocuments
	}

	String getName() {
		return name
	}

	void setName(String name) {
		this.name = name
	}

	String getEntityType() {
		return entityType
	}

	void setEntityType(String entityType) {
		this.entityType = entityType
	}

	String getId() {
		return id
	}

	void setId(String id) {
		this.id = id
	}

	String getSlug() {
		return slug
	}

	void setSlug(String slug) {
		this.slug = slug
	}

	String getLegalName() {
		return legalName
	}

	void setLegalName(String legalName) {
		this.legalName = legalName
	}

	String getShortDescription() {
		return shortDescription
	}

	void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription
	}

	String getLongDescription() {
		return longDescription
	}

	void setLongDescription(String longDescription) {
		this.longDescription = longDescription
	}

	String getLogo() {
		return logo
	}

	void setLogo(String logo) {
		this.logo = logo
	}

	List<ContactPerson> getContactPersons() {
		return contactPersons
	}

	void setContactPersons(List<ContactPerson> contactPersons) {
		this.contactPersons = contactPersons
	}

	Location getLocation() {
		return location
	}

	void setLocation(Location location) {
		this.location = location
	}

	Social getSocial() {
		return social
	}

	void setSocial(Social social) {
		this.social = social
	}

	List<Fundraisings> getFundraising() {
		return fundraising
	}

	void setFundraising(List<Fundraisings> fundraising) {
		this.fundraising = fundraising
	}

	FinancialDetail getFinance() {
		return finance
	}

	void setFinance(FinancialDetail finance) {
		this.finance = finance
	}

	String getAbn() {
		return abn
	}

	void setAbn(String abn) {
		this.abn = abn
	}

	String getPurpose() {
		return purpose
	}

	void setPurpose(String purpose) {
		this.purpose = purpose
	}

	Boolean getHasDgr() {
		return hasDgr
	}

	void setHasDgr(Boolean hasDgr) {
		this.hasDgr = hasDgr
	}

	List<StateAuthority> getFundraisings() {
		return fundraisings
	}

	void setFundraisings(List<StateAuthority> fundraisings) {
		this.fundraisings = fundraisings
	}

	List<ResponsiblePerson> getResponsiblePersons() {
		return responsiblePersons
	}

	void setResponsiblePersons(List<ResponsiblePerson> responsiblePersons) {
		this.responsiblePersons = responsiblePersons
	}

	String getStatus() {
		return status
	}

	void setStatus(String status) {
		this.status = status
	}

	String getReviewStatus() {
		return reviewStatus
	}

	void setReviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus
	}
}
