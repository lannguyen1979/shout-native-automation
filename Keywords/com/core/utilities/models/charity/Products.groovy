package com.core.utilities.models.charity


import org.apache.commons.lang3.StringUtils

class Products {

    private String id
    private String impact
    private List<HashMap<String, String>> prices


    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    String getImpact() {
        return impact
    }

    void setImpact(String impact) {
        this.impact = impact
    }

    String getPrices() {
        return prices
    }

    void setPrices(String prices) {
        this.prices = prices
    }

    @Override
    String toString() {
        final StringBuilder sb = new StringBuilder("{")
        sb.append("id='").append(id).append('\'')
        sb.append(", impact='").append(impact).append('\'')
        sb.append(", prices='").append(getProductPrice(getProductPrice(prices))).append('\'')
        sb.append('}')
        return sb.toString()
    }


    private static List<HashMap<String, String>> parseHashPrices(String srcString) {

        def hashValue = srcString.substring(srcString.indexOf("\n") + 1).trim()
        println "line 2 = ${hashValue}"
        List<String> list = hashValue.split("\n")
        List<HashMap<String, String>> hashMapList = new ArrayList<>()
        for (int i = 0; i < list.size(); i++) {
            HashMap<String, String> hashMap = new HashMap<>()
            String currency = ""
            String amount = "0"
            def item = list.get(i)
            amount = item.substring(item.indexOf(": ") + 3, item.size() - 1)
            hashMap.put("amount", amount)
            println "hashMap amount = ${hashMap.get("amount")}"
            def stCharacter = item.substring(0, 1)
            if (StringUtils.equals(stCharacter, "!")) {
                currency = item.substring(item.indexOf(" ") + 2, item.indexOf(": ") - 1)
                hashMap.put("currency", currency)
                println "abnormal currency = ${hashMap.get("currency")}"
            } else {
                currency = item.substring(0, item.indexOf(" ") - 1)
                hashMap.put("currency", currency)
                println "normal currency = ${hashMap.get("currency")}"
            }
            hashMapList.add(i, hashMap)
        }
        return hashMapList
    }

    private static String getProductPrice(List<HashMap<String, String>> prices) {
        for (HashMap<String, String> price: prices){
            if (StringUtils.equals(price.get("currency"), "AUD")){
                return price.get("amount")
            }
        }

    }
}
