package com.core.utilities.models.charity

class Fundraisings {

    private String authority
    private String licenceNo
    private String state

    String getAuthority() {
        return authority
    }

    void setAuthority(String authority) {
        this.authority = authority
    }


    String getLicenceNo() {
        return licenceNo
    }

    void setLicenceNo(String licenceNo) {
        this.licenceNo = licenceNo
    }

    String getState() {
        return state
    }

    void setState(String state) {
        this.state = state
    }


    @Override
    String toString() {
        final StringBuilder sb = new StringBuilder("{")
        sb.append("authority='").append(authority).append('\'')
        sb.append(", licenceNo='").append(licenceNo).append('\'')
        sb.append(", state='").append(state).append('\'')
        sb.append('}')
        return sb.toString()
    }
}
