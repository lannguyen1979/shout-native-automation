package com.core.utilities.models.charity

import internal.GlobalVariable

class CharityUser {
    private String username
    private String password
    private String otp = "999999"
    private String recaptchaResponse = "03AHaCkAbuLqdmopfrjC6BCo7m5tE_H4GEJYkaUqtRD8ciugOE18-CARr_HTZbUhv_tkgJilavZSVh5H-QidmoYsZjUrfzkL8PbKTgYrIu8DHwbnZWDP7Wiep61VUOXDxjiGX7ZTnChs6bVQKW3U6AcJbTRbw206_39lcvF2nsp3F9A29g90v2aBtBJrQc6M69xC2istbzSna9eLVAA7VCYL4HtGW-m8PM-v2fNoY1kUbSfQOnZMzKx0RFXGVcqo1HtBKkcO78YZIuLVg6_zSE9A4pNX1hpQ54zbcBmkOkpR0kkmzPXesZGAYuvd6U6QxtoBijTbNuOlO9cQmY8ezFgNA0YF92MYxzTcv1jCWZ8JCwLD0Wd0hUBhcqsuDP10yErG1Ck1m9QosJZ_zMGG-g0QbxMbUgTChFRw"
	
    String getUsername() {
        return username
    }

    void setUsername(String username) {
        this.username = username
    }

    String getPassword() {
        return password
    }

    void setPassword(String password) {
        this.password = password
    }

    String getOpt() {
        return opt
    }

    void setOpt(String opt) {
        this.opt = opt
    }

    String getRecaptchaResponse() {
        return recaptchaResponse
    }

    void setRecaptchaResponse(String recaptchaResponse) {
        this.recaptchaResponse = recaptchaResponse
    }

    String getOtp() {
        return otp
    }

    void setOtp(String otp) {
        this.otp = otp
    }
}
