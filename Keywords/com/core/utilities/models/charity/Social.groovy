package com.core.utilities.models.charity

class Social {

    private String website
    private String facebook
    private String twitter
    private String instagram
    private String youtube

    String getWebsite() {
        return website
    }

    void setWebsite(String website) {
        this.website = website
    }

    String getFacebook() {
        return facebook
    }

    void setFacebook(String facebook) {
        this.facebook = facebook
    }

    String getTwitter() {
        return twitter
    }

    void setTwitter(String twitter) {
        this.twitter = twitter
    }

    String getInstagram() {
        return instagram
    }

    void setInstagram(String instagram) {
        this.instagram = instagram
    }

    String getYoutube() {
        return youtube
    }

    void setYoutube(String youtube) {
        this.youtube = youtube
    }


    @Override
    String toString() {
        if (Objects.isNull(website)&&
                Objects.isNull(facebook) &&
                Objects.isNull(twitter) &&
                Objects.isNull(instagram) &&
                Objects.isNull(youtube)) {
            return null
        }else {
            final StringBuilder sb = new StringBuilder("{")
            sb.append("website='").append(website).append('\'')
            sb.append(", facebook='").append(facebook).append('\'')
            sb.append(", twitter='").append(twitter).append('\'')
            sb.append(", instagram='").append(instagram).append('\'')
            sb.append(", youtube='").append(youtube).append('\'')
            sb.append('}')
            return sb.toString()
        }

    }
}
