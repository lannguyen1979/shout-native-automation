package com.core.utilities.models.charity

import com.core.utilities.models.DonorUserInfo

import internal.GlobalVariable

class CharitySignUp {
	private String recaptchaResponse = "False"
	private String formId = "COMPONENT_SIGN_UP"
	private CharityUserInfo user
	private String otp = GlobalVariable.G_otpNumber

	String getRecaptchaResponse() {
		return recaptchaResponse
	}

	void setRecaptchaResponse(String recaptchaResponse) {
		this.recaptchaResponse = recaptchaResponse
	}

	String getFormId() {
		return formId
	}

	void setFormId(String formId) {
		this.formId = formId
	}

	DonorUserInfo getUserInfo() {
		return user
	}

	void setUserInfo(CharityUserInfo userInfo) {
		this.user = userInfo
	}

	String getOtp() {
		return otp
	}

	void setOtp(String otp) {
		this.otp = otp
	}
}
