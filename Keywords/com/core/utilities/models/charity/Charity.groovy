package com.core.utilities.models.charity

class Charity {

	private String id
	private String name
	private String abn
	private String hasDgr
	private String reviewStatus
	private String status
	private String profileStatus
	private String createdDate

	String getCreatedDate() {
		return createdDate
	}

	void setCreatedDate(String createdDate) {
		this.createdDate = createdDate
	}

	String getHasDgr() {
		return hasDgr
	}

	void setHasDgr(String hasDgr) {
		this.hasDgr = hasDgr
	}

	String getId() {
		return id
	}

	void setId(String id) {
		this.id = id
	}

	String getName() {
		return name
	}

	void setName(String name) {
		this.name = name
	}

	String getAbn() {
		return abn
	}

	void setAbn(String abn) {
		this.abn = abn
	}

	String getReviewStatus() {
		return reviewStatus
	}

	void setReviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus
	}

	String getStatus() {
		return status
	}

	void setStatus(String status) {
		this.status = status
	}

	String getProfileStatus() {
		return profileStatus
	}

	void setProfileStatus(String profileStatus) {
		this.profileStatus = profileStatus
	}

	@Override
	String toString() {
		final StringBuilder sb = new StringBuilder("{")
		sb.append("id='").append(id).append('\'')
		sb.append(", name='").append(name).append('\'')
		sb.append(", abn='").append(trimSpaces(abn)).append('\'')
		sb.append(", hasDgr='").append(trimSpaces(hasDgr)).append('\'')
		sb.append(", reviewStatus='").append(reviewStatus).append('\'')
		sb.append(", status='").append(status).append('\'')
		sb.append('}')
		return sb.toString()
	}

	static String trimSpaces(String str){
		if (Objects.nonNull(str)){
			return str.replaceAll(" ", "")
		}
	}
}
