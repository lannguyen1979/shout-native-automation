package com.core.utilities.models.charity

class CharityUserInfo {

	private String firstName
	private String lastName
	private String email
	private String password
	private String role = "charity"
	private String phoneNumber = "0499830224"

	String getPhoneNumber() {
		return phoneNumber
	}

	void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber
	}

	String getFirstName() {
		return firstName
	}

	void setFirstName(String firstName) {
		this.firstName = firstName
	}

	String getLastName() {
		return lastName
	}

	void setLastName(String lastName) {
		this.lastName = lastName
	}

	String getEmail() {
		return email
	}

	void setEmail(String email) {
		this.email = email
	}

	String getPassword() {
		return password
	}

	void setPassword(String password) {
		this.password = password
	}

	String getRole() {
		return role
	}

	void setRole(String role) {
		this.role = role
	}
}
