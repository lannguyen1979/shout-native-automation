package com.core.utilities.models.charity

class ContactPerson {
    private String email
    private String name
    private String phoneNumber
    private String id
    private Boolean isMain

    String getEmail() {
        return email
    }

    void setEmail(String email) {
        this.email = email
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getPhoneNumber() {
        return phoneNumber
    }

    void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    Boolean getIsMain() {
        return isMain
    }

    void setIsMain(Boolean isMain) {
        this.isMain = isMain
    }
}
