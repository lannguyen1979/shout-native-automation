package com.core.utilities.models.charity

class ResponsiblePerson {

    private int id
    private String address
    private String name
    private String citizenship
    private String otherCitizenship
    private String position
    private String birthDate
    private Boolean isMain

    int getId() {
        return id
    }

    void setId(int id) {
        this.id = id
    }

    String getAddress() {
        return address
    }

    void setAddress(String address) {
        this.address = address
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getCitizenship() {
        return citizenship
    }

    void setCitizenship(String citizenship) {
        this.citizenship = citizenship
    }

    String getOtherCitizenship() {
        return otherCitizenship
    }

    void setOtherCitizenship(String otherCitizenship) {
        this.otherCitizenship = otherCitizenship
    }

    String getPosition() {
        return position
    }

    void setPosition(String position) {
        this.position = position
    }

    String getBirthday() {
        return birthDate
    }

    void setBirthday(String birthday) {
        this.birthDate = birthday
    }

    Boolean getIsMain() {
        return isMain
    }

    void setIsMain(Boolean isMain) {
        this.isMain = isMain
    }
	
}
