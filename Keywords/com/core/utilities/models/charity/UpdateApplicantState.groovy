package com.core.utilities.models.charity

class UpdateApplicantState {

    private String section = "checklist"
    private String state
    private String type = "application_review_state"

    String getSection() {
        return section
    }

    void setSection(String section) {
        this.section = section
    }

    String getState() {
        return state
    }

    void setState(String state) {
        this.state = state
    }

    String getType() {
        return type
    }

    void setType(String type) {
        this.type = type
    }
}
