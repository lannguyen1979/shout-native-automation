package com.core.utilities.models.charity

class UpdateState {

	private String section = "cta"
	private String nextReviewState

	String getSection() {
		return section
	}

	void setSection(String section) {
		this.section = section
	}

	String getNextReviewState() {
		return nextReviewState
	}

	void setNextReviewState(String nextReviewState) {
		this.nextReviewState = nextReviewState
	}
}
