package com.core.utilities.models.charity

class SendOtp {
	private String method = "sms"
	private String phone = "0499830224"
	private String userName
	private String recaptchaResponse = "false"

	String getMethod() {
		return method
	}

	void setMethod(String method) {
		this.method = method
	}

	String getPhone() {
		return phone
	}

	void setPhone(String phone) {
		this.phone = phone
	}

	String getUserName() {
		return userName
	}

	void setUserName(String userName) {
		this.userName = userName
	}
}
