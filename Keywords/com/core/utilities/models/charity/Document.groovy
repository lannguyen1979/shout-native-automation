package com.core.utilities.models.charity

import org.apache.commons.lang3.builder.ToStringBuilder

class Document {
    private String id
    private String name
    private String file
    private String url
    private String size

    Document(String name){
        this.name = name
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    String getFile() {
        return file
    }

    void setFile(String file) {
        this.file = file
    }

    String getUrl() {
        return url
    }

    void setUrl(String url) {
        this.url = url
    }

    String getSize() {
        return size
    }

    void setSize(String size) {
        this.size = size
    }
}
