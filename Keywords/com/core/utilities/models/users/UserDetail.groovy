package com.core.utilities.models.users

class UserDetail {
	private String email
	private boolean enableTwoFactorAuth
	private String[] roles
	private String firstName
	private String lastName
	private String type
	private String status
	private String userId
	private String pendingRole

	String getEmail() {
		return email
	}

	void setEmail(String email) {
		this.email = email
	}

	boolean getEnableTwoFactorAuth() {
		return enableTwoFactorAuth
	}

	void setEnableTwoFactorAuth(boolean enableTwoFactorAuth) {
		this.enableTwoFactorAuth = enableTwoFactorAuth
	}

	String[] getRoles() {
		return roles
	}

	void setRoles(String[] roles) {
		this.roles = roles
	}

	String getFirstName() {
		return firstName
	}

	void setFirstName(String firstName) {
		this.firstName = firstName
	}

	String getLastName() {
		return lastName
	}

	void setLastName(String lastName) {
		this.lastName = lastName
	}

	String getType() {
		return type
	}

	void setType(String type) {
		this.type = type
	}

	String getStatus() {
		return status
	}

	void setStatus(String status) {
		this.status = status
	}

	String getUserId() {
		return userId
	}

	void setUserId(String userId) {
		this.userId = userId
	}

	String getPendingRole() {
		return pendingRole
	}

	void setPendingRole(String pendingRole) {
		this.pendingRole = pendingRole
	}
}
