package com.core.utilities.models.users

class UserPaymentMethod {
	private String cardExpiry
	private String cardNumber
	private String cardType
	private boolean isDefault
	private String type

	String getCardExpiry() {
		return cardExpiry
	}

	void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry
	}

	String getCardNumber() {
		return cardNumber
	}

	void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber
	}

	String getCardType() {
		return cardType
	}

	void setCardType(String cardType) {
		this.cardType = cardType
	}

	boolean getIsDefault() {
		return isDefault
	}

	void setIsDefault(boolean isDefault) {
		this.isDefault = isDefault
	}

	String getType() {
		return type
	}

	void setType(String type) {
		this.type = type
	}
}
