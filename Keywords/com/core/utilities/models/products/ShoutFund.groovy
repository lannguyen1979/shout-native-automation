package com.core.utilities.models.products

import com.core.utilities.models.charity.CustomDonationProduct

public class ShoutFund {

	private String charityName
	private String fundraiserName
	private String fundraiserSlug
	private String startDate
	private String endDate
	private String goal
	private String summary
	private List<CustomDonationProduct> defaultDonationProducts
	private List<CustomDonationProduct> customDonationProducts
	private List<CustomDonationProduct> newDonationItems
	private boolean isUseDefaultProducts
	private boolean isCustomImportDefaultProducts
	private boolean isCustomAddingNewProducts

	boolean getIsCustomAddingNewProducts() {
		return isCustomAddingNewProducts
	}

	void setIsCustomAddingNewProducts(boolean isCustomAddingNewProducts) {
		this.isCustomAddingNewProducts = isCustomAddingNewProducts
	}

	boolean getIsCustomImportDefaultProducts() {
		return isCustomImportDefaultProducts
	}

	void setIsCustomImportDefaultProducts(boolean isCustomImportDefaultProducts) {
		this.isCustomImportDefaultProducts = isCustomImportDefaultProducts
	}

	List<CustomDonationProduct> getNewDonationItems() {
		return newDonationItems
	}

	void setNewDonationItems(List<CustomDonationProduct> newDonationItems) {
		this.newDonationItems = newDonationItems
	}

	List<CustomDonationProduct> getDefaultDonationProducts() {
		return defaultDonationProducts
	}

	void setDefaultDonationProducts(List<CustomDonationProduct> defaultDonationProducts) {
		this.defaultDonationProducts = defaultDonationProducts
	}

	List<CustomDonationProduct> getCustomDonationProducts() {
		return customDonationProducts
	}

	void setCustomDonationProducts(List<CustomDonationProduct> customDonationProducts) {
		this.customDonationProducts = customDonationProducts
	}

	boolean getIsUseDefaultProducts() {
		return isUseDefaultProducts
	}

	void setIsUseDefaultProducts(boolean isCustomProduct) {
		this.isUseDefaultProducts = isCustomProduct
	}

	List<CustomDonationProduct> getDefaultProducts() {
		return defaultDonationProducts
	}

	void setDefaultProducts(List<CustomDonationProduct> customProducts) {
		this.defaultDonationProducts = customProducts
	}

	String getCharityName(){
		return charityName
	}

	void setCharityName(String charityname){
		this.charityName = charityname
	}

	String getFundraiserName(){
		return fundraiserName
	}

	void setFundraiserName(String fundraisername){
		this.fundraiserName = fundraisername
	}

	String getFundraiserSlug(){
		return fundraiserSlug
	}

	void setFundraiserSlug(String fundraiserslug){
		this.fundraiserSlug = fundraiserslug
	}

	String getStartDate(){
		return startDate
	}

	void setStartDate(String startdate){
		this.startDate = startdate
	}

	String getEndDate(){
		return endDate
	}

	void setEndDate(String enddate){
		this.endDate = enddate
	}

	String getGoal(){
		return goal
	}

	void setGoal(String goal){
		this.goal = goal
	}

	String getSummary(){
		return summary
	}

	void setSummary(String summary){
		this.summary = summary
	}
}

