package com.core.utilities.models

import com.core.utilities.common.Constants
import com.google.gson.Gson
import com.kms.katalon.core.testobject.*
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import groovy.json.JsonSlurper
import internal.GlobalVariable

import java.text.DecimalFormat

class FeesSetting {
    String paymentFee
    String adminFee
    String adminFeeGst
    String paymentFeeFlat
    String paymentFeeGst

    FeesSetting(){
    }

    static ResponseObject sendGetRequest(url) {
        List<TestObjectProperty> lst_DefaultHeaders = new ArrayList<>()
        lst_DefaultHeaders.add(new TestObjectProperty("Accept", ConditionType.EQUALS, 'application/json'))
        RequestObject request = new RestRequestObjectBuilder()
                .withRestRequestMethod("GET")
                .withRestUrl(url)
                .withHttpHeaders(lst_DefaultHeaders)
                .build()
        ResponseObject response = WSBuiltInKeywords.sendRequest(request)
        response
    }

    static FeesSetting getFeeValuesFromFeesSetting(ResponseObject response, String paymentType, String product = "fund") {
        JsonSlurper slurper = new JsonSlurper()
        List data = slurper.parseText(response.getResponseBodyContent()).data
        def item = data.find { item -> item.product == product && item.paymentType == paymentType }
        def result = new FeesSetting()
        result.with {
            paymentFee = item.paymentFee
            adminFee = item.adminFee
            adminFeeGst = item.adminFeeGst
            paymentFeeFlat = item.paymentFeeFlat
            paymentFeeGst = item.paymentFeeGst
        }
        return result
    }

    /**
     * @paymentType: PAYMENT_TYPE_VISA: visa, PAYMENT_TYPE_AMEX american_express, PAYMENT_TYPE_MASTER master, PAYMENT_TYPE_PAYPAL paypal
     * @product: blade, fund, button, sms**/
    FeesSetting getFeesSettings(String paymentType, String product) {
        ResponseObject fees = sendGetRequest(GlobalVariable.G_baseAPIUrl + Constants.FEES_SETTING_PATH)
        FeesSetting model = getFeeValuesFromFeesSetting(fees, paymentType, product)
        KeywordUtil.logInfo("paymentType = $paymentType having feesettings = ${new Gson().toJson(model)}")
        return model
    }

    float calculateTotalWithFees(String str_DonationAmount, FeesSetting feesModel) {
        /*This calculation is using for fee calculation
         * Following is Mapping between SA and Excel file
         1. Admin Fee (%) = Administration Fee  --------> ShoutCommissionFee
         2. Admin Fee GST (%) = Administration Tax -----> ShoutCommissionTax
         3. Payment Fee (%) = % Fee --------------------> PaymentPercentageFee
         4. Payment Fee Flat ($) = Flat Fee ------------> PaymentFlatFee
         5. Payment Fee GST (%) = Tax ------------------> PaymentTax
         */
        float float_DonationAmount = Float.parseFloat(str_DonationAmount)
        float float_AdminFee = Float.parseFloat(feesModel.getAdminFee())
        float float_AdminFeeGST = Float.parseFloat(feesModel.getAdminFeeGst())
        float float_PaymentFee =Float.parseFloat(feesModel.getPaymentFee())
        float float_PaymentFeeFlat = Float.parseFloat(feesModel.getPaymentFeeFlat())
        float float_PaymentFeeGST = Float.parseFloat(feesModel.getPaymentFeeGst())

        //corrected float_Dividend as new calculation
        float float_Dividend = (float_PaymentFeeFlat * float_PaymentFeeGST) + float_DonationAmount + float_PaymentFeeFlat
        float float_Divisor = - float_AdminFee - (float_AdminFee * float_AdminFeeGST) - float_PaymentFee - (float_PaymentFee * float_PaymentFeeGST) + 1
        float float_Quotient = float_Dividend / float_Divisor

        DecimalFormat decimalFormat = new DecimalFormat("#.00")
        float float_PaymentWithFee = Float.valueOf(decimalFormat.format(float_Quotient))
        return float_PaymentWithFee
    }
}
