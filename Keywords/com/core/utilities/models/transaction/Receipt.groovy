package com.core.utilities.models.transaction

import java.awt.geom.RoundRectangle2D

import org.apache.commons.lang3.StringUtils

class Receipt {

	private BillingInfo billingInfo
	private String charityName
	private int charityId
	private boolean DGRStatus
	private BillTo billTo
	private Float transactionFees
	private String currency
	private String total
	private List<OrderItems> orderItems

	BillingInfo getBillingInfo() {
		return billingInfo
	}

	void setBillingInfo(BillingInfo billingInfo) {
		this.billingInfo = billingInfo
	}

	String getCharityName() {
		return charityName
	}

	void setCharityName(String charityName) {
		this.charityName = charityName
	}

	int getCharityId() {
		return charityId
	}

	void setCharityId(int charityId) {
		this.charityId = charityId
	}

	boolean getDGRStatus() {
		return DGRStatus
	}

	void setDGRStatus(boolean DGRStatus) {
		this.DGRStatus = DGRStatus
	}

	BillTo getBillTo() {
		return billTo
	}

	void setBillTo(BillTo billTo) {
		this.billTo = billTo
	}

	Float getTransactionFees() {
		return transactionFees
	}

	void setTransactionFees(Float transactionFees) {
		this.transactionFees = transactionFees
	}

	String getCurrency() {
		return currency
	}

	void setCurrency(String currency) {
		this.currency = currency
	}

	String getTotal() {
		return total
	}

	void setTotal(String total) {
		this.total = total
	}

	List<OrderItems> getOrderItems() {
		return orderItems
	}

	void setOrderItems(List<OrderItems> orderItems) {
		this.orderItems = orderItems
	}


	@Override
	String toString() {
		final StringBuilder sb = new StringBuilder("Receipt{")
		sb.append("billingInfo=").append(billingInfo)
		sb.append(", charityName='").append(charityName).append('\'')
		sb.append(", charityId=").append(charityId)
		sb.append(", DGRStatus=").append(DGRStatus)
		sb.append(", billTo=").append(billTo)
		sb.append(", transactionFees=").append(transactionFees)
		sb.append(", currency='").append(currency).append('\'')
		sb.append(", total='").append(total).append('\'')
		sb.append(", orderItems=").append(orderItems)
		sb.append('}')
		return sb.toString()
	}
}
