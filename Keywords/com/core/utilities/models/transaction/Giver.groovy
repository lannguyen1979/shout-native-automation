package com.core.utilities.models.transaction

class Giver {
    private boolean shouter
    private String firstName
    private String lastName
    private String email
    private boolean anonymously
    private int donorId

    boolean getShouter() {
        return shouter
    }

    void setShouter(boolean shouter) {
        this.shouter = shouter
    }

    String getFirstName() {
        return firstName
    }

    void setFirstName(String firstName) {
        this.firstName = firstName
    }

    String getLastName() {
        return lastName
    }

    void setLastName(String lastName) {
        this.lastName = lastName
    }

    String getEmail() {
        return email
    }

    void setEmail(String email) {
        this.email = email
    }

    boolean getAnonymously() {
        return anonymously
    }

    void setAnonymously(boolean anonymously) {
        this.anonymously = anonymously
    }

    int getDonorId() {
        return donorId
    }

    void setDonorId(int donorId) {
        this.donorId = donorId
    }


    @Override
    String toString() {
        final StringBuilder sb = new StringBuilder("Giver{")
        sb.append("shouter=").append(shouter)
        sb.append(", firstName='").append(firstName).append('\'')
        sb.append(", lastName='").append(lastName).append('\'')
        sb.append(", email='").append(email).append('\'')
        sb.append(", anonymously=").append(anonymously)
        sb.append(", donorId=").append(donorId)
        sb.append('}')
        return sb.toString()
    }
}
