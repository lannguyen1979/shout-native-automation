package com.core.utilities.models.transaction

class TransactionDetails {
	private String id
	private String status
	private String referenceNo
	private String submittedDate
	private String processedDate
	private String product
	private String frequency
	private Giver giver
	private Receipt receipt
	private Attempts attempt

	String getId() {
		return id
	}

	void setId(String id) {
		this.id = id
	}

	String getStatus() {
		return status
	}

	void setStatus(String status) {
		this.status = status
	}

	String getReferenceNo() {
		return referenceNo
	}

	void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo
	}

	String getSubmittedDate() {
		return submittedDate
	}

	void setSubmittedDate(String submittedDate) {
		this.submittedDate = submittedDate
	}

	String getProcessedDate() {
		return processedDate
	}

	void setProcessedDate(String processedDate) {
		this.processedDate = processedDate
	}

	String getProduct() {
		return product
	}

	void setProduct(String product) {
		this.product = product
	}

	String getFrequency() {
		return frequency
	}

	void setFrequency(String frequency) {
		this.frequency = frequency
	}

	Giver getGiver() {
		return giver
	}

	void setGiver(Giver giver) {
		this.giver = giver
	}

	Receipt getReceipt() {
		return receipt
	}

	void setReceipt(Receipt receipt) {
		this.receipt = receipt
	}

	Attempts getAttempt() {
		return attempt
	}

	void setAttempt(Attempts attempt) {
		this.attempt = attempt
	}


	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("TransactionDetails{")
		sb.append("id='").append(id).append('\'')
		sb.append(", status='").append(status).append('\'')
		sb.append(", referenceNo='").append(referenceNo).append('\'')
		sb.append(", submittedDate='").append(submittedDate).append('\'')
		sb.append(", processedDate='").append(processedDate).append('\'')
		sb.append(", product='").append(product).append('\'')
		sb.append(", frequency='").append(frequency).append('\'')
		sb.append(", giver=").append(giver)
		sb.append(", receipt=").append(receipt)
		sb.append(", attempt=").append(attempt)
		sb.append('}')
		return sb.toString()
	}
}
