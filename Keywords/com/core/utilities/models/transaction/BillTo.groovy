package com.core.utilities.models.transaction

class BillTo {
    private String firstName
    private String lastName
    private String email

    String getFirstName() {
        return firstName
    }

    void setFirstName(String firstName) {
        this.firstName = firstName
    }

    String getLastName() {
        return lastName
    }

    void setLastName(String lastName) {
        this.lastName = lastName
    }

    String getEmail() {
        return email
    }

    void setEmail(String email) {
        this.email = email
    }


    @Override
    String toString() {
        final StringBuilder sb = new StringBuilder("BillTo{")
        sb.append("firstName='").append(firstName).append('\'')
        sb.append(", lastName='").append(lastName).append('\'')
        sb.append(", email='").append(email).append('\'')
        sb.append('}')
        return sb.toString()
    }
}
