package com.core.utilities.models.transaction


import static com.core.utilities.common.Constants.API_FREQUENCE_TYPE_ONE_TIME
import static com.core.utilities.common.Constants.CHARITY_PRODUCT
import static com.core.utilities.common.Constants.PAYMENT_TYPE_VISA
import static com.core.utilities.common.Constants.WEB_SOURCE

class TransactionRefine {

	private String shouterEmail
	private String shouterName
	private String status
	private String product
	private String source
	private String frequency
	private String paymentType
	private String fromDate
	private String toDate

	String getShouterEmail() {
		return shouterEmail
	}

	void setShouterEmail(String shouterEmail) {
		this.shouterEmail = shouterEmail
	}

	String getStatus() {
		return status
	}

	void setStatus(String status) {
		this.status = status
	}

	String getProduct() {
		return product
	}

	void setProduct(String product) {
		this.product = product
	}

	String getShouterName() {
		return shouterName
	}

	void setShouterName(String shouterName) {
		this.shouterName = shouterName
	}

	String getSource() {
		return source
	}

	void setSource(String source) {
		this.source = source
	}

	String getFrequency() {
		return frequency
	}

	void setFrequency(String frequency) {
		this.frequency = frequency
	}

	String getPaymentType() {
		return paymentType
	}

	void setPaymentType(String paymentType) {
		this.paymentType = paymentType
	}

	String gettoDate(){
		return toDate
	}

	void settoDate(String toDate) {
		this.toDate = toDate
	}

	String getfromDate(){
		return fromDate
	}

	void setfromDate(String fromDate) {
		this.fromDate = fromDate
	}

	TransactionRefine() {}

	TransactionRefine(String shouterEmail, String status, String product = CHARITY_PRODUCT, String source = WEB_SOURCE, String frequency = API_FREQUENCE_TYPE_ONE_TIME, String paymentType = PAYMENT_TYPE_VISA, String fromDate) {
		this.shouterEmail = shouterEmail
		this.status = status
		this.product = product
		this.source = source
		this.frequency = frequency
		this.paymentType = paymentType
		this.fromDate = fromDate
	}

	void setTransactionRefine(String shouterEmail, String status, String product = 'fund', String source = "web", String frequency = "one_time", String paymentType = "visa", String fromDate) {
		this.shouterEmail = shouterEmail
		this.status = status
		this.product = product
		this.source = source
		this.frequency = frequency
		this.paymentType = paymentType
		this.fromDate = fromDate
	}
}
