package com.core.utilities.models.transaction

class Attempts {
    private String gateway
    private List<TXNAttempts> txnAttempts

    String getGateway() {
        return gateway
    }

    void setGateway(String gateway) {
        this.gateway = gateway
    }

    List<TXNAttempts> getTxnAttempts() {
        return txnAttempts
    }

    void setTxnAttempts(List<TXNAttempts> txnAttempts) {
        this.txnAttempts = txnAttempts
    }


    @Override
    String toString() {
        final StringBuilder sb = new StringBuilder("Attempts{")
        sb.append("gateway='").append(gateway).append('\'')
        sb.append(", txnAttempts=").append(txnAttempts)
        sb.append('}')
        return sb.toString()
    }
}
