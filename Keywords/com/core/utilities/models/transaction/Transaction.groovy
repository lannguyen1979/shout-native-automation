package com.core.utilities.models.transaction

class Transaction {
	private int id
	private String submittedDate
	private String total
	private String state
	private String currency
	private String firstName
	private String lastName
	private String paymentMethod
	private String charityName
	private String product
	private String frequency

	String getState() {
		return state
	}

	void setState(String state) {
		this.state = state
	}

	String getSubmittedDate() {
		return submittedDate
	}

	void setSubmittedDate(String submittedDate) {
		this.submittedDate = submittedDate
	}

	String getTotal() {
		return total
	}

	void setTotal(String total) {
		this.total = total
	}

	String getFirstName() {
		return firstName
	}

	void setFirstName(String firstName) {
		this.firstName = firstName
	}

	String getLastName() {
		return lastName
	}

	void setLastName(String lastName) {
		this.lastName = lastName
	}

	String getPaymentMethod() {
		return paymentMethod
	}

	void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod
	}

	String getCharityName() {
		return charityName
	}

	void setCharityName(String charityName) {
		this.charityName = charityName
	}

	String getProduct() {
		return product
	}

	void setProduct(String product) {
		this.product = product
	}

	String getFrequency() {
		return frequency
	}

	void setFrequency(String frequency) {
		this.frequency = frequency
	}

	String getCurrency() {
		return currency
	}

	void setCurrency(String currency) {
		this.currency = currency
	}

	int getId() {
		return id
	}

	void setId(int id) {
		this.id = id
	}


	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("{")
		sb.append("id='").append(id).append('\'')
		sb.append(", submittedDate='").append(submittedDate).append('\'')
		//        sb.append(", total='").append(total).append('\'')
		sb.append(", state='").append(state).append('\'')
		sb.append(", currency='").append(currency).append('\'')
		sb.append(", firstName='").append(firstName).append('\'')
		//        sb.append(", lastName='").append(lastName).append('\'')
		sb.append(", paymentMethod='").append(paymentMethod).append('\'')
		sb.append(", charityName='").append(charityName).append('\'')
		//        sb.append(", product='").append(product).append('\'')
		sb.append(", frequency='").append(frequency).append('\'')
		sb.append('}')
		return sb.toString()
	}
}
