package com.core.utilities.models.transaction

class TXNAttempts {
    private int id
    private String date
    private String status
    private String reason

    int getId() {
        return id
    }

    void setId(int id) {
        this.id = id
    }

    String getDate() {
        return date
    }

    void setDate(String date) {
        this.date = date
    }

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }

    String getReason() {
        return reason
    }

    void setReason(String reason) {
        this.reason = reason
    }
}
