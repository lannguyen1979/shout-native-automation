package com.core.utilities.models.transaction

class BillingInfo {
    private String cardType
    private String endingDigit
    private String paymentType

    String getCardType() {
        return cardType
    }

    void setCardType(String cardType) {
        this.cardType = cardType
    }

    String getEndingDigit() {
        return endingDigit
    }

    void setEndingDigit(String endingDigit) {
        this.endingDigit = endingDigit
    }

    String getPaymentType() {
        return paymentType
    }

    void setPaymentType(String paymentType) {
        this.paymentType = paymentType
    }


    @Override
    String toString() {
        final StringBuilder sb = new StringBuilder("BillingInfo{")
        sb.append("cardType='").append(cardType).append('\'')
        sb.append(", endingDigit='").append(endingDigit).append('\'')
        sb.append(", paymentType='").append(paymentType).append('\'')
        sb.append('}')
        return sb.toString()
    }
}
