package com.core.utilities.models.transaction

class OrderItems {
    private Double amount
    private int quantity
    private String currency
    private boolean isCustom

    Double getAmount() {
        return amount
    }

    void setAmount(Double amount) {
        this.amount = amount
    }

    int getQuantity() {
        return quantity
    }

    void setQuantity(int quantity) {
        this.quantity = quantity
    }

    String getCurrency() {
        return currency
    }

    void setCurrency(String currency) {
        this.currency = currency
    }

    boolean getIsCustom() {
        return isCustom
    }

    void setIsCustom(boolean isCustom) {
        this.isCustom = isCustom
    }

    @Override
    String toString() {
        final StringBuilder sb = new StringBuilder("OrderItems{")
        sb.append("amount='").append(amount).append('\'')
        sb.append(", quantity=").append(quantity)
        sb.append(", currency='").append(currency).append('\'')
        sb.append(", isCustom=").append(isCustom)
        sb.append('}')
        return sb.toString()
    }
}
