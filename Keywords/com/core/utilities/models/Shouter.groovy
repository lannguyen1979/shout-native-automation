package com.core.utilities.models

import com.google.gson.annotations.SerializedName

class Shouter {
	private String country = "AU"
	@SerializedName("first_name")
	private String firstName = "AutoTest"

	@SerializedName("last_name")
	private String lastName = "Last"
	private String company

	String getCountry() {
		return country
	}

	void setCountry(String country) {
		this.country = country
	}

	String getFirstName() {
		return firstName
	}

	void setFirstName(String firstName) {
		this.firstName = firstName
	}

	String getLastName() {
		return lastName
	}

	void setLastName(String lastName) {
		this.lastName = lastName
	}

	String getCompany() {
		return company
	}

	void setCompany(String company) {
		this.company = company
	}
}
