package com.core.utilities.models

import com.google.gson.annotations.SerializedName

class DonationPayment {
    private String recurring = ""

    @SerializedName("include_fee")
    private String includeFee = "true"
    private String type = "credit_card"

    @SerializedName("cc")
    private CreditCard creditCard

    String getRecurring() {
        return recurring
    }

    void setRecurring(String recurring) {
        this.recurring = recurring
    }

    String getIncludeFee() {
        return includeFee
    }

    void setIncludeFee(String includeFee) {
        this.includeFee = includeFee
    }

    String getType() {
        return type
    }

    void setType(String type) {
        this.type = type
    }

    CreditCard getCreditCard() {
        return creditCard
    }

    void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard
    }


}

