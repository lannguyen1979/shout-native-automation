package com.core.utilities.models

import com.google.gson.annotations.SerializedName

class ShoutButton {
	@SerializedName("name")
	private String buttonName

	@SerializedName("id")
	private String buttonId

	private String charityName
	private int charityId

	@SerializedName("token")
	private String lightBoxToken

	@SerializedName("smsSlug")
	private String slug

	String getButtonName() {
		return buttonName
	}

	void setButtonName(String buttonName) {
		this.buttonName = buttonName
	}

	String getCharityName() {
		return charityName
	}

	void setCharityName(String charityName) {
		this.charityName = charityName
	}

	int getCharityId() {
		return charityId
	}

	void setCharityId(int charityId) {
		this.charityId = charityId
	}

	String getLightboxToken() {
		return lightBoxToken
	}

	void setLightboxToken(String lightboxToken) {
		this.lightBoxToken = lightboxToken
	}

	String getSlug() {
		return slug
	}

	void setSlug(String slug) {
		this.slug = slug
	}

	String getButtonId() {
		return buttonId
	}

	void setButtonId(String buttonId) {
		this.buttonId = buttonId
	}

	String getLightBoxToken() {
		return lightBoxToken
	}

	void setLightBoxToken(String lightBoxToken) {
		this.lightBoxToken = lightBoxToken
	}
}
