package com.core.utilities.models

class User {
    private String id
    private String firstName
    private String lastName
    private String phoneNumber
    private String email
    private String status
    private String type
    private String[] roles
	private boolean twoFactor = null
	private String pendingRole

    String getPendingRole() {
        return pendingRole
    }

    void setPendingRole(String pendingRole) {
        this.pendingRole = pendingRole
    }

    boolean getTwoFactor() {
        return twoFactor
    }

    void setTwoFactor(boolean twoFactor) {
        this.twoFactor = twoFactor
    }

    String[] getRoles() {
        return roles
    }

    void setRoles(String[] roles) {
        this.roles = roles
    }

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    String getFirstName() {
        return firstName
    }

    void setFirstName(String firstName) {
        this.firstName = firstName
    }

    String getLastName() {
        return lastName
    }

    void setLastName(String lastName) {
        this.lastName = lastName
    }

    String getPhoneNumber() {
        return phoneNumber
    }

    void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber
    }

    String getEmail() {
        return email
    }

    void setEmail(String email) {
        this.email = email
    }

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }

    String getType() {
        return type
    }

    void setType(String type) {
        this.type = type
    }
}
