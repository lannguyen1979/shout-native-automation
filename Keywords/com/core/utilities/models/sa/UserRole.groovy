package com.core.utilities.models.sa

public class UserRole {
	private String roleGroup
	private String role

	String getroleGroup(){
		return roleGroup
	}

	void setroleGroup(String roleGroups){
		this.roleGroup = roleGroup
	}

	String getrole(){
		return role
	}

	void setrole(String role){
		this.role = role
	}

	String toQueryString() {
		return roleGroup + ":" + role
	}
}
