package com.core.utilities.models.sa

import com.core.utilities.common.CommonActions
import com.core.utilities.common.Enum
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable

public class CharityDetail {

	CommonActions common = new CommonActions()

	/* about your charity section */
	private String entityType
	private String charityName
	private String abn
	private String fullLegalName
	private String charityPurpose
	private String principleFullAddress
	private String principleAddress
	private String principleSuburb
	private String principleState
	private String principlePostcode
	private String registeredAddress
	private String mainContactName
	private String mainContactEmail
	private String mainPhoneNumber

	String getEntityType(){
		return entityType
	}

	void setEntityType(String entityType) {
		this.entityType = entityType
	}

	String getCharityName(){
		return charityName
	}

	void setCharityName(String charityName) {
		this.charityName = charityName
	}

	String getAbn(){
		return abn
	}

	void setAbn(String abn){
		this.abn = abn
	}
	String getFullLegalname(){
		return fullLegalName
	}

	void setFullLegalName(String fullLegalName){
		this.fullLegalName = fullLegalName
	}

	String getcharityPurpose(){
		return charityPurpose
	}

	void setCharityPurpose(String charityPurpose){
		this.charityPurpose = charityPurpose
	}

	String getprincipleFullAddress(){
		return principleFullAddress
	}

	String getprincipleSuburb(){
		return principleSuburb
	}

	void setprincipleSuburb(String principleSuburb){
		this.principleSuburb = principleSuburb
	}

	String getprincipleState(){
		return principleState
	}

	void setprincipleState(String principleState){
		this.principleState = principleState
	}

	String getprinciplepostcode(){
		return principlePostcode
	}

	void setprinciplePostcode(String principlePostcode){
		this.principlePostcode = principlePostcode
	}

	String getprincipleAddress(){
		return principleAddress
	}

	void setprincipleAddress(String principleAddress){
		this.principleAddress = principleAddress
	}

	void setprincipleFullAddress(String principleFullAddress){
		this.principleFullAddress = principleFullAddress
	}

	String getRegisteredAddress(){
		return registeredAddress
	}

	void setregisteredAddress(String registeredAddress){
		this.registeredAddress = registeredAddress
	}

	String getMainContactName(){
		return mainContactName
	}

	void setmainContactName(String mainContactName){
		this.mainContactName = mainContactName
	}

	String getMainContactEmail(){
		return mainContactEmail
	}

	void setMainContactEmail(String mainContactEmail){
		this.mainContactEmail = mainContactEmail
	}

	String getMainPhoneNumber() {
		return mainPhoneNumber;
	}

	void setMainContactPhoneNumber(String mainPhoneNumber){
		this.mainPhoneNumber = mainPhoneNumber
	}

	/* Finance Detail section */
	private String bankName
	private String accountName
	private String bsb
	private String bankaccountNumber
	private String supportingDocument
	private String financialContactName
	private String financialContactEmail
	private String financialContactPhoneNumber

	String getBankName() {
		return bankName
	}

	void setBankName(String bankName){
		this.bankName = bankName
	}

	String getAccountName(){
		return accountName
	}

	void setaccountName(String accountName){
		this.accountName = accountName
	}

	String getSupportingDocument(){
		return supportingDocument
	}

	void setSupportingDocument(String supportingDocument){
		this.supportingDocument = supportingDocument
	}

	String getFinancialContactName(){
		return financialContactName
	}

	void setfinancialContactName(String financialContactName){
		this.financialContactName = financialContactName
	}

	String getFinancialContactEmail(){
		return financialContactEmail
	}

	void setfinancialContactEmail(String financialContactEmail){
		this.financialContactEmail = financialContactEmail
	}

	String getFinancialContactPhoneNumber(){
		return financialContactPhoneNumber
	}

	void setfinancialContactPhoneNumber(String financialContactPhoneNumber){
		this.financialContactPhoneNumber = financialContactPhoneNumber
	}

	String getBSB(){
		return bsb
	}

	void setBSB(String bsb){
		this.bsb = bsb
	}

	String getBankAccountNumber(){
		return bankaccountNumber
	}

	void setBankAccountNumber(String bankaccountNumber){
		this.bankaccountNumber = bankaccountNumber
	}

	/* DGR Status section */
	private String dgrStatus

	String getDGRStatus(){
		return dgrStatus
	}

	void setDGRStatus(String dgrStatus){
		this.dgrStatus = dgrStatus
	}

	/* Fundraising Authority Section */

	private String australiancapitalterritoryStatus
	private String newsouthwaleStatus
	private String northenterritoryStatus
	private String queenlandStatus
	private String tasmaniaStatus
	private String southaustraliaStatus
	private String victoriaStatus
	private String westernAustraliaStatus

	String getVictoriaStatus(){
		return victoriaStatus
	}

	void setVictoriaStatus(String victoriaStatus){
		this.victoriaStatus = victoriaStatus
	}

	String getnewsouthwaleStatus(){
		return newsouthwaleStatus
	}

	void setNewSouthWaleStatus(String newsouthwaleStatus){
		this.newsouthwaleStatus = newsouthwaleStatus
	}

	String getnorthenterritoryStatus(){
		return northenterritoryStatus
	}

	void setNorthernTerritoryStatus(String northenterritoryStatus){
		this.northenterritoryStatus = northenterritoryStatus
	}

	String getqueenlandStatus(){
		return queenlandStatus
	}

	void setQueenlandStatus(String queenlandStatus){
		this.queenlandStatus = queenlandStatus
	}

	String getTasmaniaStatus(){
		return tasmaniaStatus
	}

	void setTasmaniaStatus(String tasmaniaStatus){
		this.tasmaniaStatus = tasmaniaStatus
	}

	String getSouthAustraliaStatus(){
		return southaustraliaStatus
	}

	void setSouthAustraliaStatus(String southaustraliaStatus){
		this.southaustraliaStatus = southaustraliaStatus
	}

	String getAustralianCapitalTerritoryStatus(String australiancapitalterritoryStatus){
		return australiancapitalterritoryStatus
	}

	void setAustralianCapitalTerritoryStatus(String australiancapitalterritoryStatus){
		this.australiancapitalterritoryStatus = australiancapitalterritoryStatus
	}

	String getwesternAustraliaStatus(){
		return westernAustraliaStatus
	}

	void setwesternAustraliaStatus(String westernAustraliaStatus){
		this.westernAustraliaStatus = westernAustraliaStatus
	}

	/* Responsible Person Sections */
	private String responsiblePersonName
	private String responsiblePersonPosition
	private String responsibleDateOfBirth
	private String responsibleAddress
	private String responsibleCitizenship
	private String responsibleOtherCitizenship

	String getResponsiblePersonName(){
		return responsiblePersonName
	}

	void setResponsiblePersonName(String responsiblePersonName){
		this.responsiblePersonName = responsiblePersonName
	}

	String getResponsiblePersonPosition(){
		return responsiblePersonPosition
	}

	void setResponsiblePersonPosition(String responsiblePersonPosition){
		this.responsiblePersonPosition = responsiblePersonPosition
	}

	String getResponsiblePersonDateOfBirth(){
		return responsibleDateOfBirth
	}

	void setResponsiblePersonDateOfBirth(String responsibleDateOfBirth){
		this.responsibleDateOfBirth = responsibleDateOfBirth
	}

	String getResponsiblePersonAddress(){
		return responsibleAddress
	}

	void setResponsibleAddress(String responsibleAddress){
		this.responsibleAddress = responsibleAddress
	}

	String getResponsibleCitizenship(){
		return responsibleCitizenship
	}

	void setResponsibleCitizenship(String responsibleCitizenship){
		this.responsibleCitizenship = responsibleCitizenship
	}

	String getResponsibleOtherCitizenship(){
		return responsibleOtherCitizenship
	}

	void setResponsibleOtherCitizenship(String responsibleOtherCitizenship){
		this.responsibleOtherCitizenship = responsibleOtherCitizenship
	}
}
