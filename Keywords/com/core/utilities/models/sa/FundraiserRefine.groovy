package com.core.utilities.models.sa

public class FundraiserRefine {
	private String fundraiserStatus = "active"
	private String fromDate
	private String toDate
	private String fundraiserName

	String getfundraiserStatus(){
		return fundraiserStatus
	}

	void setfundraiserStatus(String fundraiserStatus) {
		this.fundraiserStatus = fundraiserStatus
	}

	String getFromDate(){
		return fromDate
	}

	void setfromDate(String fromDate) {
		this.fromDate = fromDate
	}

	String getToDate(){
		return toDate
	}

	void settoDate(String toDate) {
		this.toDate = toDate
	}

	String getfundraiserName(){
		return fundraiserName
	}

	void setfundraiserName(String fundraiserName){
		this.fundraiserName = fundraiserName
	}
}
