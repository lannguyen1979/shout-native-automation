package com.core.utilities.models.sa

import internal.GlobalVariable

public class UserRefine {
	private String userStatus = "active"
	private UserRole userRole
	private String fromDate
	private String toDate


	String getuserStatus(){
		return userStatus
	}

	void setuserStatus(String userStatus){
		this.userStatus = userStatus
	}

	UserRole getuserRole(){
		return userRole
	}


	void setuserRole(UserRole userRole){
		this.userRole = userRole
	}

	String getfromDate(){
		return fromDate
	}

	void setfromDate(String fromDate) {
		this.fromDate = fromDate
	}

	String gettoDate(){
		return toDate
	}

	void settoDate(String toDate) {
		this.toDate = toDate
	}
}
