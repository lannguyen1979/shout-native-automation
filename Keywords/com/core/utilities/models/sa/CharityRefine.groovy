package com.core.utilities.models.sa

public class CharityRefine {
	private String chStatus = "active"
	private String reviewStatus
	private String fromDate
	private String toDate
	private String charityName

	String getchStatus(){
		return chStatus
	}

	void setchStatus(String chStatus) {
		this.chStatus = chStatus
	}

	String getreviewStatus(){
		return reviewStatus
	}

	void setreviewStatus(String reviewStatus) {
		this.reviewStatus = reviewStatus
	}

	String getfromDate(){
		return fromDate
	}

	void setfromDate(String fromDate) {
		this.fromDate = fromDate
	}

	String gettoDate(){
		return toDate
	}

	void settoDate(String toDate) {
		this.toDate = toDate
	}
}
