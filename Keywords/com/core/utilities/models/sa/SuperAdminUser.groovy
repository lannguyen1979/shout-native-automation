package com.core.utilities.models.sa

public class SuperAdminUser {
	private String username
	private String password
	private String otp = "111198"
	private String recaptchaResponse = "False"

	String getUsername() {
		return username
	}

	void setUsername(String username) {
		this.username = username
	}

	String getPassword() {
		return password
	}

	void setPassword(String password) {
		this.password = password
	}

	String getOpt() {
		return opt
	}

	void setOpt(String opt) {
		this.opt = opt
	}

	String getRecaptchaResponse() {
		return recaptchaResponse
	}

	void setRecaptchaResponse(String recaptchaResponse) {
		this.recaptchaResponse = recaptchaResponse
	}

	String getOtp() {
		return otp
	}

	void setOtp(String otp) {
		this.otp = otp
	}
}
