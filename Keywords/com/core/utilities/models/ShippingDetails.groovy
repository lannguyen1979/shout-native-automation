package com.core.utilities.models

import com.google.gson.annotations.SerializedName

class ShippingDetails {

    private String address = "123 Street"
    private String suburb
    private String state

    @SerializedName("postcode")
    String postCode

    String getAddress() {
        return address
    }

    void setAddress(String address) {
        this.address = address
    }

    String getSuburb() {
        return suburb
    }

    void setSuburb(String suburb) {
        this.suburb = suburb
    }

    String getState() {
        return state
    }

    void setState(String state) {
        this.state = state
    }

    String getPostCode() {
        return postCode
    }

    void setPostCode(String postCode) {
        this.postCode = postCode
    }
}
