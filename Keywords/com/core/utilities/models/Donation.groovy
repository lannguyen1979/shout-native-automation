package com.core.utilities.models

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

class Donation {

	@SerializedName("client_token")
	private String clientToken = "35a4fffa-7ee0-4084-83ef-b732b51156cd"

	@SerializedName("cause_id")
	private String causeId = "1776"

	@SerializedName("recaptcha_response")
	private String recaptchaResponse = "recaptcha_responsesdfsdf"

	private String total
	private DonationUser user
	private Shouter shouter = new Shouter()
	private DonationPayment payment

	@SerializedName("phone_number")
	private String phoneNumber

	@SerializedName("country_code")
	private String countryCode = "au"

	@SerializedName("cause_subscribe")
	private String causeSubscribe = "false"

	@SerializedName("merchandise_amount")
	private String merchandiseAmount = "0"

	@SerializedName("donation_amount")
	private String donationAmount

	@SerializedName("merchandise_fee")
	private String merchandiseFee = "0"

	@SerializedName("donation_fee")
	private String donationFee = "0.00"

	@SerializedName("shipping_fee")
	private String shippingFee = "0.00"

	@SerializedName("shipping_details")
	ShippingDetails shippingDetails = new ShippingDetails()

	Donation(){
	}

	String getClientToken() {
		return clientToken
	}

	void setClientToken(String clientToken) {
		this.clientToken = clientToken
	}

	String getCauseId() {
		return causeId
	}

	void setCauseId(String causeId) {
		this.causeId = causeId
	}

	String getRecaptchaResponse() {
		return recaptchaResponse
	}

	void setRecaptchaResponse(String recaptchaResponse) {
		this.recaptchaResponse = recaptchaResponse
	}

	String getTotal() {
		return total
	}

	void setTotal(String total) {
		this.total = total
	}

	DonationUser getUser() {
		return user
	}

	void setUser(DonationUser user) {
		this.user = user
	}

	Shouter getShouter() {
		return shouter
	}

	void setShouter(Shouter shouter) {
		this.shouter = shouter
	}

	DonationPayment getPayment() {
		return payment
	}

	void setPayment(DonationPayment payment) {
		this.payment = payment
	}

	String getPhoneNumber() {
		return phoneNumber
	}

	void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber
	}

	String getCountryCode() {
		return countryCode
	}

	void setCountryCode(String countryCode) {
		this.countryCode = countryCode
	}

	String getCauseSubscribe() {
		return causeSubscribe
	}

	void setCauseSubscribe(String causeSubscribe) {
		this.causeSubscribe = causeSubscribe
	}

	String getMerchandiseAmount() {
		return merchandiseAmount
	}

	void setMerchandiseAmount(String merchandiseAmount) {
		this.merchandiseAmount = merchandiseAmount
	}

	String getDonationAmount() {
		return donationAmount
	}

	void setDonationAmount(String donationAmount) {
		this.donationAmount = donationAmount
	}

	String getMerchandiseFee() {
		return merchandiseFee
	}

	void setMerchandiseFee(String merchandiseFee) {
		this.merchandiseFee = merchandiseFee
	}

	String getDonationFee() {
		return donationFee
	}

	void setDonationFee(String donationFee) {
		this.donationFee = donationFee
	}

	String getShippingFee() {
		return shippingFee
	}

	void setShippingFee(String shippingFee) {
		this.shippingFee = shippingFee
	}

	ShippingDetails getShippingDetails() {
		return shippingDetails
	}

	void setShippingDetails(ShippingDetails shippingDetails) {
		this.shippingDetails = shippingDetails
	}

	String toString(Object model){
		return new Gson().toJson(model).toString()
	}

	static void main(String[] args) {
		Donation donation = new Donation()
		//		println "donation.toString() = ${donation.toString()}"
		println "donation.toString() = ${new Gson().toJson(donation).toString()}"
	}
}
