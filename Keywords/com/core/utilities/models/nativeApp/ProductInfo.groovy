package com.core.utilities.models.nativeApp

import com.core.utilities.models.FeesSetting
import com.kms.katalon.core.testobject.*

import groovy.json.JsonSlurper

public class ProductInfo {

	private String id
	private String name
	private String price
	private String icon
	
	String getid() {
		return id
	}

	void setid(String id) {
		this.id = id
	}
	
	String getPrice() {
		return price
	}

	void setPrice(String price) {
		this.price = price
	}
	
	String getName() {
		return name
	}

	void setName(String name) {
		this.name = name
	}
	
	String getIcon() {
		return icon
	}

	void setIcon(String icon) {
		this.icon = icon
	}



}
