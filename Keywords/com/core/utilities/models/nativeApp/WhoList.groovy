package com.core.utilities.models.nativeApp

class WhoList {
	private String id
	private String name
	private String type
	private String logo
	private boolean apple_pay_enabled
	private boolean feeOnTopDefault
	private boolean feeOnTopEnable

	String getId() {
		return id
	}

	void setId(String id) {
		this.id = id
	}

	String getName() {
		return name
	}

	void setName(String name) {
		this.name = name
	}

	String getType() {
		return type
	}

	void setType(String type) {
		this.type = type
	}

	String getLogo() {
		return logo
	}

	void setLogo(String logo) {
		this.logo = logo
	}

	boolean getApple_pay_enabled() {
		return apple_pay_enabled
	}

	void setApple_pay_enabled(boolean apple_pay_enabled) {
		this.apple_pay_enabled = apple_pay_enabled
	}

	boolean getFeeOnTopDefault() {
		return feeOnTopDefault
	}

	void setFeeOnTopDefault(boolean feeOnTopDefault) {
		this.feeOnTopDefault = feeOnTopDefault
	}

	boolean getFeeOnTopEnable() {
		return feeOnTopEnable
	}

	void setFeeOnTopEnable(boolean feeOnTopEnable) {
		this.feeOnTopEnable = feeOnTopEnable
	}
}
