package com.core.utilities.models.nativeApp

class NativeDonation {

	private String charity_id
	private String product_id
	private String recurring
	private String sub_total

	String getCharityId() {
		return charity_id
	}

	void setCharityId(String charity_id) {
		this.charity_id = charity_id
	}

	String getProductId() {
		return product_id
	}

	void setProductId(String product_id) {
		this.product_id = product_id
	}

	String getRecurring() {
		return recurring
	}

	void setRecurring(String recurring) {
		this.recurring = recurring
	}

	String getSubTotal() {
		return sub_total
	}

	void setSubTotal(String sub_total) {
		this.sub_total = sub_total
	}
}
