package com.core.utilities.models.nativeApp

class NavtiveAppUser {
	private String email
	private String password
	private String device_id = "12345678"

	String getEmail() {
		return email
	}

	void setEmail(String email) {
		this.email = email
	}

	String getPassword() {
		return password
	}

	void setPassword(String password) {
		this.password = password
	}

	String getDevice_id() {
		return device_id
	}

	void setDevice_id(String device_id) {
		this.device_id = device_id
	}
}
