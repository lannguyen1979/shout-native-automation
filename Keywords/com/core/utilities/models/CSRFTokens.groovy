package com.core.utilities.models

import gherkin.deps.com.google.gson.annotations.SerializedName

class CSRFTokens {
    @SerializedName("_csrf_token")
    private String csrfToken

    @SerializedName("_shout_web_session")
    private String shoutWebSession

    String getCsrfToken() {
        return csrfToken
    }

    void setCsrfToken(String csrfToken) {
        this.csrfToken = csrfToken
    }

    String getShoutWebSession() {
        return shoutWebSession
    }

    void setShoutWebSession(String shoutWebSession) {
        this.shoutWebSession = shoutWebSession
    }
}
