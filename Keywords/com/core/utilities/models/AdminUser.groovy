package com.core.utilities.models

import internal.GlobalVariable

class AdminUser {
	private String username
	private String password
	private String recaptchaResponse = "false"
	private String otp = GlobalVariable.G_otpNumber
	private String phoneNumber = "+61491570156"

	String getPhoneNumber() {
		return phoneNumber
	}

	void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber
	}

	String getRecaptcha_response() {
		return recaptchaResponse
	}

	void setRecaptcha_response(String recaptcha_response) {
		this.recaptchaResponse = recaptcha_response
	}

	String getOtp() {
		return otp
	}

	void setOtp(String otp) {
		this.otp = otp
	}

	String getUsername() {
		return username
	}

	void setUsername(String username) {
		this.username = username
	}

	String getPassword() {
		return password
	}

	void setPassword(String password) {
		this.password = password
	}
}
