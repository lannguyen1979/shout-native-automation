package com.core.utilities.models.donation

class Merchandise {
	private String id
	private String amount = "1"
	private String quantity = "1"

	String getId() {
		return id
	}

	void setId(String id) {
		this.id = id
	}

	String getAmount() {
		return amount
	}

	void setAmount(String amount) {
		this.amount = amount
	}

	String getQuantity() {
		return quantity
	}

	void setQuantity(String quantity) {
		this.quantity = quantity
	}
}
