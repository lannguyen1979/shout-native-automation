package com.core.utilities.models.donation


class OpenDonationForm {

	private String charityName

	private String charityId

	private String name

	private String token

			/*	private List<MerchandisesJs> merchandises

	List<MerchandisesJs> getMerchandises() {
		return merchandises
	}*/

			/*	void setMerchandises(List<MerchandisesJs> merchandises) {
		this.merchandises = merchandises
	}
*/
	String getCharityName() {
		return charityName
	}

	void setCharityName(String charityName) {
		this.charityName = charityName
	}

	String getCharityId() {
		return charityId
	}

	void setCharityId(String charityId) {
		this.charityId = charityId
	}

	String getName() {
		return name
	}

	void setName(String name) {
		this.name = name
	}

	String getToken() {
		return token
	}

	void setToken(String token) {
		this.token = token
	}

}
