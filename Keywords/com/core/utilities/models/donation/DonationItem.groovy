package com.core.utilities.models.donation

class DonationItem {

    private String id
    private String impact
    private String price

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    String getImpact() {
        return impact
    }

    void setImpact(String impact) {
        this.impact = impact
    }

    String getPrice() {
        return price
    }

    void setPrice(String price) {
        this.price = price
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DonationItem{")
        sb.append("id='").append(id).append('\'')
        sb.append(", impact='").append(impact).append('\'')
        sb.append(", price='").append(price).append('\'')
        sb.append('}')
        return sb.toString()
    }
}
