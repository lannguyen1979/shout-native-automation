package com.core.utilities.models.donation

import com.kms.katalon.core.testobject.FormDataBodyParameter
import gherkin.deps.com.google.gson.annotations.SerializedName

class ShoutButtonDonation {
	@SerializedName("cause_id")
	private String causeId

	@SerializedName("event_id")
	private String eventId

	@SerializedName("recaptcha_response")
	private String recaptchaResponse = "false"


	@SerializedName("authenticity_token")
	private String authenticityToken

	@SerializedName("client_token")
	private String clientToken
	private String total = "30.0"

	@SerializedName("user[email]")
	private String email = "huele@kms-technology.com"

	@SerializedName("shouter[country]")
	private String country = "AU"

	@SerializedName("shouter[first_name]")
	private String firstName = "Smoke"

	@SerializedName("shouter[last_name]")
	private String lastName = "Test"

	@SerializedName("payment[include_fee]")
	private String paymentFee = "false"

	@SerializedName("payment[type]")
	private String paymentType = "credit_card"

	@SerializedName("payment[cc][card_number]")
	private String cardNumber

	@SerializedName("payment[cc][expiry_date]")
	private String expiryDate

	@SerializedName("payment[cc][ccv]")
	private String ccv

	@SerializedName("donation_amount")
	private String donationAmount = "10"

	private List<Merchandise> merchandise

	@SerializedName("merchandise_amount")
	private String merchandiseAmount = "20"

	@SerializedName("shipping_fee")
	private String shippingFee = "0"

	@SerializedName("shipping_details[address]")
	private String shippingAddress = "283/55 Collins NV"

	@SerializedName("shipping_details[suburb]")
	private String shippingSuburb = "Melbourne"

	@SerializedName("shipping_details[state]")
	private String shippingState = "VIC"

	@SerializedName("shipping_details[postcode]")
	private String shippingPostcode = "3000"

	String getRecaptchaResponse() {
		return recaptchaResponse
	}

	void setRecaptchaResponse(String recaptchaResponse) {
		this.recaptchaResponse = recaptchaResponse
	}

	String getCauseId() {
		return causeId
	}

	void setCauseId(String causeId) {
		this.causeId = causeId
	}

	String getEventId() {
		return eventId
	}

	void setEventId(String eventId) {
		this.eventId = eventId
	}

	String getAuthenticityToken() {
		return authenticityToken
	}

	void setAuthenticityToken(String authenticityToken) {
		this.authenticityToken = authenticityToken
	}

	String getClientToken() {
		return clientToken
	}

	void setClientToken(String clientToken) {
		this.clientToken = clientToken
	}

	String getEmail() {
		return email
	}

	void setEmail(String email) {
		this.email = email
	}

	String getCountry() {
		return country
	}

	void setCountry(String country) {
		this.country = country
	}

	String getFirstName() {
		return firstName
	}

	void setFirstName(String firstName) {
		this.firstName = firstName
	}

	String getLastName() {
		return lastName
	}

	void setLastName(String lastName) {
		this.lastName = lastName
	}

	String getPaymentFee() {
		return paymentFee
	}

	void setPaymentFee(String paymentFee) {
		this.paymentFee = paymentFee
	}

	String getPaymentType() {
		return paymentType
	}

	void setPaymentType(String paymentType) {
		this.paymentType = paymentType
	}

	String getCardNumber() {
		return cardNumber
	}

	void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber
	}

	String getExpiryDate() {
		return expiryDate
	}

	void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate
	}

	String getCcv() {
		return ccv
	}

	void setCcv(String ccv) {
		this.ccv = ccv
	}

	String getDonationAmount() {
		return donationAmount
	}

	void setDonationAmount(String donationAmount) {
		this.donationAmount = donationAmount
	}

	List<Merchandise> getMerchandise() {
		return merchandise
	}

	void setMerchandise(List<Merchandise> merchandise) {
		this.merchandise = merchandise
	}

	String getMerchandiseAmount() {
		return merchandiseAmount
	}

	void setMerchandiseAmount(String merchandiseAmount) {
		this.merchandiseAmount = merchandiseAmount
	}

	String getShippingFee() {
		return shippingFee
	}

	void setShippingFee(String shippingFee) {
		this.shippingFee = shippingFee
	}

	String getShippingAddress() {
		return shippingAddress
	}

	void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress
	}

	String getShippingSuburb() {
		return shippingSuburb
	}

	void setShippingSuburb(String shippingSuburb) {
		this.shippingSuburb = shippingSuburb
	}

	String getShippingState() {
		return shippingState
	}

	void setShippingState(String shippingState) {
		this.shippingState = shippingState
	}

	String getShippingPostcode() {
		return shippingPostcode
	}

	void setShippingPostcode(String shippingPostcode) {
		this.shippingPostcode = shippingPostcode
	}

	void setCauseId(FormDataBodyParameter causeId) {
		this.causeId = causeId
	}

	String getTotal() {
		return total
	}

	void setTotal(String total) {
		this.total = total
	}
}
