package com.core.utilities.models.donation

import com.core.utilities.models.CreditCard

class DonationPaymentMethod {

    private String paymentMethod
    private CreditCard creditCard = null

    String getPaymentMethod() {
        return paymentMethod
    }

    void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod
    }

    CreditCard getCreditCard() {
        return creditCard
    }

    void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard
    }

}
