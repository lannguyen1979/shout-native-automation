package com.core.utilities.models

class Donors {
	private String id
	private String userId
	private String firstName
	private String lastName
	private String email
	private String type
	private String status

	String getId() {
		return id
	}

	void setId(String id) {
		this.id = id
	}

	String getUserId() {
		return userId
	}

	void setUserId(String userId) {
		this.userId = userId
	}

	String getFirstName() {
		return firstName
	}

	void setFirstName(String firstName) {
		this.firstName = firstName
	}

	String getLastName() {
		return lastName
	}

	void setLastName(String lastName) {
		this.lastName = lastName
	}

	String getEmail() {
		return email
	}

	void setEmail(String email) {
		this.email = email
	}

	String getType() {
		return type
	}

	void setType(String type) {
		this.type = type
	}

	String getStatus() {
		return status
	}

	void setStatus(String status) {
		this.status = status
	}


	@Override
	String toString() {
		final StringBuilder sb = new StringBuilder("{")
		sb.append("id='").append(id).append('\'')
		sb.append(", userId='").append(userId).append('\'')
		sb.append(", firstName='").append(firstName).append('\'')
		sb.append(", lastName='").append(lastName).append('\'')
		sb.append(", email='").append(email).append('\'')
		sb.append(", type='").append(type).append('\'')
		sb.append(", status='").append(status).append('\'')
		sb.append('}')
		return sb.toString()
	}
}
