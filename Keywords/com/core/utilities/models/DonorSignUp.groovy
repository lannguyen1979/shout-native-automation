package com.core.utilities.models

class DonorSignUp {
	private String recaptchaResponse = "03AOLTBLR_I8tRu_xqty4ty7Fl_AS61skHuEH1VZa7CE3TP6i-UwrP4T0fYOuOXrdxuFdkDPS70_cb3Zpm4AeZxGcS3KqjTgyqvI6dnOnILzjBtsTWNYrNoj56c7Ud6smyL2eJvss52FOE36GwWGjGbTNQZD7ha0Q366RRKubi2wNC99JTwnSp6_k_YX8__94U5wAtpijVqzaw_JJf-YRJOHIzvTlT3moi-g6Xx7YpEmRYfjhnV53Cc0GeCDBx87uybuhl9jqRLRIEjsbuORwQSHU0wDtFH4oq9VWzzXIS5zZKadVB7emgz0bPP3o_WVNxgTDyWgLR3CpsUESGJ4DkINJJR0hOvOk12ezSazvqutb69sHqofhTy2QA7ENEJZh1yN4p7TmW8myEvTtntaqeCt9aURYgscuzsA"
	private String formId = "COMPONENT_SIGN_UP"
	private DonorUserInfo user

	String getRecaptchaResponse() {
		return recaptchaResponse
	}

	void setRecaptchaResponse(String recaptchaResponse) {
		this.recaptchaResponse = recaptchaResponse
	}

	String getFormId() {
		return formId
	}

	void setFormId(String formId) {
		this.formId = formId
	}

	DonorUserInfo getUserInfo() {
		return user
	}

	void setUserInfo(DonorUserInfo userInfo) {
		this.user = userInfo
	}
}
