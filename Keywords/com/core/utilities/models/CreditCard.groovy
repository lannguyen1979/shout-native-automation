package com.core.utilities.models

import com.google.gson.annotations.SerializedName

class CreditCard {

	@SerializedName("card_number")
	String cardNumber

	@SerializedName("expiry_date")
	String expiryDate
	String ccv

	/*cardType is more detail if paymentMethod is PAYMENT_TYPE_CREDITCARD*/
	String cardType

	String getCardType() {
		return cardType
	}

	void setCardType(String cardType) {
		this.cardType = cardType
	}

	String getCardNumber() {
		return cardNumber
	}

	void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber
	}

	String getExpiryDate() {
		return expiryDate
	}

	void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate
	}

	String getCcv() {
		return ccv
	}

	void setCcv(String ccv) {
		this.ccv = ccv
	}
}
