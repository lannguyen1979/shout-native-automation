package com.core.utilities.models

import com.google.gson.JsonElement

class ServiceResponse {
    String id
    String status
    JsonElement data

    String getId() {
        return id
    }

    void setId(String id) {
        this.id = id
    }

    String getStatus() {
        return status
    }

    void setStatus(String status) {
        this.status = status
    }

    JsonElement getData() {
        return data
    }

    void setData(JsonElement data) {
        this.data = data
    }
}
