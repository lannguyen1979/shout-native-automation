package com.core.utilities.models.uploadFile

class BankStatementFiles {

	private String fileName
	private String filePath
	private String fileId
	private String snapshotFileId

	String getFileName() {
		return fileName
	}

	void setFileName(String fileName) {
		this.fileName = fileName
	}

	String getFilePath() {
		return filePath
	}

	void setFilePath(String filePath) {
		this.filePath = filePath
	}

	String getFileId() {
		return fileId
	}

	void setFileId(String fileId) {
		this.fileId = fileId
	}

	String getSnapshotFileId() {
		return snapshotFileId
	}

	void setSnapshotFileId(String snapshotFileId) {
		this.snapshotFileId = snapshotFileId
	}
}
