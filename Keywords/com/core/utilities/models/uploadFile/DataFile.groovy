package com.core.utilities.models.uploadFile

class DataFile {

	private String fileName
	private String filePath
	private String fileId

	String getFileName() {
		return fileName
	}

	void setFileName(String fileName) {
		this.fileName = fileName
	}

	String getFilePath() {
		return filePath
	}

	void setFilePath(String filePath) {
		this.filePath = filePath
	}

	String getFileId() {
		return fileId
	}

	void setFileId(String fileId) {
		this.fileId = fileId
	}
}
