package com.core.utilities.models.uploadFile

class PublicProfile {

	private String statusStep = "done"
	private String charityId
	private String logoUrl
	private String logoId
	private String charitySlug
	private String isCharitySlugExisting = "success"
	private String backgroundImageId
	private String backgroundImageUrl
	private String shortDescription = "Charity Overview"

	String getStatusStep() {
		return statusStep
	}

	void setStatusStep(String statusStep) {
		this.statusStep = statusStep
	}

	String getCharityId() {
		return charityId
	}

	void setCharityId(String charityId) {
		this.charityId = charityId
	}

	String getLogoUrl() {
		return logoUrl
	}

	void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl
	}

	String getLogoId() {
		return logoId
	}

	void setLogoId(String logoId) {
		this.logoId = logoId
	}

	String getCharitySlug() {
		return charitySlug
	}

	void setCharitySlug(String charitySlug) {
		this.charitySlug = charitySlug
	}

	String getIsCharitySlugExisting() {
		return isCharitySlugExisting
	}

	void setIsCharitySlugExisting(String isCharitySlugExisting) {
		this.isCharitySlugExisting = isCharitySlugExisting
	}

	String getBackgroundImageId() {
		return backgroundImageId
	}

	void setBackgroundImageId(String backgroundImageId) {
		this.backgroundImageId = backgroundImageId
	}

	String getBackgroundImageUrl() {
		return backgroundImageUrl
	}

	void setBackgroundImageUrl(String backgroundImageUrl) {
		this.backgroundImageUrl = backgroundImageUrl
	}

	String getShortDescription() {
		return shortDescription
	}

	void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription
	}
}
