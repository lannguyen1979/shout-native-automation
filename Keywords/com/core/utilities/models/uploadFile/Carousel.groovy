package com.core.utilities.models.uploadFile

class Carousel {

    private String fieldId = "photoField.carousel.1"
    private String fileId
    private String idAction = "photoField"
    /*
     * @index = '1' photo @index = '2' video
     * fieldId = "photoField.carousel.1" photo fieldId = 'videoField.carousel.2'
     * idAction = "photoField" idAction = "videoField"
      * */
    private String index = '1'
    private String linkPhoto

    String getFieldId() {
        return fieldId
    }

    void setFieldId(String fieldId) {
        this.fieldId = fieldId
    }

    String getFileId() {
        return fileId
    }

    void setFileId(String fileId) {
        this.fileId = fileId
    }

    String getIdAction() {
        return idAction
    }

    void setIdAction(String idAction) {
        this.idAction = idAction
    }

    String getIndex() {
        return index
    }

    void setIndex(String index) {
        this.index = index
    }

    String getLinkPhoto() {
        return linkPhoto
    }

    void setLinkPhoto(String linkPhoto) {
        this.linkPhoto = linkPhoto
    }

}
