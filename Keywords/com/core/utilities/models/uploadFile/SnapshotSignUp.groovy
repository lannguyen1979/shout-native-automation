package com.core.utilities.models.uploadFile

class SnapshotSignUp<T> {

    private String step = "financial"
    private T value

    String getStep() {
        return step
    }

    void setStep(String step) {
        this.step = step
    }

    T getValue() {
        return value
    }

    void setValue(T value) {
        this.value = value
    }

    SnapshotSignUp(String step, T value) {
        this.step = step
        this.value = value
    }
}
