package com.core.utilities.models.uploadFile

class DGRStatus {

	private String statusStep = "done"
	private String hasDgrFile = true
	private List<BankStatementFiles> dgrFiles

	String getStatusStep() {
		return statusStep
	}

	void setStatusStep(String statusStep) {
		this.statusStep = statusStep
	}

	String getHasDgrFile() {
		return hasDgrFile
	}

	void setHasDgrFile(String hasDgrFile) {
		this.hasDgrFile = hasDgrFile
	}

	List<BankStatementFiles> getDgrFiles() {
		return dgrFiles
	}

	void setDgrFiles(List<BankStatementFiles> dgrFiles) {
		this.dgrFiles = dgrFiles
	}
}
