package com.core.utilities.models.uploadFile

class PublicPage {

	private String statusStep = "done"
	private String longDescription = "Detailed Description"
	private List<Categories> categories = ["1"]
	private List<Communities> communities = ["1"]
	private List<SocialMedia> socialMedia
	private List<Carousel> carousel

	String getStatusStep() {
		return statusStep
	}

	void setStatusStep(String statusStep) {
		this.statusStep = statusStep
	}

	String getLongDescription() {
		return longDescription
	}

	void setLongDescription(String longDescription) {
		this.longDescription = longDescription
	}

	List<Categories> getCategories() {
		return categories
	}

	void setCategories(List<Categories> categories) {
		this.categories = categories
	}

	List<Communities> getCommunities() {
		return communities
	}

	void setCommunities(List<Communities> communities) {
		this.communities = communities
	}

	List<SocialMedia> getSocialMedia() {
		return socialMedia
	}

	void setSocialMedia(List<SocialMedia> socialMedia) {
		this.socialMedia = socialMedia
	}

	List<DataFile> getCarousel() {
		return carousel
	}

	void setCarousel(List<DataFile> carousel) {
		this.carousel = carousel
	}
}
