package com.core.utilities.models.uploadFile

class SnapshotOnBoarding<T> {

    private String step
    private String activeSave = "false"
    private T value

    String getStep() {
        return step
    }

    void setStep(String step) {
        this.step = step
    }

    T getValue() {
        return value
    }

    String getActiveSave() {
        return activeSave
    }

    void setActiveSave(String activeSave) {
        this.activeSave = activeSave
    }

    void setValue(T value) {
        this.value = value
    }

    SnapshotOnBoarding(String step, T value) {
        this.step = step
        this.value = value
    }
}
