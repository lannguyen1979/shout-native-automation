package com.core.utilities.models.uploadFile

import com.core.utilities.models.charity.ContactPerson
import com.core.utilities.models.charity.Document

class FinancialDetail {

	private String statusStep = "done"
	private String accountName = "Test Account Name"
	private String accountNumber = "11111111"
	private String bankName = "AMP Bank Ltd"
	private String bsb = "123456"
	private String mainFinancialContactEmail = "Auto1043435349825200@kms-technology.com"
	private String mainFinancialContactFullName = "FirstN-QA LastN"
	private String mainFinancialContactMainContactCheckbox = true
	private String mainFinancialContactPhoneNumber = "+61 0499 830 224"
	private List<String> financialContacts
	private List<BankStatementFiles> bankStatementFiles
	private List<Document> documents

	List<Document> getDocuments() {
		return documents
	}

	void setDocuments(List<Document> documents) {
		this.documents = documents
	}

	String getStatusStep() {
		return statusStep
	}

	void setStatusStep(String statusStep) {
		this.statusStep = statusStep
	}

	String getAccountName() {
		return accountName
	}

	void setAccountName(String accountName) {
		this.accountName = accountName
	}

	String getAccountNumber() {
		return accountNumber
	}

	void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber
	}

	String getBankName() {
		return bankName
	}

	void setBankName(String bankName) {
		this.bankName = bankName
	}

	String getBsb() {
		return bsb
	}

	void setBsb(String bsb) {
		this.bsb = bsb
	}

	String getMainFinancialContactEmail() {
		return mainFinancialContactEmail
	}

	void setMainFinancialContactEmail(String mainFinancialContactEmail) {
		this.mainFinancialContactEmail = mainFinancialContactEmail
	}

	String getMainFinancialContactFullName() {
		return mainFinancialContactFullName
	}

	void setMainFinancialContactFullName(String mainFinancialContactFullName) {
		this.mainFinancialContactFullName = mainFinancialContactFullName
	}

	String getMainFinancialContactMainContactCheckbox() {
		return mainFinancialContactMainContactCheckbox
	}

	void setMainFinancialContactMainContactCheckbox(String mainFinancialContactMainContactCheckbox) {
		this.mainFinancialContactMainContactCheckbox = mainFinancialContactMainContactCheckbox
	}

	String getMainFinancialContactPhoneNumber() {
		return mainFinancialContactPhoneNumber
	}

	void setMainFinancialContactPhoneNumber(String mainFinancialContactPhoneNumber) {
		this.mainFinancialContactPhoneNumber = mainFinancialContactPhoneNumber
	}

	List<ContactPerson> getFinancialContacts() {
		return financialContacts
	}

	void setFinancialContacts(List<String> financialContacts) {
		this.financialContacts = financialContacts
	}

	List<BankStatementFiles> getBankStatementFiles() {
		return bankStatementFiles
	}

	void setBankStatementFiles(List<BankStatementFiles> bankStatementFiles) {
		this.bankStatementFiles = bankStatementFiles
	}
}
