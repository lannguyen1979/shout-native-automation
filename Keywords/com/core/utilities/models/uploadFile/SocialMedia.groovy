package com.core.utilities.models.uploadFile

class SocialMedia {

    private String facebookUrl = ""
    private String instagramUrl = ""
    private String linkedInUrl = ""
    private String publicEmail = ""
    private String twitterUrl = ""
    private String websiteUrl = ""
    private String youtubeUrl = ""

    String getFacebookUrl() {
        return facebookUrl
    }

    void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl
    }

    String getInstagramUrl() {
        return instagramUrl
    }

    void setInstagramUrl(String instagramUrl) {
        this.instagramUrl = instagramUrl
    }

    String getLinkedInUrl() {
        return linkedInUrl
    }

    void setLinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl
    }

    String getPublicEmail() {
        return publicEmail
    }

    void setPublicEmail(String publicEmail) {
        this.publicEmail = publicEmail
    }

    String getTwitterUrl() {
        return twitterUrl
    }

    void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl
    }

    String getWebsiteUrl() {
        return websiteUrl
    }

    void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl
    }

    String getYoutubeUrl() {
        return youtubeUrl
    }

    void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl
    }

}
