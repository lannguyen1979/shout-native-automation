package com.core.utilities.webservice

import static com.core.utilities.common.Constants.*

import com.core.utilities.common.DateTimeUtils
import com.core.utilities.models.CSRFTokens
import com.core.utilities.models.sa.CharityRefine
import com.google.gson.Gson
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil

import groovy.json.JsonSlurper
import internal.GlobalVariable

public class CharityAPIRefineTesting {
	BaseAPITesting api = new BaseAPITesting()
	private JsonSlurper slurper = new JsonSlurper()
	private Gson gson = new Gson()
	DateTimeUtils datetime = new DateTimeUtils()

	String countActiveCharitybyCHStatus(String CharityListingAPIPath = ADMIN_CHARITIES_LISTING_PATH, CharityRefine conditions) {

		String filterString = "&status=" + conditions.getchStatus() + "&from="+ conditions.getfromDate() + "&to=" + conditions.gettoDate()
		CSRFTokens csrfTokens = api.getUserAuthorization(GlobalVariable.G_saUserName, GlobalVariable.G_loginPassword)
		def urldefault = GlobalVariable.G_baseAPIUrl + CharityListingAPIPath + filterString
		ResponseObject responseListdefault = api.sendGetRequestWithCSRF(urldefault, csrfTokens.getCsrfToken(),csrfTokens.getShoutWebSession())
		String totalActiveCharities =  slurper.parseText(responseListdefault.getResponseBodyContent()).total
		KeywordUtil.logInfo("total cs isssssssss: " + totalActiveCharities)
		return totalActiveCharities
	}
}
