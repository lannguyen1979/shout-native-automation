package com.core.utilities.webservice

import com.core.utilities.models.DonorUserInfo
import com.core.utilities.models.nativeApp.NativeDonation
import com.core.utilities.models.nativeApp.NavtiveAppUser
import com.core.utilities.models.nativeApp.ProductInfo
import com.core.utilities.models.nativeApp.WhoList
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.util.KeywordUtil

import groovy.json.JsonSlurper
import internal.GlobalVariable
import org.apache.commons.codec.binary.Base64
import org.testng.Assert
import static com.core.utilities.common.Constants.*

class NativeAppTesting {

	private def DEVICE_ID = "12345678"

	private BaseAPITesting api = new BaseAPITesting()
	private Gson gson = new Gson()
	private JsonSlurper slurper = new JsonSlurper()
	private ProductInfo product = new ProductInfo()
	private def NATIVE_APP_USER_SESSION = "/native/v1/user_sessions"

	String login(String email, String password) {
		NavtiveAppUser user = new NavtiveAppUser()
		user.setEmail(email)
		user.setPassword(password)
		def body = gson.toJson(user)
		def url = GlobalVariable.G_baseAPIUrl + NATIVE_APP_USER_SESSION
		ResponseObject request = api.sendPostRequest(url, body)
		Assert.assertEquals(request.getStatusCode(),200, "$NATIVE_APP_USER_SESSION status code is NOT 200")
		return slurper.parseText(request.getResponseBodyContent()).token
	}

	String getCharityId() {
		ResponseObject charities = getWhoListResponse()
		Assert.assertEquals(charities.getStatusCode(), 200, "api $NATIVE_APP_CHARITY_LIST status code is NOT 200")
		return slurper.parseText(charities.getResponseBodyContent()).wholist[0].id
	}

	WhoList getAvailableCharity() {
		ResponseObject charities = getWhoListResponse()
		Assert.assertEquals(charities.getStatusCode(), 200, "api $NATIVE_APP_CHARITY_LIST status code is NOT 200")
		return gson.fromJson(gson.toJson(slurper.parseText(charities.getResponseBodyContent()).wholist[0]), WhoList)
	}

	private ResponseObject getWhoListResponse() {
		ResponseObject charities = api.sendGetRequest(GlobalVariable.G_baseAPIUrl + NATIVE_APP_CHARITY_LIST)
		charities
	}

	List<WhoList> getCharities(){
		JsonArray whoList = gson.fromJson(getWhoListResponse().getResponseBodyContent(), JsonObject.class).getAsJsonArray("wholist")
		return gson.fromJson(gson.toJson(whoList), WhoList[].class)
	}


	String getProductId(String charityId) {
		ResponseObject charities = api.sendGetRequest(GlobalVariable.G_baseAPIUrl + NATIVE_APP_CHARITY_DETAIL + charityId)
		Assert.assertEquals(charities.getStatusCode(), 200, "api $NATIVE_APP_CHARITY_DETAIL status code is NOT 200")
		return slurper.parseText(charities.getResponseBodyContent()).products[0].id
	}


	ProductInfo getProductInfo() {
		String charityId = getCharityId()
		def charityDetailPath = NATIVE_APP_CHARITY_DETAIL + charityId
		ResponseObject charities = api.sendGetRequest(GlobalVariable.G_baseAPIUrl + charityDetailPath)
		Assert.assertEquals(charities.getStatusCode(), 200, "api $charityDetailPath status code is NOT 200")
		return gson.fromJson(gson.toJson(slurper.parseText(charities.getResponseBodyContent()).products[0]), ProductInfo)
	}

	DonorUserInfo newDonor(){
		DonorUserInfo saDonor = new DonorUserInfo()
		saDonor.setEmail(GlobalVariable.G_shouterUsername)
		saDonor.setPassword(GlobalVariable.G_loginPassword)
		return saDonor
	}

	/*donor user used in frequency: monthly*/
	String getDonateUrl(DonorUserInfo donorUser = newDonor(), String frequency, String charityId, ProductInfo productInfo) {
		NativeDonation donation = new NativeDonation()
		donation.setCharityId(charityId)
		donation.setProductId(productInfo.getid())
		donation.setSubTotal(productInfo.getPrice())
		donation.setRecurring(frequency)
		ResponseObject donateResponse
		if (frequency == "monthly"){
			String token = login(donorUser.getEmail(), donorUser.getPassword())
			String authString = DEVICE_ID + ":" + token
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes())
			String authStringEnc = "Basic " + new String(authEncBytes)
			ArrayList<TestObjectProperty> defaultHeaders = api.buildDefaultHeaders()
			defaultHeaders.add(api.newHeader('Content-Type', 'application/json'))
			defaultHeaders.add(api.newHeader('device_id', DEVICE_ID))
			defaultHeaders.add(api.newHeader('Authorization', authStringEnc))
			KeywordUtil.logInfo("Donate monthly")
			donateResponse = api.sendPostRequestWithCustomHeaders(defaultHeaders, GlobalVariable.G_baseAPIUrl + NATIVE_APP_DONATION, gson.toJson(donation))
		} else if (frequency == "one_time"){
			KeywordUtil.logInfo("Donate onetime")
			donateResponse = api.sendPostRequest(GlobalVariable.G_baseAPIUrl + NATIVE_APP_DONATION, gson.toJson(donation))
		}
		Assert.assertEquals(donateResponse.getStatusCode(), 200, "api $NATIVE_APP_DONATION status code is NOT 200")
		return slurper.parseText(donateResponse.getResponseBodyContent()).url
	}
}
