package com.core.utilities.webservice.charities

import com.baseTests.dataComparison.CompareUtils
import com.core.utilities.common.StringHelper
import com.core.utilities.models.charity.CharityDetailInfo
import com.core.utilities.models.charity.CustomDonationProduct
import com.core.utilities.models.charity.PaymentMethods
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.kms.katalon.core.testdata.TestDataFactory
import org.apache.commons.lang3.StringUtils

import static com.core.utilities.common.Constants.*

import com.core.utilities.models.CSRFTokens
import com.core.utilities.models.ServiceResponse
import com.core.utilities.models.charity.Charity
import com.core.utilities.models.charity.CharityUser
import com.core.utilities.models.charity.UpdateApplicantState
import com.core.utilities.models.charity.UpdateState
import com.core.utilities.models.uploadFile.BankStatementFiles
import com.core.utilities.models.uploadFile.DGRStatus
import com.core.utilities.models.uploadFile.DataFile
import com.core.utilities.models.uploadFile.FinancialDetail
import com.core.utilities.models.uploadFile.SnapshotSignUp
import com.core.utilities.webservice.BaseAPITesting
import com.google.gson.Gson
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import groovy.json.JsonSlurper
import internal.GlobalVariable
import org.testng.Assert

class CharityAPITesting {

	BaseAPITesting api = new BaseAPITesting()
	Gson gson = new Gson()
	JsonSlurper jsonSlurper = new JsonSlurper()

	CSRFTokens getCharityAuthorization(String charityUsername, String charityPassword) {
		'Login user'
		CharityUser charityUser = new CharityUser()
		charityUser.setUsername(charityUsername)
		charityUser.setPassword(charityPassword)
		ResponseObject login = api.sendPOSTRequestWithCSRF(GlobalVariable.G_baseAPIUrl + AUTHEN_TOKEN_PATH, new Gson().toJson(charityUser))
		KeywordUtil.logInfo("$AUTHEN_TOKEN_PATH status code: ${login.getStatusCode()}")
		WebUiBuiltInKeywords.verifyEqual(new JsonSlurper().parseText(login.getResponseBodyContent()).status, "success")
		'Get cookies and headers from login'
		return api.getSetCookies(login.getHeaderFields())
	}

	String getCharityIdAfterLoggedIn(String charityUsername, String charityPassword){
		CSRFTokens csrfTokens = getCharityAuthorization(charityUsername, charityPassword)
		ResponseObject responseObject = api.sendGetRequestWithCSRF(GlobalVariable.G_baseAPIUrl + CHARITY_DASHBOARD_PATH, csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())
		KeywordUtil.logInfo(responseObject.getResponseBodyContent().toString())
		WebUiBuiltInKeywords.verifyEqual(responseObject.getStatusCode(), "200")
		def data = jsonSlurper.parseText(responseObject.getResponseBodyContent()).data
		return data.id
	}

	CharityDetailInfo getCharityInfo(String saUserName, String password, String charityId) {
		CSRFTokens csrfTokens = getCharityAuthorization(saUserName, password)
		ResponseObject responseObject = api.sendGetRequestWithCSRF(GlobalVariable.G_baseAPIUrl + ADMIN_CHARITY_DETAIL_PATH + charityId + "?", csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())
		KeywordUtil.logInfo(responseObject.getResponseBodyContent().toString())
		return api.mapResponseToModel(responseObject, CharityDetailInfo.class)
	}

	String getCharitySlug(String saUserName, String password, String charityId){
		return getCharityInfo(saUserName, password, charityId).getSlug()
	}

	ResponseObject getSAExistingCharities(String saUser = GlobalVariable.G_saUserName) {
		CSRFTokens csrfTokens = api.getUserAuthorization(saUser, GlobalVariable.G_loginPassword)
		ResponseObject charityRes = api.sendGetRequestWithCSRF(GlobalVariable.G_baseAPIUrl + ADMIN_CHARITIES_FIVE_PAGE_PATH, csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())
		charityRes
	}

	Charity getCharityByReviewStatus(String reviewStatus, String charityStatus, String profileStatus = "incomplete", String firstNamePrfix, String lastNamePrefix) {
		ResponseObject charityRes = getSAExistingCharities()
		List<Charity> charities = api.mapResponseToModel(charityRes, Charity[].class)
		Charity charity = charities.find{
			it ->StringUtils.equalsAnyIgnoreCase(it.getStatus(), charityStatus) &&
					StringUtils.equalsAnyIgnoreCase(it.getReviewStatus(), reviewStatus) &&
					StringUtils.startsWithIgnoreCase(it.getName(), firstNamePrfix) &&
					StringUtils.endsWithIgnoreCase(it.getName(), lastNamePrefix) &&
					StringUtils.equalsIgnoreCase(it.getProfileStatus(), profileStatus)
		}
		if (Objects.nonNull(charity)){
			return charity
		} else {
			KeywordUtil.markFailed("Not found any charity matching reviewStatus: $reviewStatus and charityStatus: $charityStatus")
		}

	}

	Charity getCharityByOnlyReviewStatus() {
		ResponseObject charityRes = getSAExistingCharities()
		List<Charity> charities = api.mapResponseToModel(charityRes, Charity[].class)
		Charity charity = charities.find{
			it ->StringUtils.equalsAnyIgnoreCase(it.getReviewStatus(), "in_review")
		}
		if (Objects.nonNull(charity)){
			return charity
		} else {
			KeywordUtil.markFailed("Not found any charity matching reviewStatus: in_review, charityStatus: any")
		}
	}


	Charity getNewCharity(String prefixFirstName, String prefixLastName){
		return getCharityByReviewStatus("initial", "inactive", "incomplete", prefixFirstName, prefixLastName)
	}

	Charity getOnboardingCharity(String prefixFirstName, String prefixLastName){
		return getCharityByReviewStatus("initial", "inactive", "initial", prefixFirstName, prefixLastName)
	}

	Charity getCharityInfo(String charityName){
		ResponseObject charityRes = getSAExistingCharities()
		List<Charity> charities = api.mapResponseToModel(charityRes, Charity[].class)
		return charities.find{it ->it.getName() == charityName}
	}

	/*Existing charity /charities/list is consist of charityId and charityName only */
	ResponseObject getExistingCharities() {
		CSRFTokens csrfTokens = api.getUserAuthorization(GlobalVariable.G_saUserNameAPI, GlobalVariable.G_loginPassword)
		ResponseObject charityRes = api.sendGetRequestWithCSRF(GlobalVariable.G_baseAPIUrl + "/charities/list", csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())
		charityRes
	}

	Charity getActiveCharity(String saUser = GlobalVariable.G_saUserName) {
		List<Charity> activeCharities = getActiveCharities(saUser)
		return activeCharities.get(0)
	}

	List<Charity> getActiveCharities(String saUser) {
		List<Charity> charities = api.mapResponseToModel(getSAExistingCharities(saUser), Charity[].class)
		List<Charity> activeCharities = charities.findAll({ item -> item.getStatus() == "active" })
		KeywordUtil.logInfo("charity active = " + gson.toJson(activeCharities))
		activeCharities
	}

    private List<Charity> getPubliCharites() {
        ResponseObject charitiesRes = api.sendGetRequest(GlobalVariable.G_baseAPIUrl + "/charities/list")
        Assert.assertEquals(charitiesRes.getStatusCode(), 200)
        List<Charity> charities = api.mapResponseToModel(charitiesRes, Charity[].class)
		KeywordUtil.logInfo("charity active = " + gson.toJson(charities))
		charities
	}

    Charity getPublicCharity(){
        return getPubliCharites().get(0)
    }

	DataFile getUploadFileOnBoarding(String charityUsername, String charityPassword, String fileNameOnDataFiles, String type, String step) {
		CSRFTokens csrfTokens = getCharityAuthorization(charityUsername, charityPassword)
		ResponseObject uploadFile = api.sendFormData(
				GlobalVariable.G_baseAPIUrl + UPLOAD_FILE_PATH,
				csrfTokens.getCsrfToken(),
				csrfTokens.getShoutWebSession(),
				fileNameOnDataFiles,
				type, step)
		WebUiBuiltInKeywords.verifyEqual(uploadFile.getStatusCode(), "200")
		ServiceResponse sr = gson.fromJson(uploadFile.getResponseBodyContent(), ServiceResponse.class)
		return gson.fromJson(sr.getData(), DataFile.class)
	}

	public <T> T uploadFileSignUp(String userName, String password, String fileNameInDataFile, String type, String step, Class<T> dataFile) {
		def api = new BaseAPITesting()
		CSRFTokens charityAuthen = getCharityAuthorization(userName, password)
		'Send upload file'
		def csrf = charityAuthen.getCsrfToken()
		def shoutWebSession = charityAuthen.getShoutWebSession()
		KeywordUtil.logInfo("csrf: $csrf")
		KeywordUtil.logInfo("shoutWebSession: $shoutWebSession")
		ResponseObject uploadFile = api.sendFormData(GlobalVariable.G_baseAPIUrl + UPLOAD_FILE_PATH,
				csrf,
				shoutWebSession,
				fileNameInDataFile,
				type, step)
		Assert.assertEquals(uploadFile.statusCode, 200, "uploadFile for signUp status not 200.")
		return api.mapResponseToModel(uploadFile, dataFile)
	}

	void snapshotOnBoarding(String url, String charityUserName, String charityPassword, String jsonBody) {
		CSRFTokens csrfTokens = getCharityAuthorization(charityUserName, charityPassword)
		ResponseObject save = api.sendPATCHRequestWithCSRF(url, csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession(), jsonBody)
		KeywordUtil.logInfo("snapshot status = ${save.getResponseBodyContent()}")
		Assert.assertEquals(save.statusCode, 200, "snapshot on-boarding status not 200.")
	}

	void snapShotSignUpDGR(String charityUser, String charityPassword, DataFile dataFile) {
		CSRFTokens csrfTokens = getCharityAuthorization(charityUser, charityPassword)
		List<DataFile> dgrFiles1 = new ArrayList<>()
		dgrFiles1.add(dataFile)
		DGRStatus value = new DGRStatus()
		value.setDgrFiles(dgrFiles1)
		SnapshotSignUp<DGRStatus> dgrSnapShot = new SnapshotSignUp<>("dgr", value)
		ResponseObject saveDgrStatus = api.sendPATCHRequestWithCSRF(
				GlobalVariable.G_baseAPIUrl + SNAPSHOT_SIGN_UP_PATH,
				csrfTokens.getCsrfToken(),
				csrfTokens.getShoutWebSession(),
				gson.toJson(dgrSnapShot))
		Assert.assertEquals(saveDgrStatus.statusCode, 200, "snapshot sign-up DGR status not 200.")
		WebUiBuiltInKeywords.refresh()
	}

	FinancialDetail snapShotSignUpFinancial(String charityUser, String charityPassword, BankStatementFiles dataFile) {
		CSRFTokens csrfTokens = getCharityAuthorization(charityUser, charityPassword)
		List<BankStatementFiles> dataFiles = new ArrayList<>()
		dataFiles.add(dataFile)
		FinancialDetail value = new FinancialDetail()
		value.setBankStatementFiles(dataFiles)
		SnapshotSignUp<FinancialDetail> snapshotSignUp = new SnapshotSignUp<>("financial", value)
		ResponseObject saveStatus = api.sendPATCHRequestWithCSRF(
				GlobalVariable.G_baseAPIUrl + SNAPSHOT_SIGN_UP_PATH,
				csrfTokens.getCsrfToken(),
				csrfTokens.getShoutWebSession(),
				gson.toJson(snapshotSignUp))
		Assert.assertEquals(saveStatus.statusCode, 200, "snapshot sign-up Financial status not 200.")
		WebUiBuiltInKeywords.refresh()
		return value
	}

	void updateState(String saUser, String saPassword, String charityId, String nextState) {
		CSRFTokens csrfTokens = getCharityAuthorization(saUser, saPassword)
		UpdateState body = new UpdateState()
		body.setNextReviewState(nextState)
		ResponseObject res = api.sendPOSTRequestWithCSRF(
				GlobalVariable.G_baseAPIUrl + "/admin/charities/$charityId/update-state",
				gson.toJson(body),
				csrfTokens.getCsrfToken(),
				csrfTokens.getShoutWebSession())
		Assert.assertEquals(res.getResponseBodyContent(), "200")
		Assert.assertEquals(jsonSlurper.parseText(res.getResponseBodyContent()).data.status, nextState, "Change state to $nextState successfully!")
	}

	/**
	 * Precondition:
	 * 1. SA Charity Listing > Charity Overview:
	 * 2. Review Applicant: In Review and all other state: InComplete
	 * Change Review Applicant from In Review to InComplete
	 * **/
	void updateApplicantState(String saUser, String saPassword, String charityId, String nextState) {
		CSRFTokens csrfTokens = getCharityAuthorization(saUser, saPassword)
		UpdateApplicantState body = new UpdateApplicantState()
		body.setState(nextState)
		ResponseObject res = api.sendPATCHRequestWithCSRF(
				GlobalVariable.G_baseAPIUrl + "/admin/charities/$charityId/update-applicant-state",
				csrfTokens.getCsrfToken(),
				csrfTokens.getShoutWebSession(),
				gson.toJson(body))
		KeywordUtil.logInfo("updateApplicantState body = " + gson.toJson(body))
		KeywordUtil.logInfo("updateApplicantState status = " + res.getStatusCode())
		KeywordUtil.logInfo("updateApplicantState body = " + res.getResponseBodyContent())
		Assert.assertEquals(jsonSlurper.parseText(res.getResponseBodyContent()).data.status, nextState, "Change state to $nextState successfully!")
		WebUiBuiltInKeywords.delay(2)
	}

	List<CustomDonationProduct> getDefaultDonationProducts(String charityId){
        def getProductListPath = GET_PUBLIC_PRODUCTS_PATH + charityId
        ResponseObject rp = api.sendGetRequest(GlobalVariable.G_baseAPIUrl + getProductListPath)
		KeywordUtil.logInfo("$getProductListPath getResponseBodyContent: " + rp.getResponseBodyContent())
		Assert.assertEquals(rp.getStatusCode(), 200, "Status code is not 200")
		return api.mapResponseToModel(rp, CustomDonationProduct[].class)
	}

	Charity getCharityByName(String charityName){
		List<Charity> charities = api.mapResponseToModel(getSAExistingCharities(), Charity[].class)
		return charities.find{charity ->charity.getName() == charityName}
	}

	String createRandomCharityEmail() {
		String prefixFirstName = TestDataFactory.findTestData('SignUp/SignUp').getObjectValue('FirstName', SIGN_UP_INFOMATION)
		String randomUserEmail = StringHelper.randomString("s4g_" + prefixFirstName) + "@kms-technology.com"
		randomUserEmail
	}

	/*Payment methods of charity displaying when user to make donation*/

	PaymentMethods getDonationPaymentMethod(String charityId = activeCharityId, String source) {
		def charitySettingPath = "/charities/$charityId/settings?source=$source"
		ResponseObject charitySettingRes = api.sendGetRequest(GlobalVariable.G_baseAPIUrl + charitySettingPath)
		Assert.assertEquals(charitySettingRes.getStatusCode(), 200, "$charitySettingPath failed")
		KeywordUtil.logInfo("$charitySettingPath: " + charitySettingRes.getResponseBodyContent())

		JsonObject paymentMethod = api.mapJSONResponseToModel(charitySettingRes, JsonObject.class).get("data").getAsJsonObject().get("paymentMethod")
		PaymentMethods paymentMethods = gson.fromJson(paymentMethod, PaymentMethods.class)
		paymentMethods.setCharityId(charityId)
		paymentMethods.setSource(source)
		return paymentMethods
	}

	def activeCharityId = getActiveCharity(GlobalVariable.G_saUserNameAPI).getId()

	/*Payment methods of charity displaying in admin advanced charity setting*/

	PaymentMethods getSettingPaymentMethod(String charityId = activeCharityId, String source) {
		CSRFTokens csrfTokens = api.getUserAuthorization(GlobalVariable.G_saUserNameAPI, GlobalVariable.G_loginPassword)
		def advancedDetailPath = "/admin/charities/$charityId/advanced_details"
		ResponseObject advancedDetailRes = api.sendGetRequestWithCSRF(GlobalVariable.G_baseAPIUrl + advancedDetailPath, csrfTokens)
		Assert.assertEquals(advancedDetailRes.getStatusCode(), 200, "$advancedDetailPath failed")
		KeywordUtil.logInfo("$advancedDetailPath: " + advancedDetailRes.getResponseBodyContent())
		/*parsing response to PaymentMethod model*/
		JsonArray paymentMethods = api.mapJSONResponseToModel(advancedDetailRes, JsonObject.class).get("data").getAsJsonObject().get("paymentMethods")
		List<PaymentMethods> paymentMethodsList = gson.fromJson(paymentMethods, PaymentMethods[].class)

		PaymentMethods paymentMethod = paymentMethodsList.find({ it ->
			StringUtils.equalsIgnoreCase(it.getSource(), source) &&
					it.getCharityId() == charityId
		})
		if (paymentMethod.getSource() == "app" && paymentMethod.getActive() == false) {
			return paymentMethodsList.find({ it -> it.getSource() == "web" && it.getCharityId() == charityId })
		}
		return paymentMethod
	}

	void verifyPaymentMethodsOnWeb() {
		Assert.assertTrue(comparePaymentMethods("web"), "Web Payment methods on donation is not as settings")
	}

	void verifyPaymentMethodsOnApp(String charityId = activeCharityId) {
		Assert.assertTrue(comparePaymentMethods(charityId, "app"), "App Payment methods on donation is not as settings")
	}

	private boolean comparePaymentMethods(String charityId = activeCharityId, String source) {
		PaymentMethods settingPaymentMethod = getSettingPaymentMethod(charityId, source)
		PaymentMethods donationPaymentMethods = getDonationPaymentMethod(charityId, source)
		/*active properties on donation should be same with settings*/
		donationPaymentMethods.setActive(settingPaymentMethod.getActive())
		if (settingPaymentMethod.getSource() != source) {
			KeywordUtil.logInfo("Global app configure is true. Payment method app is coppied from web's")
			settingPaymentMethod.setSource(source)
		}
		KeywordUtil.logInfo("donationPaymentMethods: ${donationPaymentMethods.toString()}")
		KeywordUtil.logInfo("settingPaymentMethod  : ${settingPaymentMethod.toString()}")
		def equals = CompareUtils.modelEquals(donationPaymentMethods, settingPaymentMethod)
		equals
	}
}
