package com.core.utilities.webservice
import com.core.utilities.models.User
import org.apache.commons.lang3.StringUtils
import org.testng.Assert

import static com.core.utilities.common.Constants.AUTHEN_TOKEN_PATH
import static com.core.utilities.common.Constants.CSRF_PATH

import com.core.utilities.models.AdminUser
import com.core.utilities.models.CSRFTokens
import com.core.utilities.models.ServiceResponse
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.*
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import groovy.json.JsonSlurper
import internal.GlobalVariable
import io.restassured.RestAssured
import io.restassured.response.Response
import io.restassured.specification.RequestSpecification

class BaseAPITesting {
	JsonSlurper slurper = new JsonSlurper()
	Gson gson = new Gson()

	private static FailureHandling failureHandling = FailureHandling.STOP_ON_FAILURE
	private static WSBuiltInKeywords webService = new WSBuiltInKeywords()

	ResponseObject sendRequest(RequestObject request) {
		return webService.sendRequest(request, failureHandling)
	}

	RequestObject buildGETRequest(String url) {
		return new RestRequestObjectBuilder().withRestUrl(url).
				withRestRequestMethod("GET").
				withHttpHeaders(buildDefaultHeaders("Accept", 'application/json')).
				build()
	}

	RequestObject buildGETRequest(String url, String authToken) {
		ArrayList<TestObjectProperty> defaultHeaders = buildDefaultHeaders("Accept", 'application/json')
		defaultHeaders.add(newHeader("Authorization", 'Bearer ' + authToken))
		return new RestRequestObjectBuilder().withRestUrl(url).
				withRestRequestMethod("GET").
				withHttpHeaders(defaultHeaders).
				build()
	}

	RequestObject buildGETRequestWithCSRF(String url, String csrfToken, String webSessionToken) {
		return new RestRequestObjectBuilder().withRestUrl(url).
				withRestRequestMethod("GET").
				withHttpHeaders(buildDefaultHeadersWithCSRF(csrfToken, webSessionToken)).
				build()
	}

	RequestObject buildGETRequestWithCSRF(String url, CSRFTokens csrfTokens) {
		return new RestRequestObjectBuilder().withRestUrl(url).
				withRestRequestMethod("GET").
				withHttpHeaders(buildDefaultHeadersWithCSRF(csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())).
				build()
	}

	RequestObject buildPOSTRequest(String url, String body) {
		ArrayList<TestObjectProperty> defaultHeaders = buildDefaultHeaders()
		defaultHeaders.add(newHeader('Content-Type', 'application/json'))
		return buildPOSTRequestWithCustomHeaders(defaultHeaders, url, body)
	}

	private RequestObject buildPOSTRequestWithCustomHeaders(ArrayList<TestObjectProperty> defaultHeaders, String url, String body) {
		return new RestRequestObjectBuilder().withRestUrl(url).
				withRestRequestMethod("POST").
				withHttpHeaders(defaultHeaders).
				withTextBodyContent(body).build()
	}

	private RequestObject buildPOSTRequestWithCSRF(String method, String url, String bodyContent, String csrfToken, String webSessionToken) {
		return new RestRequestObjectBuilder().withRestUrl(url).
				withRestRequestMethod(method).
				withHttpHeaders(buildDefaultHeadersWithCSRF("Content-Type", 'application/json', csrfToken, webSessionToken)).
				withTextBodyContent(bodyContent).build()
	}

	private RequestObject buildPOSTRequestWithCSRF(String method, String url, String bodyContent, CSRFTokens csrfTokens) {
		return new RestRequestObjectBuilder().withRestUrl(url).
				withRestRequestMethod(method).
				withHttpHeaders(buildDefaultHeadersWithCSRF("Content-Type", 'application/json', csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())).
				withTextBodyContent(bodyContent).build()
	}

	private ArrayList<TestObjectProperty> buildDefaultHeadersWithCSRF(String headerKey= 'Accept', String headerValue = '*/*', String csrfToken, String webSessionToken) {
		ArrayList<TestObjectProperty> defaultHeaders = new ArrayList<>()
		defaultHeaders.add(newHeader(headerKey, headerValue))
		defaultHeaders.add(newHeader("x-csrf-token", URLDecoder.decode(csrfToken, "UTF-8")))
		defaultHeaders.add(newHeader("cookie", "_shout_web_session=" + webSessionToken))
		defaultHeaders
	}

	private ArrayList<TestObjectProperty> buildDefaultHeaders(String headerKey= 'Accept', String headerValue = '*/*') {
		ArrayList<TestObjectProperty> defaultHeaders = new ArrayList<>()
		defaultHeaders.add(newHeader(headerKey, headerValue))
		defaultHeaders
	}

	private RequestObject buildFormRequestWithCSRF(String method = "PUT", String url, String csrfToken, String webSessionToken,
			String fileName = "UDrEy.png", String typeValue = "BankStatementSnapshotFile", String stepValue = "financial") {
		List<FormDataBodyParameter> formDataList = new ArrayList<>()
		FormDataBodyParameter file = new FormDataBodyParameter('file', "Data Files" + File.separator + fileName, FormDataBodyParameter.PARAM_TYPE_FILE)
		FormDataBodyParameter type = new FormDataBodyParameter("type", typeValue, FormDataBodyParameter.PARAM_TYPE_TEXT)
		FormDataBodyParameter step = new FormDataBodyParameter("step", stepValue, FormDataBodyParameter.PARAM_TYPE_TEXT)
		formDataList.add(file)
		formDataList.add(type)
		formDataList.add(step)

		RequestObject request = new RestRequestObjectBuilder().withRestUrl(url).
				withRestRequestMethod(method).
				withHttpHeaders(buildDefaultHeadersWithCSRF("Content-Type",
				'multipart/form-data; boundary=----WebKitFormBoundaryfumSij8MCKWa46Dv',
				csrfToken,
				webSessionToken)).
				withMultipartFormDataBodyContent(formDataList).build()
		return request
	}

	ResponseObject sendGetRequest(String url) {
		sendRequest(buildGETRequest(url))
	}

	ResponseObject sendGetRequestWithCSRF (String url, String csrfToken, String webSessionToken) {
		sendRequest(buildGETRequestWithCSRF(url, csrfToken, webSessionToken))
	}

	ResponseObject sendGetRequestWithCSRF (String url, CSRFTokens csrfTokens) {
		sendRequest(buildGETRequestWithCSRF(url, csrfTokens))
	}

	ResponseObject sendPUTRequestWithCSRF (String url, String csrfToken, String webSessionToken) {
		sendRequest(buildFormRequestWithCSRF("PUT", url, csrfToken, webSessionToken ))
	}

	ResponseObject sendFormData (String url, String csrfToken, String webSessionToken, String fileName, String type, String step) {
		sendRequest(buildFormRequestWithCSRF("PUT", url, csrfToken, webSessionToken, fileName, type, step))
	}

	ResponseObject sendPOSTRequestWithCSRF (String url, String bodyContent, String csrfToken, String webSessionToken) {
		sendRequest(buildPOSTRequestWithCSRF("POST", url, bodyContent, csrfToken, webSessionToken))
	}

	ResponseObject sendPOSTRequestWithCSRF (String url, String bodyContent, CSRFTokens csrfTokens) {
		sendRequest(buildPOSTRequestWithCSRF("POST", url, bodyContent, csrfTokens))
	}

	ResponseObject sendPOSTRequestWithCSRF (String url, String bodyContent) {
		CSRFTokens csrfTokens = getTokensFromCSRF(CSRF_PATH)
		String csrfToken = csrfTokens.getCsrfToken()
		String webSessionToken = csrfTokens.getShoutWebSession()
		sendRequest(buildPOSTRequestWithCSRF("POST", url, bodyContent,csrfToken, webSessionToken))
	}

	ResponseObject sendGETRequestWithCSRF (String url) {
		CSRFTokens csrfTokens = getTokensFromCSRF(CSRF_PATH)
		String csrfToken = csrfTokens.getCsrfToken()
		String webSessionToken = csrfTokens.getShoutWebSession()
		sendRequest(buildGETRequestWithCSRF(url, csrfToken, webSessionToken))
	}

	ResponseObject sendPATCHRequestWithCSRF (String url, String csrfToken, String webSessionToken, String jsonBody) {
		sendRequest(buildPOSTRequestWithCSRF("PATCH", url, jsonBody, csrfToken, webSessionToken))
	}

	ResponseObject sendGetRequestWithAuthorization(String url, String authToken) {
		return WSBuiltInKeywords.sendRequest(buildGETRequest(url, authToken))
	}

	ResponseObject sendPostRequest(String url, String body) {
		ResponseObject response = sendRequest(buildPOSTRequest(url, body))
		response
	}

	ResponseObject sendPostRequestWithCustomHeaders(ArrayList<TestObjectProperty> defaultHeaders, String url, String body) {
		ResponseObject response = sendRequest(buildPOSTRequestWithCustomHeaders(defaultHeaders, url, body))
		response
	}

	private TestObjectProperty newHeader(String key, String value) {
		new TestObjectProperty(key, ConditionType.EQUALS, value)
	}

	String getAdminAuthToken(String username, String pass){
		AdminUser ad = new AdminUser()
		ad.setUsername(username)
		ad.setPassword(pass)
		String bodyContent = gson.toJson(ad)
		println "bodyContent = ${bodyContent}"
		ResponseObject res = sendPostRequest(GlobalVariable.G_s3Migration_baseAPIUrl + AUTHEN_TOKEN_PATH, bodyContent)
		return slurper.parseText(res.getResponseBodyContent()).data.token
	}

	public <T> T mapJSONResponseToModel(ResponseObject response, Class<T> clazz) {
		JsonElement responseJson = gson.fromJson(response.getResponseBodyContent(), JsonObject.class)
		T model = gson.fromJson(responseJson, clazz)
		return model
	}

	public <T> T mapResponseToModel(ResponseObject response, Class<T> clazz) {
		def contentBody = response.getResponseBodyContent()
		if (Objects.isNull(response) || Objects.isNull(contentBody)) {
			throw new IllegalArgumentException("Response or response body is null")
		}
		ServiceResponse sr = gson.fromJson(contentBody, ServiceResponse.class)
		return gson.fromJson(sr.getData(), clazz)
	}

	CSRFTokens getTokensFromCSRF(String baseAPIUrl = GlobalVariable.G_baseAPIUrl, String csrfRequestPath){
		ResponseObject csrf = sendGetRequest(baseAPIUrl + csrfRequestPath)
		KeywordUtil.logInfo("csrf status = ${csrf.getStatusCode()}")
		CSRFTokens tokens = getSetCookies(csrf.getHeaderFields())
		return tokens
	}

	CSRFTokens getUserAuthorization(String baseAPIURL = GlobalVariable.G_baseAPIUrl, String userName, String password) {
		ResponseObject login = postAuthenToken(userName, password, baseAPIURL)
		return getSetCookies(login.getHeaderFields())
	}

	User getUserLoginInfo(String userName, String password){
		ResponseObject login = postAuthenToken(userName, password)
		return mapResponseToModel(login, User.class)
	}

	private ResponseObject postAuthenToken(String userName, String password, String baseAPIURL = GlobalVariable.G_baseAPIUrl) {
		AdminUser user = new AdminUser()
		user.setUsername(userName)
		user.setPassword(password)
		def jsBody = gson.toJson(user)
		ResponseObject login = sendPOSTRequestWithCSRF(baseAPIURL + AUTHEN_TOKEN_PATH, jsBody)
		KeywordUtil.logInfo("POST $AUTHEN_TOKEN_PATH user body: $jsBody")
		KeywordUtil.logInfo("POST $AUTHEN_TOKEN_PATH bodyContent= " + login.getResponseBodyContent())
		Assert.assertEquals(slurper.parseText(login.getResponseBodyContent()).status, "success", "$AUTHEN_TOKEN_PATH failed, status is ${login.statusCode}")
		login
	}

	CSRFTokens getSetCookies(Map<String, List<String>> headerFields) {
		def setCookies = headerFields.get("Set-Cookie")
		KeywordUtil.logInfo("setCookies = ${setCookies}")
		CSRFTokens tokens = new CSRFTokens()
		for (int i = 0; i <setCookies.size() ; i++) {
			def splitCookies = StringUtils.split(setCookies[i], ";")
			for (String cookie : splitCookies) {
				if (StringUtils.substringBefore(cookie, "=") == "_shout_web_session") {
					tokens.setShoutWebSession(StringUtils.substringAfter(cookie, "="))
				} else if (StringUtils.substringBefore(cookie, "=") == "_csrf_token") {
					tokens.setCsrfToken(StringUtils.substringAfter(cookie, "="))
					break
				}
			}
		}
		tokens
	}

	private String getCSRFTokenFromCookie(String csrfCookies) {
		def csrfToken = csrfCookies.substring(0, csrfCookies.indexOf(";")).substring(csrfCookies.substring(0, csrfCookies.indexOf(";")).indexOf("=") + 1)
		KeywordUtil.logInfo("csrfToken = $csrfToken")
		csrfToken
	}

	private String getWebSessionTokenFromCookie(String webSessionCookie) {
		def webSessionToken = webSessionCookie.substring(0, webSessionCookie.indexOf(";")).substring(webSessionCookie.substring(0, webSessionCookie.indexOf(";")).indexOf("=") + 1)
		KeywordUtil.logInfo("webSessionToken = $webSessionToken")
		webSessionToken
	}

	public <T> List<T> sendGET(CSRFTokens csrfTokens, String requestUrl, Class<T> clazz) {
		String csrfToken = csrfTokens.getCsrfToken()
		String webSessionToken = csrfTokens.getShoutWebSession()
		Map<String, String> headers = new HashMap<String, String>()
		headers.put("x-csrf-token",  URLDecoder.decode(csrfToken, "UTF-8"))
		headers.put("cookie", "_shout_web_session=" + webSessionToken)
		int retry = 10
		Response response
		while (retry > 0) {
			try {
				response = (Response)(RestAssured.given().redirects().follow(true).headers(headers).when().get(requestUrl, new Object[0])).then().log().all().extract().response();
				if (response.getStatusCode() == 200) break
			}catch (Exception e) {
				KeywordUtil.logInfo("Caught exception: " + e.getMessage())
				Thread.sleep(1000)
				retry--
				KeywordUtil.logInfo("retry: " + retry)
			}
		}

		List<T> txnList = response.jsonPath().getList("data")
		return txnList
	}

	Response sendGET() {
		RequestSpecification httpRequest = RestAssured.given()
	}
}
