package com.core.utilities.webservice

import static com.core.utilities.common.Constants.*

import org.testng.Assert

import com.core.utilities.common.DateTimeUtils
import com.core.utilities.models.CSRFTokens
import com.core.utilities.models.transaction.TransactionDetails
import com.core.utilities.models.transaction.TransactionRefine
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil

import groovy.json.JsonSlurper
import internal.GlobalVariable

class TransactionAPITesting {

	BaseAPITesting api = new BaseAPITesting()
	private JsonSlurper slurper = new JsonSlurper()
	int EMPTY_RETRY = 30
	int FIND_SUBMITTED_TIME_RETRY = 40
	int GET_TXNLIST_RETRY = 5
	def SLEEP_RETRY_TXNLIST = 10000

	private TransactionDetails filterExactTransaction(Date dateTimeTxnSubmitted, String txnListingAPIPath = ADMIN_TRANSACTION_HISTORY_PATH, TransactionRefine conditions) {
		String filterString = "&q=" + conditions.getShouterEmail() +
				"&qType=" + "shouter_email" +
				"&status=" + conditions.getStatus() +
				"&product=" + conditions.getProduct() +
				"&source=" + conditions.getSource() +
				"&paymentType=" + conditions.getPaymentType() +
				"&from=" + conditions.getfromDate()
		def frequency = conditions.getFrequency()
		if (frequency == "One time" | frequency == "Monthly") {
			filterString = filterString + "&frequency=" + frequency
		}
		CSRFTokens csrfTokens = api.getUserAuthorization(GlobalVariable.G_saUserName, GlobalVariable.G_loginPassword)
		def url = GlobalVariable.G_baseAPIUrl + txnListingAPIPath + filterString
		List<TransactionDetails> txnList = api.mapResponseToModel(sendGetTxnList(url, csrfTokens, GET_TXNLIST_RETRY), TransactionDetails[].class)
		def size = txnList.size()
		def isEmpty = txnList.isEmpty()
		def maxSubmittedDate
		KeywordUtil.logInfo("dateTimeTxnSubmitted = $dateTimeTxnSubmitted")
		if (isEmpty){
			while (isEmpty && GET_TXNLIST_RETRY>0){
				KeywordUtil.logInfo("Retry $EMPTY_RETRY time(s) when isEmpty is $isEmpty")
				txnList = api.mapResponseToModel(sendGetTxnList(url, csrfTokens, EMPTY_RETRY), TransactionDetails[].class)
				isEmpty = txnList.isEmpty()
				size = txnList.size()
				Thread.sleep(SLEEP_RETRY_TXNLIST)
				EMPTY_RETRY--
			}
		}
		if (size >= 1){
			maxSubmittedDate = DateTimeUtils.getDate("yyyy-MM-dd'T'HH:mm:ss.000'Z'", txnList.max {it.submittedDate}.submittedDate)
			KeywordUtil.logInfo("maxSubmittedDate = $maxSubmittedDate")
			if (maxSubmittedDate.before(dateTimeTxnSubmitted)) {
				while (maxSubmittedDate.before(dateTimeTxnSubmitted) && FIND_SUBMITTED_TIME_RETRY>0){
					KeywordUtil.logInfo("Retry $FIND_SUBMITTED_TIME_RETRY time(s) when maxSubmittedDate [$maxSubmittedDate] before dateTimeTxnSubmitted [$dateTimeTxnSubmitted] is: ${maxSubmittedDate.before(dateTimeTxnSubmitted)}")
					txnList = api.mapResponseToModel(sendGetTxnList(url, csrfTokens, FIND_SUBMITTED_TIME_RETRY), TransactionDetails[].class)
					maxSubmittedDate = DateTimeUtils.getDate("yyyy-MM-dd'T'HH:mm:ss.000'Z'", txnList.max {it.submittedDate}.submittedDate)
					Thread.sleep(SLEEP_RETRY_TXNLIST)
					FIND_SUBMITTED_TIME_RETRY--
				}
			}
			if (maxSubmittedDate.compareTo(dateTimeTxnSubmitted)>=0) {
				KeywordUtil.logInfo("Not retry When maxSubmittedDate [$maxSubmittedDate] afterOrEqual dateTimeTxnSubmitted [$dateTimeTxnSubmitted] is: ${maxSubmittedDate.compareTo(dateTimeTxnSubmitted)>=0}")
				return txnList.max{it.getSubmittedDate()}
			}else {
				KeywordUtil.logInfo("Found 0 txn because maxSubmittedDate.compareTo(dateTimeTxnSubmitted) is not equalOrAfter: ${maxSubmittedDate.compareTo(dateTimeTxnSubmitted)>=0}")
			}
		}
	}

	private ResponseObject sendGetTxnList(String url, CSRFTokens csrfTokens, int retry) {
		def responseCode
		ResponseObject txnRefine
		while (retry>0) {
			try {
				txnRefine = api.sendGetRequestWithCSRF(url, csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())
				responseCode = txnRefine.getStatusCode()
				KeywordUtil.logInfo("GET $url respone code: $responseCode")
			} catch (Exception e) {
				KeywordUtil.logInfo("Caught exception: ${e.getMessage()} \n retry $retry...")
			}
			if (responseCode == 200) break
				if (responseCode == 500) {
					KeywordUtil.logInfo("$url code 500 retry $retry ...")
					Thread.sleep(SLEEP_RETRY_TXNLIST)
				}
			retry--
		}
		KeywordUtil.logInfo("GET $url response: " + txnRefine.getResponseBodyContent())
		Assert.assertEquals(responseCode, 200, "GET $url status code is not 200")
		txnRefine
	}

	String getExactTxnId(Date dateTimeTxnSubmitted, TransactionRefine conditions) {
		def id
		try {
			id = filterExactTransaction(dateTimeTxnSubmitted, conditions).id
			KeywordUtil.logInfo("transId: ${id}")
			return id
		} catch (NullPointerException e){
			KeywordUtil.markFailed("Not found any transaction matching refinde condition")
		}
	}

	String getExactPlanId(TransactionRefine conditions) {
		return filterExactTransaction(ADMIN_RECURRING_HISTORY_PATH, conditions).getId()
	}

	static Date getTxnSubmittedDate() {
		return DateTimeUtils.getDateMinusSecond(DateTimeUtils.getDate("yyyy-MM-dd'T'HH:mm:ss.000'Z'", DateTimeUtils.getUTCNow("yyyy-MM-dd'T'HH:mm:ss.000'Z'")), 1)
	}

	String getTotalSuccessfullTxns(String txnListingAPIPath = ADMIN_TRANSACTION_HISTORY_PATH, TransactionRefine conditions){
		String filterString = "&status=successful" + "&from="+ conditions.getfromDate() + "&to=" + conditions.gettoDate()
		CSRFTokens csrfTokens = api.getUserAuthorization(GlobalVariable.G_saUserName, GlobalVariable.G_loginPassword)
		def urldefault = GlobalVariable.G_baseAPIUrl + txnListingAPIPath + filterString
		ResponseObject responseListdefault = api.sendGetRequestWithCSRF(urldefault, csrfTokens.getCsrfToken(),csrfTokens.getShoutWebSession())
		String totalTxns =  slurper.parseText(responseListdefault.getResponseBodyContent()).total
		KeywordUtil.logInfo("total txns fundraiser" + totalTxns)
		return totalTxns
	}
}
