package com.core.utilities.webservice

import static com.core.utilities.common.Constants.*

import com.core.utilities.common.DateTimeUtils
import com.core.utilities.models.CSRFTokens
import com.core.utilities.models.sa.FundraiserRefine
import com.google.gson.Gson
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil

import groovy.json.JsonSlurper
import internal.GlobalVariable

public class FundraiserAPITesting {

	BaseAPITesting api = new BaseAPITesting()
	private JsonSlurper slurper = new JsonSlurper()
	private Gson gson = new Gson()
	DateTimeUtils datetime = new DateTimeUtils()

	String countLiveFundraiserbyFundraiserStatus(String FundraiserListingAPIPath = ADMIN_FUNDRAISERS_PATH, FundraiserRefine conditions) {

		String filterString = "&status=" + conditions.getfundraiserStatus() + "&from="+ conditions.getFromDate() + "&to=" + DateTimeUtils.getCurrentDate("yyyy-MM-dd")
		CSRFTokens csrfTokens = api.getUserAuthorization(GlobalVariable.G_saUserName, GlobalVariable.G_loginPassword)
		def urldefault = GlobalVariable.G_baseAPIUrl + FundraiserListingAPIPath + filterString
		ResponseObject responseListdefault = api.sendGetRequestWithCSRF(urldefault, csrfTokens.getCsrfToken(),csrfTokens.getShoutWebSession())
		String totalFundraiser =  slurper.parseText(responseListdefault.getResponseBodyContent()).total
		KeywordUtil.logInfo("total fundraiser is :" + totalFundraiser)
		int itotalFundraiser = Integer.parseInt(totalFundraiser)
		def url
		//	if (itotalFundraiser>100 && itotalFundraiser % 100 !=0){
		int count = itotalFundraiser
		for (int i = 1; i <= (count/100); i++){
			url = GlobalVariable.G_baseAPIUrl + "/admin/fundraisers?page=" + i + "&pageSize=100"  + filterString
			ResponseObject responseList = api.sendGetRequestWithCSRF(url, csrfTokens.getCsrfToken(), csrfTokens.getShoutWebSession())
			for (int j = 0; j < 100; j++){
				String startLiveDate = slurper.parseText(responseList.getResponseBodyContent()).data[j].startDate
				String endLiveDate = slurper.parseText(responseList.getResponseBodyContent()).data[j].endDate
				String compareStartDate = datetime.compareDateTime(startLiveDate, conditions.getToDate())
				String compareEndDate = datetime.compareDateTime(endLiveDate, conditions.getFromDate())
				if (compareStartDate == "after"){
					itotalFundraiser = itotalFundraiser - 1
				}
				if  (compareEndDate == "before"){
					itotalFundraiser = itotalFundraiser - 1
				}
				KeywordUtil.logInfo("total là " + itotalFundraiser)
			}
		}
		KeywordUtil.logInfo("total fundraiser" + itotalFundraiser)
		return String.valueOf(itotalFundraiser)
	}

	String countFutureFundraiserbyFundraiserStatus(String FundraiserListingAPIPath = ADMIN_FUNDRAISERS_PATH, FundraiserRefine conditions) {

		String filterString = "&status=" + conditions.getfundraiserStatus() + "&from="+ DateTimeUtils.getCurrentDate("yyyy-MM-dd") + "&to=" + conditions.getToDate()
		CSRFTokens csrfTokens = api.getUserAuthorization(GlobalVariable.G_saUserName, GlobalVariable.G_loginPassword)
		def urldefault = GlobalVariable.G_baseAPIUrl + FundraiserListingAPIPath + filterString
		ResponseObject responseListdefault = api.sendGetRequestWithCSRF(urldefault, csrfTokens.getCsrfToken(),csrfTokens.getShoutWebSession())
		String totalFutureFundraiser =  slurper.parseText(responseListdefault.getResponseBodyContent()).total
		KeywordUtil.logInfo("total future fundraiser" + totalFutureFundraiser)
		return totalFutureFundraiser
	}
}
