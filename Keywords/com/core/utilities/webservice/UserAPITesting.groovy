package com.core.utilities.webservice

import static com.core.utilities.common.Constants.*

import java.lang.reflect.Field

import org.testng.Assert

import com.core.utilities.common.DateTimeUtils
import com.core.utilities.models.CSRFTokens
import com.core.utilities.models.sa.UserRefine
import com.google.gson.Gson
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil

import groovy.json.JsonSlurper
import internal.GlobalVariable

public class UserAPITesting {

	BaseAPITesting api = new BaseAPITesting()
	int EMPTY_RETRY = 30
	int FIND_SUBMITTED_TIME_RETRY = 100
	int GET_TXNLIST_RETRY = 5
	def SLEEP_RETRY_TXNLIST = 10000
	private JsonSlurper slurper = new JsonSlurper()
	private Gson gson = new Gson()

	String countUserByRole(String userListingAPIPath = USER_LISTING_PATH, UserRefine conditions) {

		String filterString = "&status" + conditions.getuserStatus() + "&role=" + conditions.getuserRole().toQueryString() + "&from="+ conditions.getfromDate() + "&to=" + conditions.gettoDate()
		CSRFTokens csrfTokens = api.getUserAuthorization(GlobalVariable.G_saUserName, GlobalVariable.G_loginPassword)
		def urldefault = GlobalVariable.G_baseAPIUrl + userListingAPIPath + filterString
		ResponseObject responseListdefault = api.sendGetRequestWithCSRF(urldefault, csrfTokens.getCsrfToken(),csrfTokens.getShoutWebSession())
		String totalUser =  slurper.parseText(responseListdefault.getResponseBodyContent()).total
		return totalUser
	}

	Float countUserbyShouterRole(String fromDate, String toDate){
		UserRefine shouterRoleCondition = new UserRefine()
		shouterRoleCondition.getuserRole().setroleGroup("giver")
		shouterRoleCondition.getuserRole().setrole("shouter")
		shouterRoleCondition.setfromDate(fromDate)
		shouterRoleCondition.settoDate(toDate)
		String totalShouter = countUserByRole(shouterRoleCondition)
		return Float.parseFloat(totalShouter)
	}

	Float countUserbyCharityRole(String fromDate, String toDate){

		UserRefine charityapplicantCondition = new UserRefine()
		charityapplicantCondition.getuserRole().setroleGroup("charity")
		charityapplicantCondition.getuserRole().setrole("applicant")
		charityapplicantCondition.setfromDate(fromDate)
		charityapplicantCondition.settoDate(toDate)
		String totalCharityApplicant = countUserByRole(charityapplicantCondition)

		UserRefine charityownerCondition = new UserRefine()
		charityownerCondition.getuserRole().setroleGroup("charity")
		charityownerCondition.getuserRole().setrole("owner")
		charityownerCondition.setfromDate(fromDate)
		charityownerCondition.settoDate(toDate)
		String totalCharityOwner = countUserByRole(charityownerCondition)

		UserRefine charityadminCondition = new UserRefine()
		charityadminCondition.getuserRole().setroleGroup("charity")
		charityadminCondition.getuserRole().setrole("admin")
		charityadminCondition.setfromDate(fromDate)
		charityadminCondition.settoDate(toDate)
		String totalCharityAdmin = countUserByRole(charityadminCondition)

		UserRefine charityreportingCondition = new UserRefine()
		charityreportingCondition.getuserRole().setroleGroup("charity")
		charityreportingCondition.getuserRole().setrole("admin")
		charityreportingCondition.setfromDate(fromDate)
		charityreportingCondition.settoDate(toDate)
		String totalCharityReporting = countUserByRole(charityreportingCondition)

		UserRefine charityuserCondition = new UserRefine()
		charityuserCondition.getuserRole().setroleGroup("charity")
		charityuserCondition.getuserRole().setrole("admin")
		charityuserCondition.setfromDate(fromDate)
		charityuserCondition.settoDate(toDate)
		String totalCharityUser = countUserByRole(charityuserCondition)

		Float totalCharity = Float.parseFloat(totalCharityOwner) + Float.parseFloat(totalCharityAdmin) + Float.parseFloat(totalCharityReporting) + Float.parseFloat(totalCharityUser)
		return totalCharity
	}
}
