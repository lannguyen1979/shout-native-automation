package com.core.utilities.webservice

import com.baseTests.dataComparison.CompareUtils
import com.core.utilities.models.CSRFTokens
import com.core.utilities.models.ServiceResponse
import com.core.utilities.models.donation.DonationItem
import com.core.utilities.models.nativeApp.WhoList
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.util.KeywordUtil
import org.testng.Assert

import java.util.concurrent.ThreadLocalRandom

import static com.baseTests.dataComparison.CompareUtils.*
import static com.core.utilities.common.Constants.APP_CLIP_DONATION_INFO
import static internal.GlobalVariable.*

class AppClipTesting {

    BaseAPITesting api = new BaseAPITesting()
    NativeAppTesting nativeAppTesting = new NativeAppTesting()
    Gson gson =  new Gson()

    private JsonObject getSettingDonationInfo(String charityId){
        CSRFTokens csrfTokens = api.getUserAuthorization(G_saUserNameAPI, G_loginPassword)
        def donationSettingPath = "/admin/charities/$charityId/app-clip-donation-setting"
        KeywordUtil.logInfo("Sending GET request $donationSettingPath...")
        ResponseObject donationInfoResponse = api.sendGetRequestWithCSRF(
                G_baseAPIUrl + donationSettingPath,
                csrfTokens.getCsrfToken(),
                csrfTokens.getShoutWebSession())
        Assert.assertEquals(donationInfoResponse.getStatusCode(), 200, "Status code is not 200. Response is ${donationInfoResponse.getResponseBodyContent()}")
        return gson.fromJson(donationInfoResponse.getResponseBodyContent(), ServiceResponse.class).getData()
    }

    private List<DonationItem> getActiveDonation(JsonObject dataResponseFromDonationInfo){
        JsonObject donationItemSettings = dataResponseFromDonationInfo.getAsJsonObject("donationItemSettings")
        boolean active = donationItemSettings.get("active").asBoolean
        boolean useDefault = donationItemSettings.get("useDefault").asBoolean
        if(useDefault && active){
            return gson.fromJson(gson.toJson(donationItemSettings.getAsJsonArray("defaultItems")), DonationItem[])
        } else if (active && !useDefault){
            return gson.fromJson(gson.toJson(donationItemSettings.getAsJsonArray("customItems")), DonationItem[])
        }
    }

    String getUrlSettings(JsonObject dataResponFromDonationInfo, String urlType = "appClipUrl"){
        return dataResponFromDonationInfo.getAsJsonObject(urlType).getAsString()
    }

    List<DonationItem> getSettingDonationItems(String charityId){
        return getActiveDonation(getSettingDonationInfo(charityId))
    }

    List<DonationItem> getCharityDonationItems(String charityId){
        CSRFTokens csrfTokens = api.getUserAuthorization(G_saUserNameAPI, G_loginPassword)
        ResponseObject donationInfoResponse = api.sendGetRequestWithCSRF(
                G_baseAPIUrl + APP_CLIP_DONATION_INFO + "$charityId/donation_info",
                csrfTokens.getCsrfToken(),
                csrfTokens.getShoutWebSession())
        JsonObject data = gson.fromJson(donationInfoResponse.getResponseBodyContent(), ServiceResponse.class).getData()
        return gson.fromJson(gson.toJson(data.getAsJsonArray("donationItems")), DonationItem[].class)
    }

    void verifyCharityDonationItems(){
        'Step: get donation items from app-clip-setting'
        List<WhoList> charities = nativeAppTesting.getCharities()
        for (WhoList charity:charities){
            def charityId = charity.getId()
            List<DonationItem> expectedDonationItems = getSettingDonationItems(charityId)
            if(Objects.nonNull(expectedDonationItems)){
                List<DonationItem> actualDonationItems = getCharityDonationItems(charityId)
                KeywordUtil.logInfo("charityId: $charityId")
                KeywordUtil.logInfo("expectedDonationItems:" + gson.toJson(expectedDonationItems))
                KeywordUtil.logInfo("actualDonationItems:  " + gson.toJson(actualDonationItems))
                Assert.assertTrue(dataEquals(actualDonationItems, expectedDonationItems), "Charity Donation Items is not in Settings")
                break
            } else {
                KeywordUtil.logInfo("No donationItem for comparision")
            }
        }
    }

    private String getRandomCharityId() {
        List<WhoList> charities = nativeAppTesting.getCharities()
        def charityId = charities.get(getRandomNumber(0, charities.size())).getId()
        charityId
    }

    private int getRandomNumber(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1)
    }

}
