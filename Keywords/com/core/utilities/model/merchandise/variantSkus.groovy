package com.core.utilities.model.merchandise

import com.sun.org.apache.bcel.internal.generic.RETURN

public class variantSkus {
	private Attribute attribute
	private int id
	private String name
	private String amount
	private boolean isEnableStockLimit
	
	void setattribute(Attribute attribute) {
		this.attribute = attribute
	}
	
	Attribute getattribute() {
		return attribute
	}
	
	void setid(int id) {
		this.id= id
	}
	
	int id() {
		return id
	}
	
	void setName(String name) {
		this.name=name
	}
	
	String getName() {
		return name
	}
	
	void setamount(String amount) {
		this.amount=amount
	}
	
	String getAmount() {
		return amount
	}
	
	void setisEnableStockLimit(boolean isEnableStockLimit) {
		this.isEnableStockLimit = isEnableStockLimit
	}
	
	boolean getisEnableStockLimit() {
		return isEnableStockLimit
	}
}
