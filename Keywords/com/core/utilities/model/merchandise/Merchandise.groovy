package com.core.utilities.model.merchandise

public class Merchandise {
	private int id
	private String merchandiseName
	private String merchandiseAmount
	private boolean IsMerchandiseVariant
	private boolean isEnableStockLimit

	int getid(){
		return id
	}

	void setid(int id){
		this.id = id
	}

	String getmerchandiseName() {
		return merchandiseName
	}

	void setMerchandiseName(String merchandiseName) {
		this.merchandiseName= merchandiseName
	}

	String getmerchandiseAmount() {
		return merchandiseAmount
	}

	void setMerrchandiseAmount(String merchandiseAmount) {
		this.merchandiseAmount= merchandiseAmount
	}

	boolean getIsMerchandiseVariant() {
		return IsMerchandiseVariant
	}

	void setIsMerchandiseVariant(boolean IsMerchandiseVariant) {
		this.IsMerchandiseVariant = IsMerchandiseVariant
	}

	boolean getIsEnableStockLimit() {
		this.isEnableStockLimit = isEnableStockLimit
	}

	void setIsEnableStockLimit() {
		this.IsMerchandiseVariant = IsMerchandiseVariant
	}
}
