package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object G_Timeout
     
    /**
     * <p></p>
     */
    public static Object G_NotificationMessage
     
    /**
     * <p></p>
     */
    public static Object G_AndroidAppID
     
    /**
     * <p></p>
     */
    public static Object G_ShortTimeOut
     
    /**
     * <p>Profile default : seconds
Profile ENV_QA2 : seconds</p>
     */
    public static Object G_timeOut1s
     
    /**
     * <p>Profile default : seconds
Profile ENV_QA2 : seconds</p>
     */
    public static Object G_timeOut5s
     
    /**
     * <p>Profile default : seconds
Profile ENV_QA2 : seconds</p>
     */
    public static Object G_minTimeOut
     
    /**
     * <p>Profile default : seconds
Profile ENV_QA2 : seconds</p>
     */
    public static Object G_maxTimeOut
     
    /**
     * <p></p>
     */
    public static Object G_baseUrl
     
    /**
     * <p></p>
     */
    public static Object G_loginUrl
     
    /**
     * <p></p>
     */
    public static Object G_emailUrl
     
    /**
     * <p></p>
     */
    public static Object G_emailUser
     
    /**
     * <p></p>
     */
    public static Object G_gEmailLogin
     
    /**
     * <p></p>
     */
    public static Object G_emailPassword
     
    /**
     * <p></p>
     */
    public static Object G_loginUserName
     
    /**
     * <p>Profile ENV_QA2 : lyhan@kms-technology.com</p>
     */
    public static Object G_saUserName
     
    /**
     * <p>Profile default : shout_autotest_01@kms-technology.com
Profile ENV_QA2 : kms.shouter@gmail.com</p>
     */
    public static Object G_shouterUsername
     
    /**
     * <p></p>
     */
    public static Object G_loginPassword
     
    /**
     * <p></p>
     */
    public static Object G_charityPath
     
    /**
     * <p>Profile default : http:&#47;&#47;qa1-api.kms-technology.com
Profile ENV_QA2 : http:&#47;&#47;qa1-api.kms-technology.com</p>
     */
    public static Object G_baseAPIUrl
     
    /**
     * <p></p>
     */
    public static Object G_Shout2_baseAPIUrl
     
    /**
     * <p></p>
     */
    public static Object gDateSubmitted
     
    /**
     * <p></p>
     */
    public static Object gDateTxnSubmited
     
    /**
     * <p></p>
     */
    public static Object gDateTimeSubmited
     
    /**
     * <p>Profile ENV_QA2 : seconds</p>
     */
    public static Object G_timeOut2s
     
    /**
     * <p>Profile ENV_QA2 : seconds</p>
     */
    public static Object G_timeOut3s
     
    /**
     * <p></p>
     */
    public static Object G_saUserNameAPI
     
    /**
     * <p></p>
     */
    public static Object G_charityUserName
     
    /**
     * <p>Profile ENV_QA2 : http:&#47;&#47;qa2-api.kms-technology.com</p>
     */
    public static Object G_baseAPIUrlQA2
     
    /**
     * <p></p>
     */
    public static Object G_s3Migration_baseAPIUrl
     
    /**
     * <p></p>
     */
    public static Object G_adminUser
     
    /**
     * <p>Profile ENV_QA2 : dev1_shout2_01</p>
     */
    public static Object G_s2DbUrl
     
    /**
     * <p></p>
     */
    public static Object G_dbS2User
     
    /**
     * <p></p>
     */
    public static Object G_dbS2Pass
     
    /**
     * <p></p>
     */
    public static Object G_otpNumber
     
    /**
     * <p></p>
     */
    public static Object G_explorationKeyword
     
    /**
     * <p></p>
     */
    public static Object G_buttonToken
     
    /**
     * <p></p>
     */
    public static Object G_merchandiseId
     
    /**
     * <p></p>
     */
    public static Object G_emailSA1
     
    /**
     * <p></p>
     */
    public static Object G_emailSA2
     
    /**
     * <p></p>
     */
    public static Object G_shouterUsername02
     
    /**
     * <p></p>
     */
    public static Object G_s3MigrationUser
     
    /**
     * <p>Profile ENV_QA2 : &#47;fundraisers&#47;fundraiser-automation</p>
     */
    public static Object G_FundraiserPath
     
    /**
     * <p></p>
     */
    public static Object G_shouterPhone
     
    /**
     * <p></p>
     */
    public static Object G_suffixCharityEmail
     
    /**
     * <p></p>
     */
    public static Object G_saUser
     
    /**
     * <p></p>
     */
    public static Object G_saRoot
     
    /**
     * <p></p>
     */
    public static Object G_machShipAPIToken
     
    /**
     * <p></p>
     */
    public static Object G_machShipLocationId
     
    /**
     * <p></p>
     */
    public static Object G_machShipSKU
     
    /**
     * <p></p>
     */
    public static Object G_newPassword
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += TestCaseMain.getParsedValues(RunConfiguration.getOverridingParameters())
    
            G_Timeout = selectedVariables['G_Timeout']
            G_NotificationMessage = selectedVariables['G_NotificationMessage']
            G_AndroidAppID = selectedVariables['G_AndroidAppID']
            G_ShortTimeOut = selectedVariables['G_ShortTimeOut']
            G_timeOut1s = selectedVariables['G_timeOut1s']
            G_timeOut5s = selectedVariables['G_timeOut5s']
            G_minTimeOut = selectedVariables['G_minTimeOut']
            G_maxTimeOut = selectedVariables['G_maxTimeOut']
            G_baseUrl = selectedVariables['G_baseUrl']
            G_loginUrl = selectedVariables['G_loginUrl']
            G_emailUrl = selectedVariables['G_emailUrl']
            G_emailUser = selectedVariables['G_emailUser']
            G_gEmailLogin = selectedVariables['G_gEmailLogin']
            G_emailPassword = selectedVariables['G_emailPassword']
            G_loginUserName = selectedVariables['G_loginUserName']
            G_saUserName = selectedVariables['G_saUserName']
            G_shouterUsername = selectedVariables['G_shouterUsername']
            G_loginPassword = selectedVariables['G_loginPassword']
            G_charityPath = selectedVariables['G_charityPath']
            G_baseAPIUrl = selectedVariables['G_baseAPIUrl']
            G_Shout2_baseAPIUrl = selectedVariables['G_Shout2_baseAPIUrl']
            gDateSubmitted = selectedVariables['gDateSubmitted']
            gDateTxnSubmited = selectedVariables['gDateTxnSubmited']
            gDateTimeSubmited = selectedVariables['gDateTimeSubmited']
            G_timeOut2s = selectedVariables['G_timeOut2s']
            G_timeOut3s = selectedVariables['G_timeOut3s']
            G_saUserNameAPI = selectedVariables['G_saUserNameAPI']
            G_charityUserName = selectedVariables['G_charityUserName']
            G_baseAPIUrlQA2 = selectedVariables['G_baseAPIUrlQA2']
            G_s3Migration_baseAPIUrl = selectedVariables['G_s3Migration_baseAPIUrl']
            G_adminUser = selectedVariables['G_adminUser']
            G_s2DbUrl = selectedVariables['G_s2DbUrl']
            G_dbS2User = selectedVariables['G_dbS2User']
            G_dbS2Pass = selectedVariables['G_dbS2Pass']
            G_otpNumber = selectedVariables['G_otpNumber']
            G_explorationKeyword = selectedVariables['G_explorationKeyword']
            G_buttonToken = selectedVariables['G_buttonToken']
            G_merchandiseId = selectedVariables['G_merchandiseId']
            G_emailSA1 = selectedVariables['G_emailSA1']
            G_emailSA2 = selectedVariables['G_emailSA2']
            G_shouterUsername02 = selectedVariables['G_shouterUsername02']
            G_s3MigrationUser = selectedVariables['G_s3MigrationUser']
            G_FundraiserPath = selectedVariables['G_FundraiserPath']
            G_shouterPhone = selectedVariables['G_shouterPhone']
            G_suffixCharityEmail = selectedVariables['G_suffixCharityEmail']
            G_saUser = selectedVariables['G_saUser']
            G_saRoot = selectedVariables['G_saRoot']
            G_machShipAPIToken = selectedVariables['G_machShipAPIToken']
            G_machShipLocationId = selectedVariables['G_machShipLocationId']
            G_machShipSKU = selectedVariables['G_machShipSKU']
            G_newPassword = selectedVariables['G_newPassword']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
