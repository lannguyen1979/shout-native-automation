import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable
import com.core.utilities.common.DateTimeUtils as DateTimeUtils
import com.core.utilities.webservice.TransactionAPITesting as TransactionAPITesting

import com.core.utilities.common.CommonMobileActions as mobile
import com.core.utilities.common.Enum as Enum
import com.gui.mobile.screens.CharityDetail as CharityDetail
import com.gui.mobile.screens.InitialApp as InitialApp
import com.gui.mobile.screens.ConfirmPayment as ConfirmPayment
import com.gui.mobile.screens.MerchandiseItem as MerchandiseItem
import com.gui.mobile.screens.ThankYou as ThankYou

CharityDetail charity = new CharityDetail()

InitialApp inItialApp = new InitialApp()

MerchandiseItem merchandise = new MerchandiseItem()

ConfirmPayment confirmPayment = new ConfirmPayment()

ThankYou thankYou = new ThankYou()

def charityName = 'ActionAid Australia'

def merchandiseName = "Item2 long name long name long name long name long name long name long name long name long name long name"

//Go to charity detail screen
inItialApp.inItialApp()

//Click on Shop button to navigate to Merchandise list screen
charity.charityDonationWithMerchandise(charityName)

//Select merchandise has variant and Next to Confirm Payment screen
merchandise.selectMerchandiseName(merchandiseName)

//Click on GPay to donate
confirmPayment.donationWithGPay()

//Verify donate success and go to thank you and return to back to CH detail
thankYou.verifyNavigateToCharityDetailcreen()

mobile.closeApplication()

