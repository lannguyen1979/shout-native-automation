
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable
import com.core.utilities.common.DateTimeUtils as DateTimeUtils
import com.core.utilities.webservice.TransactionAPITesting as TransactionAPITesting

import com.core.utilities.common.CommonMobileActions as mobile
import com.core.utilities.common.Enum as Enum
import com.gui.mobile.screens.CharityDetail as CharityDetail
import com.gui.mobile.screens.InitialApp as InitialApp
import com.gui.mobile.screens.DonationImpact as DonationImpact
import com.gui.mobile.screens.ThankYou as ThankYou

CharityDetail charity = new CharityDetail()

InitialApp inItialApp = new InitialApp()

DonationImpact donationImpact = new DonationImpact()

ThankYou thankYou = new ThankYou()

def charityName = 'ActionAid Australia'

//Go to charity detail
inItialApp.inItialApp()

//click on Donate button
charity.charityDonationOnly(charityName)

//select donation impact and donate with GPay
donationImpact.donationWithGPay()

//verify it go to thank you page and done to navigate to Adscreen
thankYou.verifyNavigateToAdScreen()

mobile.closeApplication()