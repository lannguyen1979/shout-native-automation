import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration

Mobile.startExistingApplication(GlobalVariable.G_AndroidAppID)

RunConfiguration.setMobileDriverPreferencesProperty("appPackage", 'com.shoutforgood.shout')

RunConfiguration.setMobileDriverPreferencesProperty("appActivity", 'com.shoutforgood.shout.screen.splash.SplashScreenActivity')

Mobile.tap(findTestObject('Notice_Dialog/notice_message_btn_OK'), 0)

//Mobile.tap(findTestObject('Notice_Dialog/btn_GotIt'), 0)

Mobile.tap(findTestObject('ShoutWho/btnSearch'), 0)

Mobile.tap(findTestObject('ShoutWho/txtSearch'), 0)

//Mobile.tap(findTestObject('Notice_Dialog/notice_msg_SelectFavouriteCause'), 0)

Mobile.tap(findTestObject('ShoutWho/txtCharityName'), 0)

Mobile.tap(findTestObject('CharityDetail/btnShop'), 0)

Mobile.scrollToText('Help support')

Mobile.getText(findTestObject('MerchandiseListScreen/lblHelpSupport'), 0)

Mobile.getText(findTestObject('MerchandiseListScreen/lblMerchandiseName'), 0)

//Mobile.getText(findTestObject('MerchandiseListScreen/lsItemName'), 0)

Mobile.tap(findTestObject('MerchandiseListScreen/lsItemColor'), 0)

Mobile.scrollToText('Red')

Mobile.tap(findTestObject('MerchandiseListScreen/optColor'), 0)

Mobile.tap(findTestObject('MerchandiseListScreen/lsItemSize'), 0)

Mobile.scrollToText('Large')

Mobile.tap(findTestObject('MerchandiseListScreen/optSize'), 0)

Mobile.tap(findTestObject('MerchandiseListScreen/txtQuantity'), 0)

Mobile.tap(findTestObject('MerchandiseListScreen/btnAddToCart'), 0)

//Mobile.tap(findTestObject('Object Repository/android.widget.ImageButton'), 0)
//Mobile.tap(findTestObject('Object Repository/android.widget.Button - DONE'), 0)
Mobile.tap(findTestObject('MerchandiseListScreen/btnNext'), 0)

//Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Edit'), 0)
//Mobile.tap(findTestObject('Object Repository/android.widget.Button - DONE'), 0)
Mobile.tap(findTestObject('ConfirmPaymentScreen/btnOtherPayment'), 0)

Mobile.waitForElementPresent(findTestObject('OtherPayment-DonationScreen/btnSkipThisStep'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.swipe(300, 300, 0, 0)

Mobile.scrollToText('Skip This Step')

Mobile.tap(findTestObject('OtherPayment-DonationScreen/btnSkipThisStep'), 0)

Mobile.swipe(200, 200, 0, 0)

//Mobile.waitForElementPresent(findTestObject('Object Repository/android.view.View - Payment'), 0, FailureHandling.STOP_ON_FAILURE)
Mobile.waitForElementPresent(findTestObject('OtherPayment-DonationScreen/optPaymentByCreditCard'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('OtherPayment-DonationScreen/optPaymentByCreditCard'), 0)

Mobile.swipe(400, 400, 0, 0)

//Mobile.delay(1)

//Mobile.scrollToText('Details')
Mobile.waitForElementPresent(findTestObject('Object Repository/OtherPayment-DonationScreen/txtFirstName'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.setText(findTestObject('Object Repository/OtherPayment-DonationScreen/txtFirstName'),  'Lan', 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Object Repository/OtherPayment-DonationScreen/txtLastName'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.setText(findTestObject('Object Repository/OtherPayment-DonationScreen/txtLastName'), 'Nguyen', 0)

//Mobile.swipe(200, 200, 0, 0)

Mobile.waitForElementPresent(findTestObject('Object Repository/OtherPayment-DonationScreen/txtEmail'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Object Repository/OtherPayment-DonationScreen/txtEmail'), 'lannguyen@kms-technology.com', 0)

Mobile.waitForElementPresent(findTestObject('Object Repository/OtherPayment-DonationScreen/txtPhoneNumber'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Object Repository/OtherPayment-DonationScreen/txtPhoneNumber'), '499830224', 0)

Mobile.swipe(400, 400, 0, 0)


/*
 * Mobile.waitForElementPresent(findTestObject('Object
 * Repository/OtherPayment-DonationScreen/creditCardNumber'), 0,
 * FailureHandling.STOP_ON_FAILURE)
 * 
 * Mobile.setText(findTestObject('Object
 * Repository/OtherPayment-DonationScreen/creditCardNumber'),'4111111111111111',
 * 0)
 * 
 * Mobile.waitForElementPresent(findTestObject('Object
 * Repository/OtherPayment-DonationScreen/ccExpiry'), 0,
 * FailureHandling.STOP_ON_FAILURE)
 * 
 * Mobile.setText(findTestObject('Object
 * Repository/OtherPayment-DonationScreen/ccExpiry'), '12/21', 0)
 * 
 * Mobile.waitForElementPresent(findTestObject('Object
 * Repository/OtherPayment-DonationScreen/cCV'),
 * 0,FailureHandling.STOP_ON_FAILURE)
 * 
 * Mobile.setText(findTestObject('Object
 * Repository/OtherPayment-DonationScreen/cCV'), '123', 0)
 */
Mobile.swipe(400, 400, 0, 0)

Mobile.waitForElementPresent(findTestObject('OtherPayment-DonationScreen/btnDonate'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('OtherPayment-DonationScreen/btnDonate'), 0)

Mobile.pressBack()

Mobile.closeApplication()

