import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('D:\\nativeqa3-release-bypass-gpay-automation_test.apk', true)

Mobile.tap(findTestObject('DonationOnly/android.widget.Button - OK'), 0)

Mobile.tap(findTestObject('Notice_Dialog/notice_message_btn_OK'), 0)

Mobile.tap(findTestObject('Notice_Dialog/btn_GotIt'), 0)

Mobile.tap(findTestObject('ShoutWho/btnSearch'), 0)

Mobile.tap(findTestObject('ShoutWho/btnSearch'), 0)

Mobile.tap(findTestObject('Notice_Dialog/notice_msg_SelectFavouriteCause'), 0)

Mobile.tap(findTestObject('ShoutWho/txtCharityName'), 0)

//Mobile.tap(findTestObject('DonationOnly/android.widget.TextView - ActionAid Australia'), 0)

Mobile.tap(findTestObject('CharityDetail/btn_Donate'), 0)

Mobile.tap(findTestObject('DonationImpact/btn_GPay'), 0)

Mobile.tap(findTestObject('ThankYou/btnDone'), 0)

//Mobile.(findTestObject, 0, FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

