import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('D:\\nativeqa3-release-bypass-gpay-automation_test.apk', true)

Mobile.tap(findTestObject('DonationOnly/android.widget.Button - OK'), 0)

Mobile.tap(findTestObject('Notice_Dialog/notice_message_btn_OK'), 0)

Mobile.tap(findTestObject('Notice_Dialog/btn_GotIt'), 0)

Mobile.tap(findTestObject('ShoutWho/btnSearch'), 0)

Mobile.tap(findTestObject('ShoutWho/btnSearch'), 0)

Mobile.tap(findTestObject('Notice_Dialog/notice_msg_SelectFavouriteCause'), 0)

Mobile.tap(findTestObject('ShoutWho/txtCharityName'), 0)

Mobile.tap(findTestObject('CharityDetail/btnShop'), 0)

Mobile.scrollToText('Help support')

Mobile.getText(findTestObject('MerchandiseListScreen/lblHelpSupport'), 0)

Mobile.getText(findTestObject('MerchandiseListScreen/lblMerchandiseName'), 0)

//Mobile.getText(findTestObject('MerchandiseListScreen/lsItemName'), 0)

Mobile.tap(findTestObject('MerchandiseListScreen/lsItemColor'), 0)

Mobile.scrollToText('Red')

Mobile.tap(findTestObject('MerchandiseListScreen/optColor'), 0)

Mobile.tap(findTestObject('MerchandiseListScreen/lsItemSize'), 0)

Mobile.scrollToText('Large')

Mobile.tap(findTestObject('MerchandiseListScreen/optSize'), 0)

Mobile.tap(findTestObject('MerchandiseListScreen/txtQuantity'), 0)

Mobile.tap(findTestObject('MerchandiseListScreen/btnAddToCart'), 0)

//Mobile.tap(findTestObject('Object Repository/android.widget.ImageButton'), 0)
//Mobile.tap(findTestObject('Object Repository/android.widget.Button - DONE'), 0)
Mobile.tap(findTestObject('MerchandiseListScreen/btnNext'), 0)

//Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Edit'), 0)
//Mobile.tap(findTestObject('Object Repository/android.widget.Button - DONE'), 0)

Mobile.tap(findTestObject('ConfirmPaymentScreen/btn_GPayMerchandise'), 0)

Mobile.tap(findTestObject('ThankYou/btnDone'), 0)

//Mobile.(findTestObject, 0, FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

