import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

def app_patch = "D:\\app-qa3-release-Gpay-2.6.2.7(367).apk"

TestObject notice_message_btn_OK = findTestObject('Object Repository/Notice_Dialog/notice_message_btn_OK')
TestObject btn_SkipThisStep = findTestObject('Object Repository/DonationInformationPage/btn_SkipThisStep')
TestObject btn_Skip = findTestObject('null')
//Mobile.startApplication(app_patch, false)
//Mobile.tap(notice_message_btn_OK, 5)

WebUI.openBrowser("https://qa3.shoutforgood.net/app/charities/a-perfect-foundation/donate?token=5691e31ba1de1ca8a5eeff1ef890358a1a6803eb71da8770294f6fa9e5ff6375%24%24%2BbITSQJ2JKa23ce6ailHC7Qk2gOAoNKkoGiaKG%2BUU4hJclnXCwYLrzzu4sCa7A%3D%3D--kz%2BdA5%2FR0MxvEKtn--9rsO2BQugvTmHsn6JbStBA%3D%3D")
WebUI.scrollToElement(btn_Skip, 0)
WebUI.click(btn_SkipThisStep)

