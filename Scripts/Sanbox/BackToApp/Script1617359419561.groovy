import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration
//import com.kms.katalon.core.appium.driver as AppiumDriver

import com.core.utilities.common.CommonActions as common
import com.core.utilities.common.Enum



def app_patch = "D:\\Native-qa3-release-2.11.1.22.apk"

//def app_id = "com.shoutforgood.shout"


//TestObject notice_message_btn_OK = findTestObject('Object Repository/Notice_Dialog/notice_message_btn_OK')
//TestObject area_ReturnToApp = findTestObject('Object Repository/DonationInformationPage/area_ReturnToApp')
//TestObject btn_ReturnToApp = findTestObject('Object Repository/DonationInformationPage/btn_ReturnToApp')
//TestObject btn_ColelctMerchandise = findTestObject('Object Repository/DonationInformationPage/btn_CollectMerchandise')
//TestObject btn_SkipThisStep = findTestObject('Object Repository/DonationInformationPage/btn_SkipThisStep')
//TestObject area_SkipThisStep = findTestObject('Object Repository/DonationInformationPage/area_SkipThisStep')
//TestObject btn_Done = findTestObject('Object Repository/DonationInformationPage/btn_Done')
//
//TestObject appLink = findTestObject('Object Repository/DonationInformationPage/appLink')
//Mobile.startApplication(app_patch, false)

//Mobile.tap(notice_message_btn_OK, 5)

'Donation URL from Native App'
//WebUI.openBrowser("https://qa3.shoutforgood.net/app/charities/a-perfect-foundation/donate?token=5246bd77da7b9d6858065a2c8def689e5783535cdb5002f607c1c190a028843e%24%24%2BdxgYZUCzY5O%2Bb1n0EfVu2UmK8u2ZSODy43XQGBLfgZHYRIN%2B1aGboBnFnXw5g%3D%3D--iVl0h4jyJ2Nrqn0X--4N3mGiAJSE1vL479UP3xbw%3D%3D")
//WebUI.scrollToElement(btn_SkipThisStep, 5)
//WebUI.click(btn_SkipThisStep)

//'Moible Thank You URL'
//WebUI.openBrowser("https://qa3.shoutforgood.net/app/donations/12321/thank-you?token=deprecated-dfc17afa-4f62-4ea5-bd52-99d22a9b8b11")
////
////'Tapping on RETURN TO APP'
//WebUI.scrollToElement(btn_ReturnToApp, 5)
//WebUI.waitForElementClickable(btn_ReturnToApp, 5)
//WebUI.enhancedClick(btn_ReturnToApp)
//
//
//
//RunConfiguration.setMobileDriverPreferencesProperty("appPackage", 'com.shoutforgood.shout')
//RunConfiguration.setMobileDriverPreferencesProperty("appActivity", 'com.shoutforgood.shout.screen.splash.SplashScreenActivity')
//RunConfiguration.setMobileDriverPreferencesProperty("appActivity", 'com.shoutforgood.shout.screen.thankyou.ThankActivity')
// Start the application, but it will actually use the appPackage from above

//Mobile.startExistingApplication(app_id)

TestObject btn_message_OK = common.createTestObject(Enum.LocatorType.XPATH.getType(),
	"//*[@class = 'android.widget.Button' and @text = 'OK' and @resource-id = 'android:id/button1']")

Mobile.startApplication("D:\\Native-qa3-release-2.11.1.22.apk", false)

Mobile.tap(btn_message_OK, 0)

//WebUI.click(appLink)

//def appID = 'com.shoutforgood.shout'

//Mobile.startExistingApplication(appID)





'Tapping on COLLECT MERCHANDISE'
//WebUI.scrollToElement(btn_ColelctMerchandise, 5)
//WebUI.click(btn_ColelctMerchandise)

//'Fund Thank You URL'
//WebUI.openBrowser("https://qa3.shoutforgood.net/donations/12321/thank-you?token=deprecated-21ed4666-103f-4e02-be9f-45f87778ecf0")
//WebUI.scrollToElement(btn_Done, 5)
//WebUI.click(btn_Done)

