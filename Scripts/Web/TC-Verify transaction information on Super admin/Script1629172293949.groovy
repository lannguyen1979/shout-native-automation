import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.core.utilities.webservice.TransactionAPITesting
import com.core.utilities.models.transaction.TransactionRefine

import com.kms.katalon.core.util.KeywordUtil
import com.gui.pages.GenericMenu
import com.gui.pages.TransactionHistory

import com.gui.pages.LoginPage
import com.gui.pages.LeftNavigations
import com.gui.pages.TransactionDetailPage
import com.core.utilities.browser.GenericBrowser

def guestEmail ="shoutfortest@gmail.com"

def adminEmail = "lannguyen_3@kms-technology.com"

def totalWithFees = "11.00"

def charityName = "ActionAid Australia"

//Login admin and verity txn created 'Step: Get txnID from   TransactionAPITesting'
TransactionAPITesting txnTest = new TransactionAPITesting()
TransactionRefine conditions = new   TransactionRefine(guestEmail,"successful", "charity", "app",  "onetime","visa", GlobalVariable.gDateTxnSubmited)
String transId=  txnTest.getExactTxnId(GlobalVariable.gDateTimeSubmited, conditions)
KeywordUtil.logInfo("transId = $transId")

'Step: Login' 

new LoginPage().twoFALogin(adminEmail, GlobalVariable.G_loginPassword)

'Step: Verify SA Transaction History info'
GenericMenu genericMenu = new  GenericMenu()
genericMenu.navigateToSuperAdmin()
LeftNavigations leftNavigations = new LeftNavigations()
leftNavigations.navigateToMenuTransactionsHistory()

TransactionHistory transactionHistory = new TransactionHistory() TestObject
resultSearch = transactionHistory.getSearchResultByTransId(transId, charityName, GlobalVariable.gDateTimeSubmited, String.valueOf(totalWithFees))

'Step: Verify Transaction Details'
transactionHistory.navigatetoTransactionDetail(resultSearch)
TransactionDetailPage transDetailPage = new TransactionDetailPage()
transDetailPage.verifyTxnDetails(transId, "One Time", charityName,  GlobalVariable.gDateTimeSubmited, totalWithFees, guestEmail, "", "Card Holder Name",  "ShoutCharity", "1111", "")

@SetUp
void setUp(){
	GenericBrowser.openBrowser()
	GenericBrowser.maximizeWindow()
}



