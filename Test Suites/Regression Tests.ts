<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>9c54801c-d57d-404c-be15-6c167ec3650a</testSuiteGuid>
   <testCaseLink>
      <guid>9eb14078-21e6-435b-9587-31e88946038f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Sanbox/Verify Correct Alarm Message</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb594fd1-d00d-4a89-84a4-7a5675904a79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Sanbox/Verify Last Items In List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d9a6d99-8230-4d13-9455-9c8768f470f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Donation/Success/TC-Donation one time with GPay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>765d9fef-c204-4205-b182-f9a8ec1315a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web/TC-Verify transaction information on Super admin</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
